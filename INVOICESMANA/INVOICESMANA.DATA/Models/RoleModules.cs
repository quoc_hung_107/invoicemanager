﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace INVOICESMANA.DATA.Models
{
    public class RoleModules
    {
        public System.Guid RoleId { get; set; }
        public System.Guid ModuleId { get; set; }
        public bool IsAddAllowed { get; set; }
        public bool IsUpdateAllowed { get; set; }
        public bool IsDeleteAllowed { get; set; }
        public bool IsViewAllowed { get; set; }
        public Nullable<System.Guid> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.Guid> LastModifiedBy { get; set; }
        public Nullable<System.DateTime> LastModifiedDate { get; set; }

        public virtual Module Module { get; set; }
        public virtual Role Role { get; set; }
    }
}