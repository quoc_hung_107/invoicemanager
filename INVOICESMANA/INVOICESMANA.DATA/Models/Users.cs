﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace INVOICESMANA.DATA.Models
{
    public class Users
    {
        public System.Guid UserId { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string PasswordSalt { get; set; }
        public System.Guid RoleId { get; set; }
        public string FullName { get; set; }
        public Nullable<System.Guid> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.Guid> LastModifiedBy { get; set; }
        public Nullable<System.DateTime> LastModifiedDate { get; set; }
        public string Email { get; set; }
        public Nullable<bool> EmailConfirmed { get; set; }
        public System.Guid CompanyId { get; set; }
        public Nullable<bool> Status { get; set; }
        public virtual Company Company { get; set; }
        public virtual ICollection<Role> Roles { get; set; }
    }
}