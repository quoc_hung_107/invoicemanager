﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace INVOICESMANA.DATA.Models
{
    public class Roles
    {
        public System.Guid RoleId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Nullable<System.Guid> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.Guid> LastModifiedBy { get; set; }
        public Nullable<System.DateTime> LastModifiedDate { get; set; }
        public Nullable<bool> Status { get; set; }

        public virtual ICollection<RoleModule> RoleModules { get; set; }
        public virtual ICollection<User> Users { get; set; }
    }
}