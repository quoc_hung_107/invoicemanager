﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using INVOICESMANA.DATA;
using INVOICESMANA.DATA.Common;

namespace INVOICESMANA.DATA.Common
{
    public class UnitOfWork : IUnitOfWork
    {
        #region "Private Member(s)"

        private bool _disposed = false;
        private readonly ezInvoicesEntities _context;

        #endregion

        /// <summary>
        /// Constructor
        /// </summary>
        public UnitOfWork()
        {
            _context = new ezInvoicesEntities();
        }

        private ezInvoicesEntities _INVOICESdbContext;
        public ezInvoicesEntities INVOICESdbContext
        {
            get
            {
                if (_INVOICESdbContext == null)
                    this._INVOICESdbContext = _context;
                return _INVOICESdbContext;
            }
        }

        private IUserRepository _userRepository;
        public IUserRepository UserRepository
        {
            get
            {
                if (this._userRepository == null)
                    this._userRepository = new UserRepository(_context);
                return _userRepository;
            }
        }

        private IRepository<Role, Guid> _rolesRepository;
        public IRepository<Role, Guid> RolesRepository
        {
            get
            {
                if (this._rolesRepository == null)
                    this._rolesRepository = new Repository<Role, Guid>(_context);
                return _rolesRepository;
            }
        }

        private IRepository<Company, Guid> _companiesRepository;
        public IRepository<Company, Guid> CompaniesRepository
        {
            get
            {
                if (this._companiesRepository == null)
                    this._companiesRepository = new Repository<Company, Guid>(_context);
                return _companiesRepository;
            }
        }

        private IRepository<InvoiceType, Guid> _invoiceTypeRepository;
        public IRepository<InvoiceType, Guid> InvoiceTypeRepository
        {
            get
            {
                if (this._invoiceTypeRepository == null)
                    this._invoiceTypeRepository = new Repository<InvoiceType, Guid>(_context);
                return _invoiceTypeRepository;
            }
        }

        private IRepository<RoleModule, Guid> _roleModulesRepository;
        public IRepository<RoleModule, Guid> RoleModulesRepository
        {
            get
            {
                if (this._roleModulesRepository == null)
                    this._roleModulesRepository = new Repository<RoleModule, Guid>(_context);
                return _roleModulesRepository;
            }
        }

        private IRepository<Module, Guid> _modulesRepository;
        public IRepository<Module, Guid> ModulesRepository
        {
            get
            {
                if (this._modulesRepository == null)
                    this._modulesRepository = new Repository<Module, Guid>(_context);
                return _modulesRepository;
            }
        }

        private IRepository<PaymentType, Guid> _paymentTypeRepository;
        public IRepository<PaymentType, Guid> PaymentTypeRepository
        {
            get
            {
                if (this._paymentTypeRepository == null)
                    this._paymentTypeRepository = new Repository<PaymentType, Guid>(_context);
                return _paymentTypeRepository;
            }
        }

        private IRepository<Vat, Guid> _VatRepository;
        public IRepository<Vat, Guid> VatRepository
        {
            get
            {
                if (this._VatRepository == null)
                    this._VatRepository = new Repository<Vat, Guid>(_context);
                return _VatRepository;
            }
        }

        private IRepository<Invoice, Guid> _invoicesRepository;
        public IRepository<Invoice, Guid> InvoicesRepository
        {
            get
            {
                if (this._invoicesRepository == null)
                    this._invoicesRepository = new Repository<Invoice, Guid>(_context);
                return _invoicesRepository;
            }
        }


        private IRepository<InvoiceDetail, Guid> _invoiceDetailsRepository;
        public IRepository<InvoiceDetail, Guid> InvoiceDetailsRepository
        {
            get
            {
                if (this._invoiceDetailsRepository == null)
                    this._invoiceDetailsRepository = new Repository<InvoiceDetail, Guid>(_context);
                return _invoiceDetailsRepository;
            }
        }

        private IRepository<Customer, Guid> _customerRepository;
        public IRepository<Customer, Guid> CustomersRepository
        {
            get
            {
                if (this._customerRepository == null)
                    this._customerRepository = new Repository<Customer, Guid>(_context);
                return _customerRepository;
            }
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this._disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this._disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void SaveChanges()
        {
            try
            {
                _context.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    var rs = string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    rs = rs + eve.ValidationErrors.Aggregate(rs, (current, ve) => current + ("<br />" + string.Format("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage)));
                }
                throw e;
            }
            catch (DbUpdateException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}