﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace INVOICESMANA.DATA.Common
{
    public class UserRepository: Repository<User, Guid>, IUserRepository
    {
        private Repository<User, Guid> _userRepository;
        private DbSet<User> _dbSet;
        private readonly ezInvoicesEntities _dbContext;

        public UserRepository(ezInvoicesEntities dbContext)
            : base(dbContext)
        {
            _dbContext = dbContext;
            _userRepository = new Repository<User, Guid>(_dbContext);
            this._dbSet = _dbContext.Set<User>();
        }

        public User GetByUserId(Guid UserId)
        {
            User user = _dbSet.Where(s => s.UserId == UserId).SingleOrDefault();
            return user;
        }

        public void Update(User user)
        {
            User _user = _dbSet.Where(s => s.UserId == user.UserId).FirstOrDefault();
            if (_user != null)
            {
                _user.FullName = user.FullName;
                _user.CompanyId = user.CompanyId;
                _user.Email = user.Email;
                _user.EmailConfirmed = user.EmailConfirmed;
                _user.Password = user.Password;
                _user.PasswordSalt = user.PasswordSalt;
                _user.Status = user.Status;
                _user.UserName = user.UserName;
                _user.LastModifiedBy = user.LastModifiedBy;
                _user.LastModifiedDate = user.LastModifiedDate;
                _user.UserId = user.UserId;
            }
        }

        public void UpdatePassword(Guid id, string newPassword, string passwordSalt)
        {
            throw new NotImplementedException();
        }

        List<User> IUserRepository.GetAll()
        {
            return _dbSet.ToList();
        }
    }
}