﻿using System;
using INVOICESMANA.DATA;

namespace INVOICESMANA.DATA.Common
{
    public interface IUnitOfWork : IDisposable
    {
        ezInvoicesEntities INVOICESdbContext { get; }
        IUserRepository UserRepository { get; }
        IRepository<Role, Guid> RolesRepository { get; }
        IRepository<Company, Guid> CompaniesRepository { get; }
        IRepository<RoleModule, Guid> RoleModulesRepository { get; }
        IRepository<Module, Guid> ModulesRepository { get; }
        void SaveChanges();
    }
}