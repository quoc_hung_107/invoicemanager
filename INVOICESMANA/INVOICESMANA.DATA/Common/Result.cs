﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace INVOICESMANA.DATA.Common
{
    public class Result
    {
        public Int16 Code { get; set; }
        public string Message { get; set; }
        public object Data { get; set; }
        public string InvoiceId { get; set; }
        public string InvoiceNumber { get; set; }
        public Detail Detail { get; set; }
    }

    public class Detail
    {
        public List<DetailItem> DetailItems { get; set; }
    }

    public class DetailItem
    {
        public string InvoiceDetailId { get; set; }
        public string InvoiceDetailName { get; set; }
    }
}