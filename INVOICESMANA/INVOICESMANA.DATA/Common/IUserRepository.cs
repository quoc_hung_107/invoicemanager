﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using INVOICESMANA.DATA;

namespace INVOICESMANA.DATA.Common
{
    public interface IUserRepository : IRepository<User, Guid>
    {
        User GetById(Guid UserId);
        new List<User> GetAll();
        void UpdatePassword(Guid id, string newPassword, string passwordSalt);
        void Update(User user);
    }
}