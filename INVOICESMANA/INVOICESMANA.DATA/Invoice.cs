//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace INVOICESMANA.DATA
{
    using System;
    using System.Collections.Generic;
    
    public partial class Invoice
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Invoice()
        {
            this.InvoiceDetails = new HashSet<InvoiceDetail>();
        }
    
        public System.Guid InvoiceId { get; set; }
        public Nullable<System.Guid> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.Guid> LastModifiedBy { get; set; }
        public Nullable<System.DateTime> LastModifiedDate { get; set; }
        public string InvoiceType { get; set; }
        public System.Guid InvoiceCodeId { get; set; }
        public string InvoiceSeries { get; set; }
        public string InvoiceNumber { get; set; }
        public string InvoiceName { get; set; }
        public System.DateTime InvoiceIssuedDate { get; set; }
        public string CurrencyCode { get; set; }
        public decimal ExchangeRate { get; set; }
        public int AdjustmentType { get; set; }
        public Nullable<System.Guid> CustomerId { get; set; }
        public Nullable<System.Guid> PaymentId { get; set; }
        public string SellerBankAccount { get; set; }
        public string SellerBankName { get; set; }
        public Nullable<int> Status { get; set; }
        public string Signer { get; set; }
        public string SecretNumber { get; set; }
        public Nullable<int> InvoiceStatus { get; set; }
        public Nullable<bool> PrintStatus { get; set; }
        public Nullable<System.DateTime> SignedDate { get; set; }
        public string PrintBy { get; set; }
        public string Code_CT { get; set; }
        public Nullable<System.DateTime> SubmittedDate { get; set; }
        public string ContractNumber { get; set; }
        public Nullable<System.DateTime> ContractDate { get; set; }
        public string InvoiceNote { get; set; }
        public string AdditionalReferenceDes { get; set; }
        public Nullable<System.DateTime> AdditionalReferenceDate { get; set; }
        public Nullable<decimal> DiscountAmount { get; set; }
        public string OrderNumber { get; set; }
        public string LocalPDF { get; set; }
        public Nullable<System.Guid> CompanyId { get; set; }
        public string Account { get; set; }
        public string Bank { get; set; }
        public string so_benh_an { get; set; }
        public string sovb { get; set; }
        public Nullable<System.DateTime> ngayvb { get; set; }
        public string so_hd_dc { get; set; }
        public Nullable<System.Guid> inv_originalId { get; set; }
        public string dieu_tri { get; set; }
        public string ma_dvcs { get; set; }
        public string in_chuyen_doi { get; set; }
        public Nullable<System.DateTime> ngay_in_cdoi { get; set; }
        public string TemplaceCode { get; set; }
        public Nullable<int> SupplierType { get; set; }
    
        public virtual Company Company { get; set; }
        public virtual Customer Customer { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<InvoiceDetail> InvoiceDetails { get; set; }
        public virtual PaymentType PaymentType { get; set; }
    }
}
