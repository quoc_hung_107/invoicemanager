﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Configuration;
using INVOICESMANA.DATA;
using INVOICESMANA.DATA.Common;
using INVOICESMANA.Models;
using System.IO;
using INVOICESMANA.Helper;
using System.Net;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using System.Globalization;
using System.Transactions;

namespace INVOICESMANA.Controllers
{
    [Authorize]
    public class CompanyController : Controller
    {
        // GET: Company
        [LogFilterAttribute(ModuleId = "9cbfdfc3-dab3-46dc-88d0-eac5d7e31fdf")]
        public ActionResult Index()
        {

            return View();
        }

        public ActionResult GetCompanies(DataTableParams _params)
        {
            DataTableRespond result = new DataTableRespond();
            result.data = new List<Company>();
            try
            {
                using (var uow = new UnitOfWork())
                {
                    List<CompanyModel> res = new List<CompanyModel>();
                    var total = uow.CompaniesRepository.GetAll();
                    result.recordsTotal = total.Count();
                    result.recordsFiltered = total.Count();
                    switch (_params.iSortCol_0)
                    {
                        case 0:
                            {
                                if (_params.sSortDir_0 == "asc")
                                {
                                    total = total.OrderBy(x => x.CompanyCode).ToList();
                                }
                                else
                                {
                                    total = total.OrderByDescending(x => x.CompanyCode).ToList();
                                }
                                break;
                            }
                        case 1:
                            {
                                if (_params.sSortDir_0 == "asc")
                                {
                                    total = total.OrderBy(x => x.Name).ToList();
                                }
                                else
                                {
                                    total = total.OrderByDescending(x => x.Name).ToList();
                                }
                                break;
                            }
                        case 2:
                            {
                                if (_params.sSortDir_0 == "asc")
                                {
                                    total = total.OrderBy(x => x.Address).ToList();
                                }
                                else
                                {
                                    total = total.OrderByDescending(x => x.Address).ToList();
                                }
                                break;
                            }
                        case 3:
                            {
                                if (_params.sSortDir_0 == "asc")
                                {
                                    total = total.OrderBy(x => x.CreatedDate).ToList();
                                }
                                else
                                {
                                    total = total.OrderByDescending(x => x.CreatedDate).ToList();
                                }
                                break;
                            }
                        case 4:
                            {
                                if (_params.sSortDir_0 == "asc")
                                {
                                    total = total.OrderBy(x => x.PMS).ToList();
                                }
                                else
                                {
                                    total = total.OrderByDescending(x => x.PMS).ToList();
                                }
                                break;
                            }
                        case 5:
                            {
                                if (_params.sSortDir_0 == "asc")
                                {
                                    total = total.OrderBy(x => x.Status).ToList();
                                }
                                else
                                {
                                    total = total.OrderByDescending(x => x.Status).ToList();
                                }
                                break;
                            }
                    }
                    if (!string.IsNullOrEmpty(_params.sSearch))
                    {
                        total = total.Where(x => x.Name.ToLower().Contains(_params.sSearch.ToLower())).ToList();
                        result.recordsFiltered = total.Count();
                    }

                    var temp = total.Skip(_params.iDisplayStart).Take(_params.iDisplayLength);
                    if (temp.Any())
                    {
                        foreach (var r in temp)
                        {
                            res.Add(new CompanyModel
                            {
                                CompanyId = r.CompanyId,
                                CompanyCode = r.CompanyCode,
                                Name = r.Name,
                                Address = r.Address,
                                CreatedDate = r.CreatedDate,
                                InvoiceType = r.InvoiceType,
                                PMS = r.PMS,
                                Status = r.Status,
                                IsUses = Session["UserId"] == null
                            });
                        }
                    }
                    result.data = res;
                }
            }
            catch (Exception ex)
            {

            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Update(Guid companyid)
        {
            ViewBag.Title = "Chi tiết";
            var uow = new UnitOfWork();
            Company company = uow.CompaniesRepository.GetById(companyid);
            if (company.Logo != null)
            {
                if (System.IO.File.Exists(Server.MapPath(Url.Content(ConfigurationManager.AppSettings["logoCompanyPath"] + company.Logo))))
                {
                    company.Logo = Url.Content(ConfigurationManager.AppSettings["logoCompanyPath"] + company.Logo);
                }
                else
                {
                    company.Logo = Url.Content("~/Content/images/no-logo.jpg");
                }
            }

            return View("Detail", company);
        }

        [HttpPost]
        public ActionResult Update()
        {
            Result result = new Result();
            try
            {
                var comReq = new CompanyModel();
                comReq = JsonConvert.DeserializeObject<CompanyModel>(Request.Params["comReq"]);

                if (comReq.invoiceTypes == null && comReq.invoiceTypes.Count < 1)
                {
                    result.Code = -1;
                    result.Message = "Thông tin mẫu hóa đơn và ký hiệu";
                }

                if (!Directory.Exists(Server.MapPath(ConfigurationManager.AppSettings["logoCompanyPath"])))
                {
                    Directory.CreateDirectory(Server.MapPath(ConfigurationManager.AppSettings["logoCompanyPath"]));
                }
                bool changeLogo = false;
                var context = HttpContext;

                if (Request.Files.Count > 0)
                {
                    HttpPostedFileBase file = Request.Files[0];
                    string fileName = file.FileName;
                    string fileExtension = file.ContentType;

                    fileExtension = Path.GetExtension(fileName);
                    string str_image = "LOGO-" + Guid.NewGuid() + fileExtension;
                    string pathToSave = Server.MapPath(ConfigurationManager.AppSettings["logoCompanyPath"] + str_image);
                    file.SaveAs(pathToSave);
                    comReq.Logo = str_image;
                    changeLogo = true;
                }
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
                {
                    using (var uow = new UnitOfWork())
                    {
                        Company com = uow.CompaniesRepository.GetById(comReq.CompanyId);
                        var types = uow.InvoiceTypeRepository.GetWhere(x => x.CompanyId == com.CompanyId).ToList();
                        uow.InvoiceTypeRepository.DeleteRange(types);
                        uow.SaveChanges();
                        if (com != null)
                        {
                            if (changeLogo == true)
                            {
                                com.Logo = comReq.Logo;
                            }
                            com.Name = comReq.Name;
                            com.Address = comReq.Address;
                            com.Email = comReq.Email;
                            com.Phone = comReq.Phone;
                            com.TaxIdentification = comReq.TaxIdentification;
                            com.Account = comReq.Account;
                            com.Password = comReq.Password;
                            com.Status = comReq.Status;
                            com.InvoiceType = comReq.InvoiceType;
                            com.PMS = comReq.PMS;
                            com.CompanyCode = comReq.CompanyCode;
                            com.Token = comReq.Token;
                            com.ExpiryDate = comReq.ExpiryDate;
                            com.CreatedDate = comReq.CreatedDate;
                            com.CreatedBy = Helper.Identity.UserId;
                            com.LastModifiedBy = Helper.Identity.UserId;
                            com.LastModifiedDate = DateTime.Now;

                            foreach (var item in comReq.invoiceTypes)
                            {
                                com.InvoiceTypes.Add(new InvoiceType
                                {
                                    Id = Guid.NewGuid(),
                                    InvoiceSeries = item.InvoiceSeries,
                                    InvoiceTypes = item.InvoiceTypes
                                });
                            }
                            uow.SaveChanges();

                            scope.Complete();
                            result.Message = "OK";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                result.Code = -1;
                result.Message = ex.Message;
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddCompany()
        {
            ViewBag.Title = "Thêm mới";
            ViewBag.AddNew = "true";
            Company com = new Company();
            com.Logo = Url.Content("~/Content/images/no-logo.jpg");
            return View("Detail", com);
        }

        [HttpPost]
        public ActionResult AddNewCompany()
        {
            Result result = new Result();
            try
            {
                var comReq = new CompanyModel();
                var context = HttpContext;
                
                if (Request.Files.Count > 0)
                {
                    HttpPostedFileBase file = Request.Files[0];
                    string fileName = file.FileName;
                    string fileExtension = file.ContentType;

                    fileExtension = Path.GetExtension(fileName);
                    string str_image = "LOGO-" + Guid.NewGuid() + fileExtension;
                    string pathToSave = Server.MapPath(ConfigurationManager.AppSettings["logoCompanyPath"] + str_image);
                    file.SaveAs(pathToSave);
                    comReq.Logo = str_image;
                }
                using (var uow = new UnitOfWork())
                {
                    comReq = JsonConvert.DeserializeObject<CompanyModel>(Request.Params["comReq"]);

                    if (comReq.invoiceTypes == null && comReq.invoiceTypes.Count < 1)
                    {
                        result.Code = -1;
                        result.Message = "Thông tin mẫu hóa đơn và ký hiệu";
                    }

                    Company com = new Company();
                    com.CompanyId = Guid.NewGuid();
                    com.CompanyCode = comReq.CompanyCode;
                    com.Name = comReq.Name;
                    com.Address = comReq.Address;
                    com.Email = comReq.Email;
                    com.Phone = comReq.Phone;
                    com.TaxIdentification = comReq.TaxIdentification;
                    com.Account = comReq.Account;
                    com.Password = comReq.Password;
                    com.Status = comReq.Status;
                    com.InvoiceType = comReq.InvoiceType;
                    com.PMS = comReq.PMS;
                    com.CompanyCode = comReq.CompanyCode;
                    com.Logo = comReq.Logo;
                    com.Token = comReq.Token;
                    com.ExpiryDate = comReq.ExpiryDate;
                    com.CreatedBy = Helper.Identity.UserId;
                    com.CreatedDate = comReq.CreatedDate;
                    uow.CompaniesRepository.Add(com);

                    foreach (var item in comReq.invoiceTypes)
                    {
                        com.InvoiceTypes.Add(new InvoiceType
                        {
                            Id = Guid.NewGuid(),
                            InvoiceSeries = item.InvoiceSeries,
                            InvoiceTypes = item.InvoiceTypes
                        });
                    }

                    uow.SaveChanges();
                    result.Message = "OK";
                }
            }
            catch (Exception ex)
            {
                result.Code = -1;
                result.Message = ex.Message;
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult getToken(Guid id)
        {
            Result result = new Result();
            try
            {
                using (var uow = new UnitOfWork())
                {
                    var com = uow.CompaniesRepository.GetById(id);
                    if (com != null && com.Token != null)
                    {
                        result.Data = string.Format("{0} {1};{2}", "Bear", com.Token.ToString(), ConfigurationManager.AppSettings["ma_dvcs"]);
                    }
                }
            }
            catch (Exception ex)
            {
                result.Code = -1;
                result.Message = ex.Message;
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult refreshToken(Guid id)
        {
            Result result = new Result();
            try
            {
                string UserName = string.Empty;
                string Password = string.Empty;
                using (var uow = new UnitOfWork())
                {
                    var com = uow.CompaniesRepository.GetById(id);
                    if (com != null)
                    {
                        UserName = com.Account;
                        Password = com.Password;
                    }
                }
                var request = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings["URL_LoginMinvoice"]);

                string json = new JavaScriptSerializer().Serialize(new
                {
                    username = UserName,
                    password = Password,
                    ma_dvcs = ConfigurationManager.AppSettings["ma_dvcs"]
                });

                var data = System.Text.Encoding.ASCII.GetBytes(json);

                request.Method = "POST";
                request.ContentType = "application/json";
                request.ContentLength = data.Length;

                using (var stream = request.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }

                var response = (HttpWebResponse)request.GetResponse();
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var responseString = new System.IO.StreamReader(response.GetResponseStream()).ReadToEnd();
                    var temp = JsonConvert.DeserializeObject<Dictionary<string, string>>(responseString);
                    using (var uow = new UnitOfWork())
                    {
                        var com = uow.CompaniesRepository.GetById(id);
                        if (com != null)
                        {
                            com.Token = temp["token"].ToString();
                            uow.SaveChanges();
                        }
                    }
                    result.Data = string.Format("Bear {0};{1}", temp["token"].ToString(), ConfigurationManager.AppSettings["ma_dvcs"]);
                }
                else
                {
                    result.Message = response.StatusDescription;
                    result.Code = -2;
                }
            }
            catch (Exception ex)
            {

                result.Code = -1;
                result.Message = ex.Message;
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}