﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using INVOICESMANA.DATA;
using INVOICESMANA.DATA.Common;
using INVOICESMANA.Models;

namespace INVOICESMANA.Controllers
{
    public class CustomerController : Controller
    {
        // GET: Customer
        public ActionResult getCustomer(string seachString = "")
        {
            Result result = new Result();
            try
            {
                using (var uow = new UnitOfWork())
                {
                   var customers = uow.CustomersRepository.GetAll().ToList();
                    if (!string.IsNullOrEmpty(seachString))
                    {
                        customers = customers.Where(x => x.CustomerCode.ToLower().Contains(seachString.ToLower()) || x.FullName.ToLower().Contains(seachString.ToLower())).ToList();
                    }
                    List<Customer> res = new List<Customer>();
                    if (customers.Any())
                    {
                        foreach(var c in customers)
                        {
                            res.Add(new Customer()
                            {
                                CustomerCode = c.CustomerCode,
                                CustomerId = c.CustomerId,
                                FullName = c.FullName,
                                TaxIdentification = c.TaxIdentification,
                                Address = c.Address,
                                AccountNumber = c.AccountNumber,
                                CompanyId = c.CompanyId,
                                Bank = c.Bank,
                                CustomerCompanyName = c.CustomerCompanyName
                            });
                        }
                    }
                    result.Data = res;
                }
            }
            catch (Exception ex)
            {
                result.Code = -1;
                result.Message = ex.Message;
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public Result Add(Customer model)
        {
            Result result = new Result();
            try
            {
                using (var uow = new UnitOfWork())
                {
                    Customer c = new Customer();
                    c.CustomerId = Guid.NewGuid();
                    c.CompanyId = model.CompanyId;
                    c.CustomerCode = model.CustomerCode;
                    c.Address = model.Address;
                    c.Email = model.Email;
                    c.FullName = model.FullName;
                    c.Status = true;
                    c.TaxIdentification = model.TaxIdentification;
                    c.AccountNumber = model.AccountNumber;
                    c.Bank = model.Bank;
                    uow.CustomersRepository.Add(c);
                    uow.SaveChanges();
                    result.Data = c.CustomerId;
                }
            }
            catch (Exception ex)
            {
                result.Code = -1;
                result.Message = ex.Message;
            }
            return result;
        }


        public ActionResult getSuggestion(bool searchByCode, string seachString = "")
        {
            Result result = new Result();
            try
            {
                using (var uow = new UnitOfWork())
                {
                    var customers = uow.CustomersRepository.GetAll().ToList();
                    if (!string.IsNullOrEmpty(seachString))
                    {
                        if (searchByCode)
                        {
                            customers = customers.Where(x => x.CustomerCode.ToLower().Contains(seachString.ToLower())).ToList();
                        }else
                        {
                            customers = customers.Where(x => x.FullName.ToLower().Contains(seachString.ToLower())).ToList();
                        }
                    }
                    List<Customer> res = new List<Customer>();
                    if (customers.Any())
                    {
                        foreach (var c in customers)
                        {
                            res.Add(new Customer()
                            {
                                CustomerCode = c.CustomerCode,
                                CustomerId = c.CustomerId,
                                FullName = c.FullName,
                                TaxIdentification = c.TaxIdentification,
                                Address = c.Address,
                                AccountNumber = c.AccountNumber,
                                CompanyId = c.CompanyId,
                                Bank = c.Bank,
                                CustomerCompanyName = c.CustomerCompanyName
                            });
                        }
                    }
                    result.Data = res;
                }
            }
            catch (Exception ex)
            {
                result.Code = -1;
                result.Message = ex.Message;
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}