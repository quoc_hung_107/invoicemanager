﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Configuration;
using INVOICESMANA.DATA;
using INVOICESMANA.DATA.Common;
using INVOICESMANA.Helper;
using INVOICESMANA.Models;

namespace INVOICESMANA.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetPermissionOnPage(Guid moduleId)
        {
            Result result = new Result();
            result.Data = new PermisionOnPage();
            try
            {
                if (Identity.IsSuperAdmin)
                {
                    result.Data = new PermisionOnPage
                    {
                        IsAddAllowed = true,
                        IsDeleteAllowed = true,
                        IsUpdateAllowed = true,
                        IsViewAllowed = true
                    };
                }
                else
                {
                    using (var uow = new UnitOfWork())
                    {

                        var roleModules = uow.ModulesRepository.GetWhere(x => x.Status == true && x.ModuleId == moduleId).FirstOrDefault().RoleModules;
                        if (roleModules.Any())
                        {
                            List<Guid> userRoleId = uow.UserRepository.GetById(Identity.UserId).Roles.Select(x => x.RoleId).ToList();
                            List<DATA.RoleModule> permisions = roleModules.Where(x => userRoleId.Contains(x.RoleId)).ToList();
                            if (permisions.Any() && permisions.FirstOrDefault() != null)
                            {
                                var p = permisions.FirstOrDefault();
                                result.Data = new PermisionOnPage
                                {
                                    IsAddAllowed = p.IsAddAllowed,
                                    IsDeleteAllowed = p.IsDeleteAllowed,
                                    IsUpdateAllowed = p.IsUpdateAllowed,
                                    IsViewAllowed = p.IsViewAllowed
                                };
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                result.Code = -1;
                result.Message = ex.Message;
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}