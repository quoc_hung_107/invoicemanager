﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using INVOICESMANA.Models;
using INVOICESMANA.DATA;
using INVOICESMANA.DATA.Common;
using INVOICESMANA.Helper;

namespace INVOICESMANA.Controllers
{
    [Authorize]
    
    public class RoleController : Controller
    {
        // GET: Role
        [LogFilterAttribute(ModuleId = "26AD318C-7DB8-477B-9839-22AAA416884E")]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetRoles(DataTableParams _params)
        {
            DataTableRespond page = new DataTableRespond();
            List<RoleRespondModel> result = new List<RoleRespondModel>();
            try
            {
                using (var uow = new UnitOfWork())
                {
                    var res = uow.RolesRepository.GetWhere(x => x.Status == true).ToList();
                    if (res.Any())
                    {
                        page.recordsTotal = res.Count;
                        page.recordsFiltered = res.Count;
                        if (!string.IsNullOrEmpty(_params.sSearch))
                        {
                            res = res.Where(x => x.Name.ToLower().Contains(_params.sSearch.ToLower())).ToList();
                            page.recordsFiltered = res.Count;
                        }

                        foreach (var r in res)
                        {
                            result.Add(new RoleRespondModel() { RoleId = r.RoleId, Name = r.Name, UsersInRole = r.Users.Count() });
                        }
                        switch (_params.iSortCol_0)
                        {
                            case 0:
                                {
                                    if (_params.sSortDir_0 == "asc")
                                    {
                                        result = result.OrderBy(x => x.Name).ToList();
                                    }
                                    else
                                    {
                                        result = result.OrderByDescending(x => x.Name).ToList();
                                    }
                                    break;
                                }
                            case 1:
                                {
                                    if (_params.sSortDir_0 == "asc")
                                    {
                                        result = result.OrderBy(x => x.UsersInRole).ToList();
                                    }
                                    else
                                    {
                                        result = result.OrderByDescending(x => x.UsersInRole).ToList();
                                    }
                                    break;
                                }
                        }
                        page.data = result;
                    }
                    else
                    {
                        page.data = new List<RoleRespondModel>();
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
            return Json(page, JsonRequestBehavior.AllowGet);
        }

        public ActionResult RoleRights(Guid? RoleId)
        {
            if (RoleId == null)
            {
                return RedirectToAction("Index", "Role");
            }
            else
            {
				using (var uow = new UnitOfWork())
				{
					var role = uow.RolesRepository.GetById(RoleId.Value);
					if (role != null)
					{
						ViewBag.RoleId = RoleId;
						ViewBag.RoleName = role.Name;
					}
				}
                return View();
            }
        }

        public ActionResult GetRoleRights(DataTableParams _params)
        {
            DataTableRespond page = new DataTableRespond();
            List<RoleRightsModel> result = new List<RoleRightsModel>();
            try
            {
                Guid? RoleId = null;
                if (_params.customData != null)
                {
                    var lst = (string[])_params.customData;
                    Guid _out;
                    if (Guid.TryParse(lst[0].ToString(), out _out) == true)
                    {
                        RoleId = _out;
                    }
                }
                using (var uow = new UnitOfWork())
                {
                    var modules = uow.ModulesRepository.GetWhere(x => x.Status == true);
                    var roles = uow.RoleModulesRepository.GetWhere(x => x.Role.Status == true);
                    var res = (from module in modules
                               join role in roles.Where(x => x.RoleId == RoleId)
                               on module.ModuleId equals role.ModuleId into temp
                               from t in temp.DefaultIfEmpty()
                               select new RoleRightsModel()
                               {
                                   RoleId = (Guid)RoleId,
                                   ModuleId = module.ModuleId,
                                   ModuleName = module.Name,
                                   IsAddAllowed = (t == null ? false : t.IsAddAllowed),
                                   IsDeleteAllowed = (t == null ? false : t.IsDeleteAllowed),
                                   IsUpdateAllowed = (t == null ? false : t.IsUpdateAllowed),
                                   IsViewAllowed = (t == null ? false : t.IsViewAllowed)
                               }).ToList();
                    switch (_params.iSortCol_0)
                    {
                        case 0:
                            {
                                if (_params.sSortDir_0 == "asc")
                                {
                                    res = res.OrderBy(x => x.ModuleName).ToList();
                                }
                                else
                                {
                                    res = res.OrderByDescending(x => x.ModuleName).ToList();
                                }
                                break;
                            }
                    }
                    page.data = res;
                    page.recordsTotal = res.Count;
                    page.recordsFiltered = res.Count;
                }
            }
            catch (Exception)
            {

                throw;
            }
            return Json(page, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateRoleRights(List<RoleRightsModel> model)
        {
            Result result = new Result();
            try
            {
                using (var uow = new UnitOfWork())
                {
                    foreach (RoleRightsModel m in model)
                    {
                        var roleModule = uow.RoleModulesRepository.GetWhere(x => x.ModuleId == m.ModuleId && x.RoleId == m.RoleId).FirstOrDefault();
                        if (roleModule != null)
                        {
                            roleModule.LastModifiedBy = Identity.UserId;
                            roleModule.LastModifiedDate = DateTime.Now;
                            roleModule.IsAddAllowed = m.IsAddAllowed;
                            roleModule.IsDeleteAllowed = m.IsDeleteAllowed;
                            roleModule.IsUpdateAllowed = m.IsUpdateAllowed;
                            roleModule.IsViewAllowed = m.IsViewAllowed;
                            uow.SaveChanges();
                        }
                        else
                        {
                            RoleModule rm = new RoleModule();
                            rm.CreatedBy = Identity.UserId;
                            rm.CreatedDate = DateTime.Now;
                            rm.IsAddAllowed = m.IsAddAllowed;
                            rm.IsDeleteAllowed = m.IsDeleteAllowed;
                            rm.IsUpdateAllowed = m.IsUpdateAllowed;
                            rm.IsViewAllowed = m.IsViewAllowed;
                            rm.RoleId = m.RoleId;
                            rm.ModuleId = m.ModuleId;
                            uow.RoleModulesRepository.Add(rm);
                            uow.SaveChanges();
                        }
                    }
                    result.Message = "OK";
                }
            }
            catch (Exception ex)
            {
                result.Code = -1;
                result.Message = ex.Message;
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetListRole()
        {
            Result result = new Result();
            try
            {
                using (var uow = new UnitOfWork())
                {
                    var temp = uow.RolesRepository.GetWhere(x => x.Status == true).ToList();
                    if (temp.Any())
                    {
                        List<SelectItem> res = new List<SelectItem>();
                        foreach (var t in temp)
                        {
                            res.Add(new SelectItem() { Value = t.RoleId.ToString(), Text = t.Name });
                            result.Data = res;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.Message;
                result.Code = -1;
                result.Data = new List<SelectItem>();
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}