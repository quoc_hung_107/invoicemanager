﻿using System;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Net;
using System.Net.Mail;
using INVOICESMANA.DATA;
using INVOICESMANA.Models;
using INVOICESMANA.DATA.Common;
using INVOICESMANA.WEB.Manager;
using System.Web.Security;
using System.Configuration;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Data.Entity;
using INVOICESMANA.Helper;
using System.Net.Http;
using System.Web.Script.Serialization;

namespace INVOICESMANA.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        private IUserManager _userManager { get; set; }
        private List<UserSimple> _superAdmin = new List<UserSimple>();
        private List<Guid> superAdminRoleId = new List<Guid>();
        public AccountController()
        {
            _userManager = new UserManager();
            var temp = ConfigurationManager.AppSettings["superAdmin"];
            if (temp != null)
            {
                try
                {
                    _superAdmin = JsonConvert.DeserializeObject<List<UserSimple>>(temp);
                    superAdminRoleId = _superAdmin.Select(x => Guid.Parse(x.RoleId)).ToList();
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }
        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            if (Request.IsAuthenticated)
                return RedirectToAction("Index", "Home");
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        //[ValidateAntiForgeryToken]
        public ActionResult Login(LoginRequest model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                var superAdmin = _superAdmin.Where(x => x.UserName == model.Username && x.Password == model.Password).FirstOrDefault();
                if (superAdmin != null)
                {
                    string userData = string.Format("{{\"UserName\":\"{0}\",\"RoleId\":\"{1}\",\"UserId\":\"{2}\",\"SRolesId\":[{3}]}}",
                        superAdmin.UserName.ToString(),
                        superAdmin.RoleId.ToString(),
                        superAdmin.UserId.ToString(),
                        string.Join(",", superAdminRoleId.Select(x => "\"" + x.ToString() + "\"")));
                    FormsAuthentication.SetAuthCookie(userData, true);
                    ViewBag.UserID = superAdmin.UserId;
                    if (returnUrl == null || returnUrl == "")
                        return RedirectToAction("Index", "Home");
                    return RedirectToLocal(returnUrl);
                }

                Result result = _userManager.Login(model.Username, model.Password);
                if (result.Code == 0)
                {
                    User user = (User)result.Data;
                    string userData = string.Format("{{\"UserName\":\"{0}\",\"RoleId\":\"{1}\",\"UserId\":\"{2}\",\"SRolesId\":[{3}]}}",
                        user.UserName.ToString(),
                        string.Join(",", user.Roles.Select(x => x.RoleId.ToString()).ToList()),
                        user.UserId.ToString(),
                        string.Join(",", superAdminRoleId.Select(x => "\"" + x.ToString() + "\"")));
                    FormsAuthentication.SetAuthCookie(userData, true);

                    Session["UserId"] = user.UserId;
                    ViewBag.UserID = user.UserId;
                    Session["CompanyId"] = user.CompanyId;
                    if (user.Company != null)
                    {
                        Session["CompanyName"] = user.Company.Name;
                    }
                    
                    if (returnUrl == null || returnUrl == "")
                        return RedirectToAction("Index", "Home");
                    return RedirectToLocal(returnUrl);
                }
                else
                {
                    ModelState.AddModelError("", result.Message);
                    return View(model);
                }
            }
            else
            {
                ModelState.AddModelError("", "Yêu cầu nhập tên tài khoản và mật khẩu");
                return View(model);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        [HttpGet]
        [AllowAnonymous]
        public Result CreateDefaultUser()
        {
            return _userManager.CreateDefaultUser();
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ForgotPassword(LoginRequest model)
        {

            try
            {
                User user = _userManager.GetByName(model.Username);
                if (user == null)
                {
                    ModelState.AddModelError("", "Tài khoản không tồn tại!");
                }
                else
                {
                    // Send mail
                    SmtpClient smtpClient = new SmtpClient(ConfigurationManager.AppSettings["SmtpHost"], Convert.ToInt32(ConfigurationManager.AppSettings["SmtpPort"]));
                    
                    smtpClient.EnableSsl = true;
                    smtpClient.UseDefaultCredentials = false;
                    smtpClient.Credentials = new NetworkCredential(ConfigurationManager.AppSettings["SmtpUser"], ConfigurationManager.AppSettings["SmtpPassword"]);
                    smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;

                    MailMessage mailMessage = new MailMessage();
                    mailMessage.IsBodyHtml = true;
                    mailMessage.From = new MailAddress(ConfigurationManager.AppSettings["FromEmail"]);
                    mailMessage.Sender = new MailAddress(ConfigurationManager.AppSettings["FromEmail"]);
                    mailMessage.To.Add(new MailAddress(user.Email));
                    //mailMessage.Bcc.Add(new MailAddress(ConfigurationManager.AppSettings["ReceiptBccEmail"]));
                    mailMessage.Subject = "Đặt lại mặt khẩu";

                    string FilePath = System.Web.HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["EmailTemp_ForgotPassword"]);
                    string body = string.Empty;

                    if (System.IO.File.Exists(FilePath))
                    {
                        var crypto = new SimpleCrypto.PBKDF2();
                        System.IO.FileStream f1 = new System.IO.FileStream(FilePath, System.IO.FileMode.Open);
                        System.IO.StreamReader sr = new System.IO.StreamReader(f1);
                        string link = GetSiteRoot();
                        link += ("/Account/ForgotPasswordConfirm/?username=" + user.UserName + "&key=" + crypto.Compute(user.Password + user.PasswordSalt, user.PasswordSalt));
                        body = sr.ReadToEnd();
                        body = body.Replace("<%UserName%>", user.UserName);
                        body = body.Replace("<%here%>", link);
                        f1.Close();
                    }
                    mailMessage.Body = body;
                    mailMessage.Priority = MailPriority.Normal;
                    smtpClient.Send(mailMessage);
                    ModelState.AddModelError("", string.Format("Vui lòng kiểm tra email bạn đã đăng kí cho tài khoản để đặt lại mật khẩu. ({0}***@{1})", string.Concat(user.Email.Split('@')[0].Take(5)), user.Email.Split('@')[1]));
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
            }

            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirm()
        {
            string username = Request.QueryString["username"];
            string key = Request.QueryString["key"];

            User user = _userManager.GetByName(username);
            ResetPasswordRequest req = new ResetPasswordRequest();
            if (user != null)
            {
                req.Username = user.UserName;
                var crypto = new SimpleCrypto.PBKDF2();
                if (HttpUtility.UrlDecode(crypto.Compute(user.Password + user.PasswordSalt, user.PasswordSalt)) != key)
                {
                    //return RedirectToAction("Login", "Account");
                    ModelState.AddModelError("", "Đường link lấy lại mật khẩu đã hết phiên làm việc");
                }
            }
            else
            {
                ModelState.AddModelError("", "Tài khoản không tồn tại!");
            }

            return View("ForgotPasswordConfirmation", req);
        }

        [HttpGet]
        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();
            Session.Clear();
            return RedirectToAction("Login", "Account");
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ChangePassword(ResetPasswordRequest model)
        {
            if (model.Password != model.ConfirmPassword)
            {
                ModelState.AddModelError("", "Mật khẩu mới và xác nhận mật khẩu mới không giống nhau");
            }
            else
            {
                if (_userManager.ChangePassword(model.Username, model.Password).Code == 0)
                {
                    return RedirectToAction("Login", "Account");
                }
            }
            return View("ForgotPasswordConfirmation", model);
        }

        private static string GetSiteRoot()
        {
            string port = System.Web.HttpContext.Current.Request.ServerVariables["SERVER_PORT"];
            if (port == null || port == "80" || port == "443")
                port = "";
            else
                port = ":" + port;

            string protocol = System.Web.HttpContext.Current.Request.ServerVariables["SERVER_PORT_SECURE"];
            if (protocol == null || protocol == "0")
                protocol = "http://";
            else
                protocol = "https://";

            string sOut = protocol + System.Web.HttpContext.Current.Request.ServerVariables["SERVER_NAME"] + port + System.Web.HttpContext.Current.Request.ApplicationPath;

            if (sOut.EndsWith("/"))
            {
                sOut = sOut.Substring(0, sOut.Length - 1);
            }

            return sOut;
        }

        [LogFilterAttribute(ModuleId = "26ad318c-7db8-477b-9839-22aaa416884e")]
        public ActionResult Users(Guid? companyId)
        {
            if (companyId.HasValue)
            {
                using (var uow = new UnitOfWork())
                {
                    var company = uow.CompaniesRepository.GetById(companyId.Value);
                    if (company != null)
                    {
                        ViewBag.CompanyId = company.CompanyId;
                        Session["CompanyName"] = company.Name;
                        Session["CompanyId"] = company.CompanyId;
                    }
                }
            }
            return View();

            return RedirectToAction("Index", "Invoice");
        }

        public ActionResult GetUsers(DataTableParams _params)
        {
            DataTableRespond data = new DataTableRespond();
            List<UserModel> users = new List<UserModel>();
            try
            {
                bool isSuperAdmin = false;
                if (Helper.Identity.RoleId != null && Helper.Identity.RoleIdSuperAdmin.Contains((Guid)Helper.Identity.RoleId))
                {
                    isSuperAdmin = true;
                }

                Guid? CompanyId = null;

                using (var uow = new UnitOfWork())
                {
                    if (_params.customData != null)
                    {
                        var lst = (string[])_params.customData;
                        Guid _out;
                        if (Guid.TryParse(lst[0].ToString(), out _out) == true)
                        {
                            CompanyId = _out;
                        }
                    }
                    if (isSuperAdmin == false)
                    {
                        CompanyId = uow.UserRepository.GetWhere(x => x.UserId == Helper.Identity.UserId).FirstOrDefault().CompanyId;

                    }

                    List<DATA.User> total = uow.UserRepository.GetAll();
                    if (CompanyId != null)
                    {
                        total = total.Where(x => x.CompanyId == CompanyId).ToList();
                    }
                    data.recordsTotal = total.Count;
                    data.recordsFiltered = data.recordsTotal;
                    if (!string.IsNullOrEmpty(_params.sSearch))
                    {
                        total = total.Where(x => x.UserName.ToLower().Contains(_params.sSearch.ToLower()) || x.FullName.ToLower().Contains(_params.sSearch.ToLower())).ToList();
                        data.recordsFiltered = total.Count();
                    }
                    switch (_params.iSortCol_0)
                    {
                        case 0:
                            {
                                if (_params.sSortDir_0 == "asc")
                                {
                                    total = total.OrderBy(x => x.UserName).ToList();
                                }
                                else
                                {
                                    total = total.OrderByDescending(x => x.UserName).ToList();
                                }
                                break;
                            }
                        case 1:
                            {
                                if (_params.sSortDir_0 == "asc")
                                {
                                    total = total.OrderBy(x => x.FullName).ToList();
                                }
                                else
                                {
                                    total = total.OrderByDescending(x => x.FullName).ToList();
                                }
                                break;
                            }
                        case 2:
                            {
                                if (_params.sSortDir_0 == "asc")
                                {
                                    total = total.OrderBy(x => x.Company.Name).ToList();
                                }
                                else
                                {
                                    total = total.OrderByDescending(x => x.Company.Name).ToList();
                                }
                                break;
                            }
                        case 3:
                            {
                                if (_params.sSortDir_0 == "asc")
                                {
                                    total = total.OrderBy(x => string.Join(", ", x.Roles.Select(z => z.Name))).ToList();
                                }
                                else
                                {
                                    total = total.OrderByDescending(x => string.Join(", ", x.Roles.Select(z => z.Name))).ToList();
                                }
                                break;
                            }
                        case 4:
                            {
                                if (_params.sSortDir_0 == "asc")
                                {
                                    total = total.OrderBy(x => x.CreatedDate).ToList();
                                }
                                else
                                {
                                    total = total.OrderByDescending(x => x.CreatedDate).ToList();
                                }
                                break;
                            }
                        case 5:
                            {
                                if (_params.sSortDir_0 == "asc")
                                {
                                    total = total.OrderBy(x => x.Status).ToList();
                                }
                                else
                                {
                                    total = total.OrderByDescending(x => x.Status).ToList();
                                }
                                break;
                            }
                    }
                    var res = total.Skip(_params.iDisplayStart).Take(_params.iDisplayLength);
                    if (res.Any())
                    {
                        foreach (var r in res)
                        {
                            users.Add(new UserModel()
                            {
                                UserId = r.UserId,
                                UserName = r.UserName,
                                FullName = r.FullName,
                                CreatedDate = r.CreatedDate,
                                Status = r.Status,
                                RolesName = r.Roles.Select(x => x.Name).ToList(),
                                CompanyName = r.Company.Name
                            });
                        }
                        data.data = users;
                    }
                    else
                    {
                        data.data = new List<User>();
                    }
                }

            }
            catch (Exception ex)
            {

            }
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Update(Guid userId)
        {
            ViewBag.UserId = userId;
            ViewBag.Title = "Chi tiết";
            return View("Detail");
        }

        public ActionResult MyAccount(Guid userId)
        {
            ViewBag.MyAccount = "true";
            ViewBag.UserId = userId;
            ViewBag.Title = "Tài khoản của tôi";
            return View("MyAccount");
        }

        [HttpPost]
        public ActionResult Update(UserParams model)
        {
            Result result = new Result();
            using (var uow = new UnitOfWork())
            {
                try
                {
                    var crypto = new SimpleCrypto.PBKDF2();
                    User user = uow.INVOICESdbContext.Users.Include("Roles").FirstOrDefault(x => x.UserId == model.UserId);
                    if (!String.IsNullOrEmpty(model.Password))
                    {
                        user.Password = crypto.Compute(model.Password);
                        user.PasswordSalt = crypto.Salt;
                    }
                    if (model.Role != null && model.Role.Any())
                    {
                        if (user.Roles.Any())
                        {
                            foreach (Role role in user.Roles.ToList())
                            {
                                user.Roles.Remove(role);
                            }
                        }
                        foreach (Guid roleId in model.Role)
                        {
                            Role role = uow.RolesRepository.GetById(roleId);
                            user.Roles.Add(role);
                        }
                    }
                    user.CompanyId = model.CompanyId;
                    user.FullName = model.FullName;
                    user.Email = model.Email;
                    user.LastModifiedDate = DateTime.Now;
                    user.Status = model.Status;
                    uow.UserRepository.Update(user);
                    uow.SaveChanges();
                }
                catch (Exception ex)
                {
                    result.Code = -1;
                    result.Message = ex.Message;
                }
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddUser()
        {
            ViewBag.AddNew = "true";
            ViewBag.Title = "Thêm mới người dùng";
            return View("Detail");
        }

        [HttpPost]
        public ActionResult AddUser(UserParams model)
        {
            Result result = new Result();
            using (var uow = new UnitOfWork())
            {
                try
                {
                    var crypto = new SimpleCrypto.PBKDF2();
                    bool checkExist = _superAdmin.Select(x => x.UserName.ToLower()).ToList().Contains(model.UserName.ToLower());
                    if (checkExist == false)
                    {
                        checkExist = uow.UserRepository.GetWhere(x => x.UserName.ToLower() == model.UserName.ToLower()).FirstOrDefault() != null;
                    }
                    if (!checkExist)
                    {
                        User user = new User()
                        {
                            UserId = Guid.NewGuid(),
                            UserName = model.UserName.ToLower(),
                            CompanyId = model.CompanyId,
                            Password = crypto.Compute(model.Password),
                            PasswordSalt = crypto.Salt,
                            FullName = model.FullName,
                            Email = model.Email,
                            EmailConfirmed = false,
                            Status = model.Status,
                            CreatedBy = Identity.UserId,
                            CreatedDate = DateTime.Now
                        };
                        if (model.Role.Any())
                        {
                            foreach (Guid roleId in model.Role)
                            {
                                Role role = uow.RolesRepository.GetById(roleId);
                                user.Roles.Add(role);
                            }
                        }
                        uow.UserRepository.Add(user);
                        uow.SaveChanges();
                    }
                    else
                    {
                        result.Message = "Tên tài khoản đã được sử dụng";
                        result.Code = -2;
                    }
                }
                catch (Exception ex)
                {
                    result.Message = ex.Message;
                    result.Code = -1;
                }
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult getCompanies()
        {
            var uow = new UnitOfWork();
            var res = uow.CompaniesRepository.GetAll().OrderBy(x => x.Name);
            List<SelectItem> list = new List<SelectItem>();
            if (res.Any())
            {
                foreach (var x in res)
                {
                    list.Add(new SelectItem() { Value = x.CompanyId.ToString(), Text = x.Name, Attributes = new { TaxIdentification = x.TaxIdentification, Address = x.Address } });
                }
            }
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetUserDetail(Guid userId)
        {
            Result result = new Result();
            try
            {
                var uow = new UnitOfWork();
                UserParams model = new UserParams();
                User user = uow.INVOICESdbContext.Users.Include("Roles").FirstOrDefault(x => x.UserId == userId);
                List<Role> Roles = uow.RolesRepository.GetAll().ToList();
                List<Company> Companies = uow.CompaniesRepository.GetAll().ToList();
                if (!Identity.IsSuperAdmin)
                {
                    var currentUser = uow.UserRepository.GetById(Identity.UserId);
                    Companies = Companies.Where(x => x.CompanyId == currentUser.CompanyId).ToList();
                }
                List<SelectItem> com = new List<SelectItem>();
                if (Companies.Any())
                {
                    foreach (Company c in Companies)
                    {
                        com.Add(new SelectItem() { Value = c.CompanyId.ToString(), Text = c.Name, Attributes = new { TaxIdentification = c.TaxIdentification, Address = c.Address } });
                    }
                }
                List<SelectItem> role = new List<SelectItem>();
                if (Roles.Any())
                {
                    foreach (Role c in Roles)
                    {
                        role.Add(new SelectItem() { Value = c.RoleId.ToString(), Text = c.Name });
                    }
                }
                model.UserId = user.UserId;
                model.UserName = user.UserName;
                model.CompanyId = user.CompanyId;
                model.Email = user.Email;
                model.FullName = user.FullName;
                model.Role = user.Roles.Select(x => x.RoleId).ToList();
                model.Status = user.Status;
                model.Companies = com;
                model.Roles = role;
                result.Data = model;
            }
            catch (Exception ex)
            {
                result.Message = ex.Message;
                result.Code = -1;
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult getTokenMinvoice(string UserName, string Password)
        {
            Result result = new Result();
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings["URL_LoginMinvoice"]);

                string json = new JavaScriptSerializer().Serialize(new
                {
                    username = UserName,
                    password = Password,
                    ma_dvcs = ConfigurationManager.AppSettings["ma_dvcs"]
                });

                var data = System.Text.Encoding.ASCII.GetBytes(json);

                request.Method = "POST";
                request.ContentType = "application/json";
                request.ContentLength = data.Length;

                using (var stream = request.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }

                var response = (HttpWebResponse)request.GetResponse();
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var responseString = new System.IO.StreamReader(response.GetResponseStream()).ReadToEnd();
                    var resultObject = JsonConvert.DeserializeObject<dynamic>(responseString);
                    string error = resultObject.error;
                    if (!string.IsNullOrEmpty(resultObject.error))
                    {
                        result.Message = resultObject.error;
                        result.Code = -2;
                    }
                    else
                    {
                        result.Code = 0;
                        result.Data = resultObject.token;
                    }
                }
                else
                {
                    result.Message = response.StatusDescription;
                    result.Code = -2;
                }
            }
            catch (Exception ex)
            {

                result.Code = -1;
                result.Message = ex.Message;
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}