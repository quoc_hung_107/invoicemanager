﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using INVOICESMANA.Helper;
using INVOICESMANA.Models;
using INVOICESMANA.DATA;
using INVOICESMANA.DATA.Common;
using System.Net;
using System.IO;
using System.Configuration;
using INVOICESMANA.Controllers;

namespace INVOICESMANA.Controllers
{
    [Authorize]
    public class InvoiceController : Controller
    {
        // GET: Invoice
        [LogFilterAttribute(ModuleId = "96289b7d-d686-4e2e-9986-7d20e930d394")]
        public ActionResult Index()
        {
            if (Session["UserId"] != null)
            {
                var userId = new Guid(Session["UserId"].ToString());
                using (var uow = new UnitOfWork())
                {
                    var user = uow.UserRepository.GetById(userId);
                    if (user != null)
                    {
                        var company = uow.CompaniesRepository.GetById(user.CompanyId);
                        if (company != null)
                        {
                            ViewBag.CompanyId = company.CompanyId;
                        }
                    }
                }
            }

            if (Session["CompanyId"] == null)
            {
                return RedirectToAction("Index", "Company");
            }

            return View();
        }

        [LogFilterAttribute(ModuleId = "96289b7d-d686-4e2e-9986-7d20e930d394")]
        public ActionResult AddNew()
        {
            if (Session["UserId"] != null)
            {
                var userId = new Guid(Session["UserId"].ToString());
                using (var uow = new UnitOfWork())
                {
                    var user = uow.UserRepository.GetById(userId);
                    if (user != null)
                    {
                        var company = uow.CompaniesRepository.GetById(user.CompanyId);
                        if (company != null)
                        {
                            ViewBag.CompanyId = company.CompanyId;
                        }
                    }
                }
            }
            if (Session["CompanyId"] == null)
            {
                return RedirectToAction("Index", "Company");
            }

            ViewBag.EditMode = "false";
            ViewBag.Title = "Thêm hóa đơn";
            return View("Detail");
        }

        [LogFilterAttribute(ModuleId = "96289b7d-d686-4e2e-9986-7d20e930d394")]
        public ActionResult Detail(Guid? id)
        {
            if (Session["UserId"] != null)
            {
                var userId = new Guid(Session["UserId"].ToString());
                using (var uow = new UnitOfWork())
                {
                    var user = uow.UserRepository.GetById(userId);
                    if (user != null)
                    {
                        ViewBag.CompanyId = user.CompanyId;
                    }
                }
            }

            if (Session["CompanyId"] == null)
            {
                return RedirectToAction("Index", "Company");
            }

            if (id.HasValue == false)
            {
                return RedirectToAction("Index");
            }

            ViewBag.InvoiceId = id;
            ViewBag.EditMode = "true";
            ViewBag.Title = "Chi tiết hóa đơn";
            return View("Detail");
        }

        [LogFilterAttribute(ModuleId = "96289b7d-d686-4e2e-9986-7d20e930d394")]
        public ActionResult Invoice(Guid? companyId)
        {
            if (companyId.HasValue)
            {
                using (var uow = new UnitOfWork())
                {
                    var company = uow.CompaniesRepository.GetById(companyId.Value);
                    if (company != null)
                    {
                        ViewBag.CompanyId = company.CompanyId;
                        Session["CompanyName"] = company.Name;
                        Session["CompanyId"] = company.CompanyId;
                        Session["Type"] = (int)InvoiceTypes.mInvoice;
                    }
                }
            }
            return RedirectToAction("Index");
        }

        public ActionResult getCombobox()
        {
            Result result = new Result();
            DroplistDependency res = new DroplistDependency();
            try
            {
                using (var uow = new UnitOfWork())
                {
                    res.VAT = new List<SelectItem>();
                    var VAT = uow.VatRepository.GetAll();
                    if (VAT.Any())
                    {
                        foreach (var v in VAT)
                        {
                            res.VAT.Add(new SelectItem() { Value = v.VatValue.ToString(), Text = v.Name });
                        }
                    }

                    res.PaymentType = new List<SelectItem>();
                    var paymentType = uow.PaymentTypeRepository.GetAll();
                    if (paymentType.Any())
                    {
                        foreach (var p in paymentType)
                        {
                            res.PaymentType.Add(new SelectItem() { Value = p.PaymentId.ToString(), Text = p.Name });
                        }
                    }
                }
                result.Data = res;
            }
            catch (Exception ex)
            {
                result.Code = -1;
                result.Message = ex.Message;
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult getListInvoice(DataTableParams _params)
        {
            DataTableRespond result = new DataTableRespond();
            result.data = new List<Invoice>();
            try
            {
                using (var uow = new UnitOfWork())
                {
                    var invoices = uow.InvoicesRepository.GetAll().AsQueryable();
                    result.recordsTotal = invoices.Count();
                    result.recordsFiltered = invoices.Count();

                    if (invoices.Any())
                    {
                        switch (_params.iSortCol_0)
                        {
                            case 0:
                                {
                                    if (_params.sSortDir_0 == "asc")
                                    {
                                        invoices = invoices.OrderBy(x => x.InvoiceNumber);
                                    }
                                    else
                                    {
                                        invoices = invoices.OrderByDescending(x => x.InvoiceNumber);
                                    }
                                    break;
                                }
                            case 1:
                                {
                                    if (_params.sSortDir_0 == "asc")
                                    {
                                        invoices = invoices.OrderBy(x => x.OrderNumber);
                                    }
                                    else
                                    {
                                        invoices = invoices.OrderByDescending(x => x.OrderNumber);
                                    }
                                    break;
                                }
                            case 2:
                                {
                                    if (_params.sSortDir_0 == "asc")
                                    {
                                        invoices = invoices.OrderBy(x => x.ContractDate);
                                    }
                                    else
                                    {
                                        invoices = invoices.OrderByDescending(x => x.ContractDate);
                                    }
                                    break;
                                }
                            case 3:
                                {
                                    if (_params.sSortDir_0 == "asc")
                                    {
                                        invoices = invoices.OrderBy(x => x.Customer.FullName);
                                    }
                                    else
                                    {
                                        invoices = invoices.OrderByDescending(x => x.Customer.FullName);
                                    }
                                    break;
                                }
                            case 4:
                                {
                                    if (_params.sSortDir_0 == "asc")
                                    {
                                        invoices = invoices.OrderBy(x => x.Company.Name);
                                    }
                                    else
                                    {
                                        invoices = invoices.OrderByDescending(x => x.Company.Name);
                                    }
                                    break;
                                }
                            case 5:
                                {
                                    if (_params.sSortDir_0 == "asc")
                                    {
                                        invoices = invoices.OrderBy(x => x.Customer.Address);
                                    }
                                    else
                                    {
                                        invoices = invoices.OrderByDescending(x => x.Customer.Address);
                                    }
                                    break;
                                }
                            case 6:
                                {
                                    if (_params.sSortDir_0 == "asc")
                                    {
                                        invoices = invoices.OrderBy(x => x.Customer.Email);
                                    }
                                    else
                                    {
                                        invoices = invoices.OrderByDescending(x => x.Customer.Email);
                                    }
                                    break;
                                }
                            case 7:
                                {
                                    if (_params.sSortDir_0 == "asc")
                                    {
                                        invoices = invoices.OrderBy(x => x.InvoiceDetails.Sum(y => y.TotalAmount));
                                    }
                                    else
                                    {
                                        invoices = invoices.OrderByDescending(x => x.InvoiceDetails.Sum(y => y.TotalAmount));
                                    }
                                    break;
                                }
                            case 8:
                                {
                                    if (_params.sSortDir_0 == "asc")
                                    {
                                        invoices = invoices.OrderBy(x => x.PaymentType.Name);
                                    }
                                    else
                                    {
                                        invoices = invoices.OrderByDescending(x => x.PaymentType.Name);
                                    }
                                    break;
                                }
                        }

                        if (!string.IsNullOrEmpty(_params.companyId))
                        {
                            var company = uow.CompaniesRepository.GetById(new Guid(_params.companyId));
                            if (company != null)
                            {
                                invoices = invoices.Where(x => x.CompanyId == company.CompanyId && x.InvoiceStatus != 15);
                                ViewBag.CompanyId = company.CompanyId;
                                Session["CompanyName"] = company.Name;
                            }
                        }

                        if (!string.IsNullOrEmpty(_params.sSearch))
                        {
                            //System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern
                            invoices = invoices.Where(x =>
                                (x.InvoiceNumber != null ? x.InvoiceNumber.ToLower().Contains(_params.sSearch.ToLower()) : false) ||
                                (x.OrderNumber != null ? x.OrderNumber.ToLower().Contains(_params.sSearch.ToLower()) : false) ||
                                (x.ContractDate.HasValue ? x.ContractDate.ToString().Contains(_params.sSearch.ToLower()) : false) ||
                                (x.Customer != null ? (x.Customer.FullName != null ? x.Customer.FullName.ToLower().Contains(_params.sSearch.ToLower()) : false) : false) ||
                                (x.Company != null ? (x.Company.Name != null ? x.Company.Name.ToLower().Contains(_params.sSearch.ToLower()) : false) : false) &&
                                x.SupplierType != (int)InvoiceTypes.viettel
                            );
                        }

                        List<InvoiceModel> res = new List<InvoiceModel>();
                        var temp = invoices.Skip(_params.iDisplayStart).Take(_params.iDisplayLength).ToList();
                        result.recordsFiltered = invoices.Count();

                        if (temp.Any())
                        {
                            foreach (var r in temp)
                            {
                                res.Add(new InvoiceModel
                                {
                                    InvoiceId = r.InvoiceId,
                                    InvoiceNumber = r.InvoiceNumber,
                                    OrderNumber = r.OrderNumber,
                                    ContractDate = r.ContractDate,
                                    CustomerName = r.Customer.FullName,
                                    CompanyName = r.Company.Name,
                                    Address = r.Customer.Address,
                                    Email = r.Customer.Email,
                                    PDF = r.LocalPDF,
                                    TotalAmount = r.InvoiceDetails.Sum(x => x.TotalAmount),
                                    PaymentType = r.PaymentType == null ? "" : r.PaymentType.Name,
                                    Status = r.InvoiceStatus
                                });
                            }
                        }
                        result.data = res;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult copyFileToLocal(string uri, Guid InvoiceId)
        {
            Result result = new Result();
            try
            {
                if (!Directory.Exists(Server.MapPath(ConfigurationManager.AppSettings["localPDF"])))
                {
                    Directory.CreateDirectory(Server.MapPath(ConfigurationManager.AppSettings["localPDF"]));
                }
                string fileName = InvoiceId + ".pdf";
                string pathToSave = Server.MapPath(ConfigurationManager.AppSettings["localPDF"] + fileName);
                WebClient webClient = new WebClient();
                webClient.DownloadFile(uri, pathToSave);
                if (System.IO.File.Exists(pathToSave))
                {
                    using (var uow = new UnitOfWork())
                    {
                        var invoice = uow.InvoicesRepository.GetById(InvoiceId);
                        if (invoice != null)
                        {
                            invoice.LocalPDF = fileName;
                            uow.InvoicesRepository.Save();
                            uow.SaveChanges();
                        }
                    }
                    result.Message = "Copy và lưu file thành công";
                }
                else
                {
                    result.Code = -2;
                    result.Message = "File không tồn tại để copy, hoặc copy không thành công";
                }
            }
            catch (Exception ex)
            {
                result.Code = -1;
                result.Message = ex.Message;
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetDetail(Guid id)
        {
            Result result = new Result();
            try
            {
                using (var uow = new UnitOfWork())
                {
                    Invoice invoice = uow.InvoicesRepository.GetById(id);
                    if (invoice != null)
                    {
                        InvoiceDetailModel inv = new InvoiceDetailModel();
                        inv.InvoiceId = invoice.InvoiceId;
                        inv.InvoiceCodeId = invoice.InvoiceCodeId;
                        inv.InvoiceType = invoice.InvoiceType;
                        inv.InvoiceSeries = invoice.InvoiceSeries;
                        inv.ContractDate = invoice.ContractDate;
                        inv.ContractNumber = invoice.InvoiceNumber;
                        inv.InvoiceNumber = invoice.InvoiceNumber;
                        inv.OrderNumber = invoice.OrderNumber;
                        inv.PaymentId = invoice.PaymentId;
                        inv.CustomerId = invoice.CustomerId;
                        inv.CustomerFullName = invoice.Customer.FullName;
                        inv.CustomerTaxIdentification = invoice.Customer.TaxIdentification;
                        inv.CustomerAddress = invoice.Customer.Address;
                        inv.CustomerAccountNumber = invoice.Customer.AccountNumber;
                        inv.CustomerBank = invoice.Customer.Bank;
                        inv.CustomerCompanyName = invoice.Customer.CustomerCompanyName;
                        inv.CompanyId = invoice.CompanyId;
                        inv.CustomerCode = invoice.Customer.CustomerCode;
                        inv.Account = invoice.Account;
                        inv.Bank = invoice.Bank;

                        List<Models.Detail> details = new List<Models.Detail>();
                        if (invoice.InvoiceDetails.Any())
                        {
                            foreach (var d in invoice.InvoiceDetails)
                            {
                                details.Add(new Models.Detail()
                                {
                                    InvoiceDetailId = d.InvoiceDetailId,
                                    InvoiceId = d.InvoiceId,
                                    CompanyId = d.CompanyId,
                                    DiscountAmount = d.DiscountAmount,
                                    DiscountPercent = d.DiscountPercent,
                                    ItemCode = d.ItemCode,
                                    ItemName = d.ItemName,
                                    Promotion = d.Promotion,
                                    Quantity = d.Quantity,
                                    TotalAmount = d.TotalAmount,
                                    TotalAmountWithoutVat = d.TotalAmountWithoutVat,
                                    UnitCode = d.UnitCode,
                                    UnitName = d.UnitName,
                                    UnitPrice = d.UnitPrice,
                                    VatAmount = d.VatAmount,
                                    VatPercentage = d.VatPercentage
                                });
                            }
                        }
                        inv.Details = details;
                        result.Data = inv;
                    }
                }
            }
            catch (Exception ex)
            {
                result.Code = -1;
                result.Message = ex.Message;
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CreateID()
        {
            return Json(Guid.NewGuid(), JsonRequestBehavior.AllowGet);
        }
    }
}