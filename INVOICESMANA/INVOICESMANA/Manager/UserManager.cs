﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Reflection;
using System.Configuration;
using log4net;
using INVOICESMANA.DATA;
using INVOICESMANA.DATA.Common;

namespace INVOICESMANA.WEB.Manager
{
    public class UserManager : IUserManager
    {
        private readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        // User Login
        public Result Login(string Username, string Password)
        {
            Result result = new Result();
            try
            {
                using (IUnitOfWork unitOfWork = new UnitOfWork())
                {
                    User user = this.GetByName(Username);
                    if (user != null)
                    {
                        var crypto = new SimpleCrypto.PBKDF2();
                        if (user.Status == false)
                        {
                            result.Code = -3;
                            result.Message = "Tài khoản đã ngừng hoạt động!";

                            return result;
                        }
                        if(user.CompanyId != null && unitOfWork.CompaniesRepository.GetById(user.CompanyId).Status == false)
                        {
                            result.Code = -4;
                            result.Message = "Đơn vị đã ngừng hoạt động";
                        }
                        else if (user.Password.ToString() == crypto.Compute(Password, user.PasswordSalt))
                        {
                            result.Code = 0;
                            result.Data = new User
                            {
                                UserId = user.UserId,
                                CompanyId = user.CompanyId,
                                CreatedBy = user.CreatedBy,
                                CreatedDate = user.CreatedDate,
                                Email = user.Email,
                                EmailConfirmed = user.EmailConfirmed,
                                FullName = user.FullName,
                                LastModifiedBy = user.LastModifiedBy,
                                LastModifiedDate = user.LastModifiedDate,
                                UserName = user.UserName
                            };
                        }
                        else
                        {
                            result.Code = -2;
                            result.Message = "Tên đăng nhập hoặc mật khẩu không đúng!";
                        }

                        return result;
                    }
                    else
                    {
                        result.Code = -1;
                        result.Message = "Tài khoản không tồn tại!";
                    }

                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Change user password
        public Result ChangePassword(Guid loginId, string oldPassword, string newPassword)
        {
            Result result = new Result();
            try
            {
                using (IUnitOfWork unitOfWork = new UnitOfWork())
                {
                    User user = unitOfWork.UserRepository.GetById(loginId);
                    if (user != null)
                    {
                        var crypto = new SimpleCrypto.PBKDF2();
                        if (user.Password.ToString() == crypto.Compute(oldPassword, user.PasswordSalt))
                        {
                            unitOfWork.UserRepository.UpdatePassword(user.UserId, crypto.Compute(newPassword), crypto.Salt);
                            result.Code = 0;
                        }
                        else
                        {
                            result.Code = -2;
                            result.Message = "Mật khẩu cũ không đúng!";
                        }
                    }
                    else
                    {
                        result.Code = -1;
                        result.Message = "Tài khoản không tồn tại";
                    }

                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<User> GetAll()
        {
            IUnitOfWork unitOfWork = new UnitOfWork();
            return unitOfWork.UserRepository.GetAll().ToList();
        }

        public List<User> GetAllActiveUsers()
        {
            IUnitOfWork unitOfWork = new UnitOfWork();
            return unitOfWork.UserRepository.GetWhere(x => x.Status == true).OrderBy(x => x.FullName).ToList();
        }

        public Result AddUser(User model)
        {
            Result result = new Result();
            try
            {
                using (IUnitOfWork unitOfWork = new UnitOfWork())
                {
                    var userId = Guid.NewGuid();
                    User user = new User();
                    var crypto = new SimpleCrypto.PBKDF2();
                    user.Password = crypto.Compute(model.Password);
                    user.PasswordSalt = crypto.Salt;
                    user.UserId = userId;
                    user.FullName = model.FullName;
                    user.UserName = model.UserName;
                    user.EmailConfirmed = false;
                    user.Email = model.Email;
                    user.Status = true;
                    user.CreatedBy = this.GetByName(Helper.Identity.UserName).UserId;
                    user.CreatedDate = DateTime.Now;
                    user.CompanyId = model.CompanyId;
                    unitOfWork.UserRepository.Add(user);
                    unitOfWork.SaveChanges();
                    model.UserId = userId;
                    result.Code = 0;
                    result.Data = model;
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                throw ex;
            }
            return result;
        }

        public User GetById(Guid UserId)
        {
            using (IUnitOfWork unitOfWork = new UnitOfWork())
            {
                return unitOfWork.UserRepository.GetById(UserId);
            }
        }

        public Result UpdateUser(User user)
        {
            throw new NotImplementedException();
        }

        public Result Delete(Guid id)
        {
            throw new NotImplementedException();
        }

        public List<RoleModule> GetRoleRights(Guid roleId)
        {
            throw new NotImplementedException();
        }

        public Result UpdateRoleRights(RoleModule model)
        {
            throw new NotImplementedException();
        }

        public User GetByName(string UserName)
        {
            try
            {
                using (IUnitOfWork unitOfWork = new UnitOfWork())
                {
                    var user = unitOfWork.INVOICESdbContext.Users.FirstOrDefault(x => x.UserName == UserName);
                    return user;
                }
            }
            catch (Exception ex)
            {
                _log.Error("GetByName:", ex);
            }
            return null;
        }

        public Result CreateDefaultUser()
        {
            Result result = new Result();
            try
            {
                using (IUnitOfWork unitOfWork = new UnitOfWork())
                {
                    if (unitOfWork.UserRepository.GetWhere(x => x.Status == true).FirstOrDefault() == null)
                    {
                        var userId = Guid.NewGuid();
                        User user = new User();
                        var crypto = new SimpleCrypto.PBKDF2();
                        user.Password = crypto.Compute(System.Configuration.ConfigurationManager.AppSettings["defaultPassword"] ?? "12345678");
                        user.PasswordSalt = crypto.Salt;
                        user.UserId = userId;
                        user.FullName = System.Configuration.ConfigurationManager.AppSettings["defaultFullName"] ?? "Administrator";
                        user.UserName = System.Configuration.ConfigurationManager.AppSettings["defaultUser"] ?? "Admin";
                        user.Email = "defaultEmail@example.com";
                        user.EmailConfirmed = false;
                        user.Status = true;
                        user.CreatedDate = DateTime.Now;
                        user.CompanyId = Guid.Parse("c0a28fe7-14b4-4a31-b8c4-b85504666471");
                        unitOfWork.UserRepository.Add(user);
                        unitOfWork.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                result.Code = -1;
                _log.Error("CreateDefaultUser:", ex);
            }
            return result;
        }

        public Result ChangePassword(string userName, string newPassword)
        {
            Result result = new Result();
            try
            {
                using (IUnitOfWork unitOfWork = new UnitOfWork())
                {
                    User user = unitOfWork.UserRepository.GetWhere(x => x.UserName == userName && x.Status == true).FirstOrDefault();
                    if (user != null)
                    {
                        var crypto = new SimpleCrypto.PBKDF2();
                        user.Password = crypto.Compute(newPassword);
                        user.PasswordSalt = crypto.Salt;
                        user.LastModifiedDate = DateTime.Now;
                        user.LastModifiedBy = user.UserId;
                        unitOfWork.SaveChanges();
                    }else
                    {
                        result.Code = -1;
                        result.Message = "Tài khoản không tồn tại hoặc đã bị khóa";
                    }
                }
            }
            catch (Exception ex)
            {
                result.Code = -1;
                _log.Error("ChangePassword:", ex);
            }
            return result;
        }
    }
}
