﻿using System;
using System.Collections.Generic;
using INVOICESMANA.DATA;
using INVOICESMANA.DATA.Common;

namespace INVOICESMANA.WEB.Manager
{
    public interface IUserManager
    {
        User GetById(Guid UserId);
        User GetByName(string UserName);
        Result Login(string username, string password);
        Result ChangePassword(string userName, string newPassword);
        List<User> GetAll();
        List<User> GetAllActiveUsers();
        Result AddUser(User user);
        Result UpdateUser(User user);
        Result Delete(Guid id);
        List<RoleModule> GetRoleRights(Guid roleId);
        Result UpdateRoleRights(RoleModule model);
        Result CreateDefaultUser();
    }
}
