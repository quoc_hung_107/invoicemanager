﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(INVOICESMANA.Startup))]
namespace INVOICESMANA
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
