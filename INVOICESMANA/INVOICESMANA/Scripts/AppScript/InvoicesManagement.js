﻿var InvoicesManagement = function () {
    var retry = 0;
    var retryTimes = 3;
    const columnDefined = {
        ItemCode: 0,
        ItemName: 1,
        UnitName: 2,
        Quantity: 3,
        UnitPrice: 4,
        TotalAmountWithoutVat: 5,
        DiscountPercent: 6,
        DiscountAmount: 7,
        VatPercentage: 8,
        VatAmount: 9,
        TotalAmount: 10,
        InvoiceDetailId: 11
    }

    var editor;
    var self = this;
    var VAT_data = [];
    var paymentType_data = [];
    var table;
    var Customer = [];
    var companyIdWork = '';
    var listTable = undefined;
    self.initList = function () {
        Cookies.set('company', $("#CompanyId").val());
        LoadData();
    }

    self.initDetail = function () {
        companyIdWork = Cookies.get('company');

        Utilities.getInvoiceType(Cookies.get('company')).success(function (data) {
            if (data.error) {
                $.notification.show('error', data.Message);
            }
            else {
                var invoiceTypes = JSON.parse(data);
                window.invoiceType = {};
                _.forEach(invoiceTypes, function (item) {
                    if (!window.invoiceType[item.mau_so]) {
                        window.invoiceType[item.mau_so] = []
                    }

                    var invoiceSeri = {};
                    invoiceSeri.id = item.id;
                    invoiceSeri.ky_hieu = item.ky_hieu;

                    window.invoiceType[item.mau_so].push(invoiceSeri);
                })

                appendToInvoiceType(invoiceTypes);
            }
        });

        $("#InvoiceType").unbind().change(function () {
            appendToInvoiceSeries($(this).val());
        });

        $("#PaymentId").unbind().change(function () {
            var value = $(this).val();
            if (value == "812ef2f2-09eb-4ad6-83cf-923e5d10a874" || value == "ae5539e0-b198-4ae0-8cb4-eb2218321adf") {
                $("#payment").show();
            }
            else {
                $("#payment").hide();
            }
        });

        initAutoComplete();

        getCombobox(function (data) {
            if (data.Code === 0) {
                VAT_data = data.Data.VAT;
                paymentType_data = data.Data.PaymentType;
            }
            renderPaymentType();

            var optionVAT = VAT_data.map(function (item) {
                return { value: item.Value, display: item.Text };
            })

            table = $('#InvoiceDetail').DataTable({
                "paging": false,
                "searching": false,
                "info": false,
                "ordering": false,
                "fnCreatedRow": function (dom, data, row) {
                    $(dom.cells[dom.cells.length - 1]).hide();
                    $(dom).attr('title', 'Click vào ô cuối cùng để chọn dòng')
                },
                "oLanguage": $.extend(Config.LanguageDataTable, Config.LanguageDataInvoiceTable)
            });

            table.MakeCellsEditable({
                "onUpdate": myCallbackFunction,
                "columns": [0, 1, 2, 3, 4, 6, 7, 8, 9],
                "inputCss": "form-control",
                "inputTypes": [
                    {
                        "column": 0,
                        "type": "text",
                        "options": null
                    },
                    {
                        "column": 8,
                        "type": "list",
                        "options": optionVAT,
                    }
                ]
            });

            $('#ContractDate').datetimepicker({
                viewMode: 'days',
                format: 'DD/MM/YYYY',
                showTodayButton: true,
                showClose: true,
                tooltips: Config.TooltipDateTimePicker,
                locale: 'vi',
                icons: Config.DateTimePickerIcons
            });

            // get Date => $('#ContractDate').data('date')


            $('#ExportInvoice').unbind().bind('click', function () {
                var error = 0
                if (!checkValidForm()) {
                    $.notification.show('error', 'Vui lòng nhập đầy đủ thông tin!');
                    return;
                }

                if (validateVietnameseName($('#CustomerAccountNumber').val().trim()) || validateVietnameseName($('#Account').val().trim())) {
                    $.notification.show('error', 'Tài khoản ngân hàng không được nhập dấu');
                    return;
                }

                var value = $("#PaymentId").val();

                if (value == "812ef2f2-09eb-4ad6-83cf-923e5d10a874" || value == "ae5539e0-b198-4ae0-8cb4-eb2218321adf") {
                    if (($("#Account").val().trim() == undefined || $("#Account").val().trim() == null || $("#Account").val().trim() == '') ||
                        ($("#Bank").val().trim() == undefined || $("#Bank").val().trim() == null || $("#Bank").val().trim() == '')) {
                        $.notification.show('error', 'Bạn phải nhập đủ thông tin Tài Khoản và Ngân hàng.');
                        return;
                    }
                }

                var formData = getFormData();

                if (!formData) {
                    return;
                }

                var dataSend = getTemplateData();
                dataSend.data[0].details[0].data = [];
                formData.Details.forEach(function (item, index) {
                    if (!validateInterger(item.Quantity) || !validateInterger(item.UnitPrice)) {
                        error++;
                        $.notification.show('error', 'Số lượng hoặc đơn giá phải là dạng số.');
                        return;
                    }

                    if (item.TotalAmount > 0) {
                        dataSend.data[0].details[0].data.push({
                            "inv_InvoiceAuthDetail_id": item.InvoiceDetailId,
                            "inv_InvoiceAuth_id": formData.InvoiceId,
                            "stt_rec0": formatStt(index + 1),
                            "inv_itemCode": item.ItemCode,
                            "inv_itemName": item.ItemName,
                            "inv_unitCode": "",
                            "inv_unitName": item.UnitName,
                            "inv_unitPrice": item.UnitPrice,
                            "inv_quantity": item.Quantity,
                            "inv_TotalAmountWithoutVat": item.TotalAmountWithoutVat,
                            "inv_vatPercentage": item.VatPercentage,
                            "inv_vatAmount": item.VatAmount,
                            "inv_TotalAmount": item.TotalAmount,
                            "inv_promotion": false,
                            "inv_discountPercentage": item.DiscountPercent,
                            "inv_discountAmount": item.DiscountAmount,
                            "ma_thue": item.VatPercentage
                        });
                    }
                    else if (item.TotalAmount <= 0) {
                        error++;
                        $.notification.show('error', 'Thành tiền phải dương không được âm hoặc bằng  0');
                        return;
                    }
                });

                dataSend.data[0]['inv_InvoiceAuth_id'] = formData.InvoiceId;
                dataSend.data[0]['inv_InvoiceCode_id'] = $('#InvoiceSeries').val();
                dataSend.data[0]['inv_invoiceType'] = $('#InvoiceType').val();
                dataSend.data[0]['inv_invoiceSeries'] = $('#InvoiceSeries option:selected').text();
                dataSend.data[0]['inv_contractNumber'] = $('#ContractNumber').val();
                dataSend.data[0]['paymentId'] = $('#PaymentId').val();
                dataSend.data[0]['orderNumber'] = $('#OrderNumber').val();
                dataSend.data[0]['inv_buyerAddressLine'] = formData.CustomerAddress;
                dataSend.data[0]['inv_buyerBankAccount'] = $('#CustomerAccountNumber').val();
                dataSend.data[0]['inv_buyerBankName'] = $('#CustomerBank').val();
                dataSend.data[0]['inv_buyerDisplayName'] = formData.CustomerFullName;
                dataSend.data[0]['inv_buyerEmail'] = formData.CustomerEmail;
                dataSend.data[0]['inv_buyerLegalName'] = formData.CustomerCompanyName;
                dataSend.data[0]['inv_buyerTaxCode'] = formData.CustomerTaxIdentification;
                dataSend.data[0]['inv_paymentMethodName'] = $('#PaymentId option:selected').text();
                dataSend.data[0]['ma_dt'] = formData.CustomerCode;
                dataSend.data[0]['inv_sellerBankAccount'] = formData.Account;
                dataSend.data[0]['inv_sellerBankName'] = formData.Bank;
                dataSend.data[0]['inv_invoiceIssuedDate'] = moment(formData.ContractDate, "DD/MM/YYYY").format("YYYY-MM-DD");
                dataSend.companyId = Cookies.get('company');

                if (error == 0) {
                    AddInvoice(dataSend);
                }
               
            })

            $('#UpdateIdentificationInvoice').unbind().bind('click', function () {
                var error = 0;
                if (!checkValidForm()) {
                    $.notification.show('error', 'Vui lòng nhập đầy đủ thông tin!');
                    return;
                }

                if (validateVietnameseName($('#CustomerAccountNumber').val().trim()) || validateVietnameseName($('#Account').val().trim())) {
                    $.notification.show('error', 'Tài khoản ngân hàng không được nhập dấu');
                    return;
                }

                var value = $("#PaymentId").val();
                if (value == "812ef2f2-09eb-4ad6-83cf-923e5d10a874" || value == "ae5539e0-b198-4ae0-8cb4-eb2218321adf") {
                    if (($("#Account").val().trim() == undefined || $("#Account").val().trim() == null || $("#Account").val().trim() == '') ||
                        ($("#Bank").val().trim() == undefined || $("#Bank").val().trim() == null || $("#Bank").val().trim() == '')) {
                        $.notification.show('error', 'Bạn phải nhập đủ thông tin Tài Khoản và Ngân hàng.');
                        return;
                    }
                }

                var formData = getFormData();

                if (!formData) {
                    return;
                }

                var dataSend = getTemplateData();

                dataSend.data[0].details[0].data = [];

                formData.Details.forEach(function (item, index) {
                    if (!validateInterger(item.Quantity) || !validateInterger(item.UnitPrice)) {
                        error++;
                        $.notification.show('error', 'Số lượng hoặc đơn giá phải là dạng số.');
                        return;
                    }

                    if (item.TotalAmount > 0) {
                        dataSend.data[0].details[0].data.push({
                            "inv_InvoiceAuthDetail_id": item.InvoiceDetailId,
                            "inv_InvoiceAuth_id": formData.InvoiceId,
                            "stt_rec0": formatStt(index + 1),
                            "inv_itemCode": item.ItemCode,
                            "inv_itemName": item.ItemName,
                            "inv_unitCode": "",
                            "inv_unitName": item.UnitName,
                            "inv_unitPrice": item.UnitPrice,
                            "inv_quantity": item.Quantity,
                            "inv_TotalAmountWithoutVat": item.TotalAmountWithoutVat,
                            "inv_vatPercentage": item.VatPercentage,
                            "inv_vatAmount": item.VatAmount,
                            "inv_TotalAmount": item.TotalAmount,
                            "inv_promotion": false,
                            "inv_discountPercentage": item.DiscountPercent,
                            "inv_discountAmount": item.DiscountAmount,
                            "ma_thue": "0"
                        });
                    }
                    else if (item.TotalAmount <= 0) {
                        error++;
                        $.notification.show('error', 'Thành tiền phải dương không được âm hoặc bằng  0');
                        return;
                    }
                });

                dataSend.data[0]['inv_InvoiceAuth_id'] = formData.InvoiceId;
                dataSend.data[0]['inv_InvoiceCode_id'] = $('#InvoiceSeries').val();
                dataSend.data[0]['inv_invoiceType'] = $('#InvoiceType').val();
                dataSend.data[0]['inv_invoiceSeries'] = $('#InvoiceSeries option:selected').text();
                dataSend.data[0]['inv_contractNumber'] = $('#ContractNumber').val();
                dataSend.data[0]['paymentId'] = $('#PaymentId').val();
                dataSend.data[0]['orderNumber'] = $('#OrderNumber').val();
                dataSend.data[0]['inv_buyerAddressLine'] = formData.CustomerAddress;
                dataSend.data[0]['inv_buyerBankAccount'] = $('#CustomerAccountNumber').val();
                dataSend.data[0]['inv_buyerBankName'] = $('#CustomerBank').val();
                dataSend.data[0]['inv_buyerDisplayName'] = formData.CustomerFullName;
                dataSend.data[0]['inv_buyerEmail'] = formData.CustomerEmail;
                dataSend.data[0]['inv_buyerLegalName'] = formData.CustomerCompanyName;
                dataSend.data[0]['inv_buyerTaxCode'] = formData.CustomerTaxIdentification;
                dataSend.data[0]['inv_paymentMethodName'] = $('#PaymentId option:selected').text();
                dataSend.data[0]['ma_dt'] = formData.CustomerCode;
                dataSend.data[0]['inv_sellerBankAccount'] = formData.Account;
                dataSend.data[0]['inv_sellerBankName'] = formData.Bank;
                dataSend.data[0]['inv_invoiceIssuedDate'] = moment(formData.ContractDate, "DD/MM/YYYY").format("YYYY-MM-DD");
                dataSend.companyId = Cookies.get('company');

                if (error == 0) {
                    UpdateInvoice(dataSend);
                }
            })

            $('#UpdateLowInvoice').unbind().bind('click', function () {
                var error = 0;
                if (!checkValidForm()) {
                    $.notification.show('error', 'Vui lòng nhập đầy đủ thông tin!');
                    return;
                }

                if (validateVietnameseName($('#CustomerAccountNumber').val().trim()) || validateVietnameseName($('#Account').val().trim())) {
                    $.notification.show('error', 'Tài khoản ngân hàng không được nhập dấu');
                    return;
                }

                var value = $("#PaymentId").val();
                if (value == "812ef2f2-09eb-4ad6-83cf-923e5d10a874" || value == "ae5539e0-b198-4ae0-8cb4-eb2218321adf") {
                    if (($("#Account").val().trim() == undefined || $("#Account").val().trim() == null || $("#Account").val().trim() == '') ||
                        ($("#Bank").val().trim() == undefined || $("#Bank").val().trim() == null || $("#Bank").val().trim() == '')) {
                        $.notification.show('error', 'Bạn phải nhập đủ thông tin Tài Khoản và Ngân hàng.');
                        return;
                    }
                }

                var formData = getFormData();

                if (!formData) {
                    return;
                }

                var dataSend = getTemplateData();

                dataSend.data[0].details[0].data = [];
                formData.Details.forEach(function (item, index) {
                    if (!validateInterger(item.Quantity) || !validateInterger(item.UnitPrice)) {
                        error++;
                        $.notification.show('error', 'Số lượng hoặc đơn giá phải là dạng số.');
                        return;
                    }
                    if (item.TotalAmount > 0) {
                        dataSend.data[0].details[0].data.push({
                            "inv_InvoiceAuthDetail_id": item.InvoiceDetailId,
                            "inv_InvoiceAuth_id": formData.InvoiceId,
                            "stt_rec0": formatStt(index + 1),
                            "inv_itemCode": item.ItemCode,
                            "inv_itemName": item.ItemName,
                            "inv_unitCode": "",
                            "inv_unitName": item.UnitName,
                            "inv_unitPrice": item.UnitPrice,
                            "inv_quantity": item.Quantity,
                            "inv_TotalAmountWithoutVat": item.TotalAmountWithoutVat,
                            "inv_vatPercentage": item.VatPercentage,
                            "inv_vatAmount": item.VatAmount,
                            "inv_TotalAmount": item.TotalAmount,
                            "inv_promotion": false,
                            "inv_discountPercentage": item.DiscountPercent,
                            "inv_discountAmount": item.DiscountAmount,
                            "ma_thue": "0"
                        });
                    }
                    else if (item.TotalAmount <= 0) {
                        error++;
                        $.notification.show('error', 'Thành tiền phải dương không được âm hoặc bằng  0');
                        return;
                    }
                });

                dataSend.data[0]['inv_InvoiceAuth_id'] = formData.InvoiceId;
                dataSend.data[0]['inv_InvoiceCode_id'] = $('#InvoiceSeries').val();
                dataSend.data[0]['inv_invoiceType'] = $('#InvoiceType').val();
                dataSend.data[0]['inv_invoiceSeries'] = $('#InvoiceSeries option:selected').text();
                dataSend.data[0]['inv_buyerAddressLine'] = formData.CustomerAddress;
                dataSend.data[0]['inv_buyerBankAccount'] = $('#CustomerAccountNumber').val();
                dataSend.data[0]['inv_buyerBankName'] = $('#CustomerBank').val();
                dataSend.data[0]['inv_buyerDisplayName'] = formData.CustomerFullName;
                dataSend.data[0]['inv_buyerEmail'] = formData.CustomerEmail;
                dataSend.data[0]['inv_buyerLegalName'] = formData.CustomerCompanyName;
                dataSend.data[0]['inv_buyerTaxCode'] = formData.CustomerTaxIdentification;
                dataSend.data[0]['inv_paymentMethodName'] = $('#PaymentId option:selected').text();
                dataSend.data[0]['ma_dt'] = formData.CustomerCode;
                dataSend.data[0]['inv_sellerBankAccount'] = formData.Account;
                dataSend.data[0]['inv_sellerBankName'] = formData.Bank;
                dataSend.data[0]['paymentId'] = $('#PaymentId').val();
                dataSend.data[0]['orderNumber'] = $('#OrderNumber').val();
                dataSend.data[0]['inv_invoiceIssuedDate'] = moment(formData.ContractDate, "DD/MM/YYYY").format("YYYY-MM-DD");
                dataSend.companyId = Cookies.get('company');

                if (error == 0) {
                    UpdateLowInvoice(dataSend);
                }
            })

            $('#UpdateHighInvoice').unbind().bind('click', function () {
                var error = 0;
                if (!checkValidForm()) {
                    $.notification.show('error', 'Vui lòng nhập đầy đủ thông tin!');
                    return;
                }

                if (validateVietnameseName($('#CustomerAccountNumber').val().trim()) || validateVietnameseName($('#Account').val().trim())) {
                    $.notification.show('error', 'Tài khoản ngân hàng không được nhập dấu');
                    return;
                }

                var value = $("#PaymentId").val();
                if (value == "812ef2f2-09eb-4ad6-83cf-923e5d10a874" || value == "ae5539e0-b198-4ae0-8cb4-eb2218321adf") {
                    if (($("#Account").val().trim() == undefined || $("#Account").val().trim() == null || $("#Account").val().trim() == '') ||
                        ($("#Bank").val().trim() == undefined || $("#Bank").val().trim() == null || $("#Bank").val().trim() == '')) {
                        $.notification.show('error', 'Bạn phải nhập đủ thông tin Tài Khoản và Ngân hàng.');
                        return;
                    }
                }

                var formData = getFormData();

                if (!formData) {
                    return;
                }

                var dataSend = getTemplateData();

                dataSend.data[0].details[0].data = [];
                formData.Details.forEach(function (item, index) {
                    if (!validateInterger(item.Quantity) || !validateInterger(item.UnitPrice)) {
                        error++;
                        $.notification.show('error', 'Số lượng hoặc đơn giá phải là dạng số.');
                        return;
                    }

                    if (item.TotalAmount > 0) {
                        dataSend.data[0].details[0].data.push({
                            "inv_InvoiceAuthDetail_id": item.InvoiceDetailId,
                            "inv_InvoiceAuth_id": formData.InvoiceId,
                            "stt_rec0": formatStt(index + 1),
                            "inv_itemCode": item.ItemCode,
                            "inv_itemName": item.ItemName,
                            "inv_unitCode": "",
                            "inv_unitName": item.UnitName,
                            "inv_unitPrice": item.UnitPrice,
                            "inv_quantity": item.Quantity,
                            "inv_TotalAmountWithoutVat": item.TotalAmountWithoutVat,
                            "inv_vatPercentage": item.VatPercentage,
                            "inv_vatAmount": item.VatAmount,
                            "inv_TotalAmount": item.TotalAmount,
                            "inv_promotion": false,
                            "inv_discountPercentage": item.DiscountPercent,
                            "inv_discountAmount": item.DiscountAmount,
                            "ma_thue": "0"
                        });
                    }
                    else if (item.TotalAmount <= 0) {
                        error++;
                        $.notification.show('error', 'Thành tiền phải dương không được âm hoặc bằng  0');
                        return;
                    }
                });

                dataSend.data[0]['inv_InvoiceAuth_id'] = formData.InvoiceId;
                dataSend.data[0]['inv_InvoiceCode_id'] = $('#InvoiceSeries').val();
                dataSend.data[0]['inv_invoiceType'] = $('#InvoiceType').val();
                dataSend.data[0]['inv_invoiceSeries'] = $('#InvoiceSeries option:selected').text();
                dataSend.data[0]['inv_buyerAddressLine'] = formData.CustomerAddress;
                dataSend.data[0]['inv_buyerBankAccount'] = $('#CustomerAccountNumber').val();
                dataSend.data[0]['inv_buyerBankName'] = $('#CustomerBank').val();
                dataSend.data[0]['inv_buyerDisplayName'] = formData.CustomerFullName;
                dataSend.data[0]['inv_buyerEmail'] = formData.CustomerEmail;
                dataSend.data[0]['inv_buyerLegalName'] = formData.CustomerCompanyName;
                dataSend.data[0]['inv_buyerTaxCode'] = formData.CustomerTaxIdentification;
                dataSend.data[0]['inv_paymentMethodName'] = $('#PaymentId option:selected').text();
                dataSend.data[0]['ma_dt'] = formData.CustomerCode;
                dataSend.data[0]['inv_sellerBankAccount'] = formData.Account;
                dataSend.data[0]['inv_sellerBankName'] = formData.Bank;
                dataSend.data[0]['paymentId'] = $('#PaymentId').val();
                dataSend.data[0]['orderNumber'] = $('#OrderNumber').val();
                dataSend.data[0]['inv_invoiceIssuedDate'] = moment(formData.ContractDate, "DD/MM/YYYY").format("YYYY-MM-DD");
                dataSend.companyId = Cookies.get('company');

                if (error == 0) {
                    UpdateHighInvoice(dataSend);
                }
            })

            $('#ChangeInvoice').unbind().bind('click', function () {
                var error = 0;
                if (!checkValidForm()) {
                    $.notification.show('error', 'Vui lòng nhập đầy đủ thông tin!');
                    return;
                }

                if (validateVietnameseName($('#CustomerAccountNumber').val().trim()) || validateVietnameseName($('#Account').val().trim())) {
                    $.notification.show('error', 'Tài khoản ngân hàng không được nhập dấu');
                    return;
                }

                var value = $("#PaymentId").val();
                if (value == "812ef2f2-09eb-4ad6-83cf-923e5d10a874" || value == "ae5539e0-b198-4ae0-8cb4-eb2218321adf") {
                    if (($("#Account").val().trim() == undefined || $("#Account").val().trim() == null || $("#Account").val().trim() == '') ||
                        ($("#Bank").val().trim() == undefined || $("#Bank").val().trim() == null || $("#Bank").val().trim() == '')) {
                        $.notification.show('error', 'Bạn phải nhập đủ thông tin Tài Khoản và Ngân hàng.');
                        return;
                    }
                }

                var formData = getFormData();

                if (!formData) {
                    return;
                }

                var dataSend = getTemplateData();

                dataSend.data[0].details[0].data = [];
                formData.Details.forEach(function (item, index) {
                    if (!validateInterger(item.Quantity) || !validateInterger(item.UnitPrice)) {
                        error++;
                        $.notification.show('error', 'Số lượng hoặc đơn giá phải là dạng số.');
                        return;
                    }

                    if (item.TotalAmount > 0) {
                        dataSend.data[0].details[0].data.push({
                            "inv_InvoiceAuthDetail_id": item.InvoiceDetailId,
                            "inv_InvoiceAuth_id": formData.InvoiceId,
                            "stt_rec0": formatStt(index + 1),
                            "inv_itemCode": item.ItemCode,
                            "inv_itemName": item.ItemName,
                            "inv_unitCode": "",
                            "inv_unitName": item.UnitName,
                            "inv_unitPrice": item.UnitPrice,
                            "inv_quantity": item.Quantity,
                            "inv_TotalAmountWithoutVat": item.TotalAmountWithoutVat,
                            "inv_vatPercentage": item.VatPercentage,
                            "inv_vatAmount": item.VatAmount,
                            "inv_TotalAmount": item.TotalAmount,
                            "inv_promotion": false,
                            "inv_discountPercentage": item.DiscountPercent,
                            "inv_discountAmount": item.DiscountAmount,
                            "ma_thue": "0"
                        });
                    }
                    else if (item.TotalAmount <= 0) {
                        error++;
                        $.notification.show('error', 'Thành tiền phải dương không được âm hoặc bằng  0');
                        return;
                    }
                });

                dataSend.data[0]['inv_InvoiceAuth_id'] = formData.InvoiceId;
                dataSend.data[0]['inv_InvoiceCode_id'] = $('#InvoiceSeries').val();
                dataSend.data[0]['inv_invoiceType'] = $('#InvoiceType').val();
                dataSend.data[0]['inv_invoiceSeries'] = $('#InvoiceSeries option:selected').text();
                dataSend.data[0]['inv_buyerAddressLine'] = formData.CustomerAddress;
                dataSend.data[0]['inv_buyerBankAccount'] = $('#CustomerAccountNumber').val();
                dataSend.data[0]['inv_buyerBankName'] = $('#CustomerBank').val();
                dataSend.data[0]['inv_buyerDisplayName'] = formData.CustomerFullName;
                dataSend.data[0]['inv_buyerEmail'] = formData.CustomerEmail;
                dataSend.data[0]['inv_buyerLegalName'] = formData.CustomerCompanyName;
                dataSend.data[0]['inv_buyerTaxCode'] = formData.CustomerTaxIdentification;
                dataSend.data[0]['inv_paymentMethodName'] = $('#PaymentId option:selected').text();
                dataSend.data[0]['ma_dt'] = formData.CustomerCode;
                dataSend.data[0]['inv_sellerBankAccount'] = formData.Account;
                dataSend.data[0]['inv_sellerBankName'] = formData.Bank;
                dataSend.data[0]['paymentId'] = $('#PaymentId').val();
                dataSend.data[0]['orderNumber'] = $('#OrderNumber').val();
                dataSend.data[0]['inv_invoiceIssuedDate'] = moment(formData.ContractDate, "DD/MM/YYYY").format("YYYY-MM-DD");
                dataSend.companyId = Cookies.get('company');

                if (error == 0) {
                    ChangeInvoice(dataSend);
                }
            })

            $('#DeleteInvoice').unbind().bind('click', function () {
                if (!checkValidForm()) {
                    $.notification.show('error', 'Vui lòng nhập đầy đủ thông tin!');
                    return;
                }

                if (validateVietnameseName($('#CustomerAccountNumber').val().trim()) || validateVietnameseName($('#Account').val().trim())) {
                    $.notification.show('error', 'Tài khoản ngân hàng không được nhập dấu');
                    return;
                }

                if (confirm("Bạn có chắc chắn muốn hủy hóa đơn này?")) {
                    var formData = getFormData();

                    if (!formData) {
                        return;
                    }

                    var dataSend = getTemplateData();

                    dataSend.data[0].details[0].data = [];
                    formData.Details.forEach(function (item, index) {
                        if (item.TotalAmount > 0) {
                            dataSend.data[0].details[0].data.push({
                                "inv_InvoiceAuthDetail_id": item.InvoiceDetailId,
                                "inv_InvoiceAuth_id": formData.InvoiceId,
                                "stt_rec0": formatStt(index + 1),
                                "inv_itemCode": item.ItemCode,
                                "inv_itemName": item.ItemName,
                                "inv_unitCode": "",
                                "inv_unitName": item.UnitName,
                                "inv_unitPrice": item.UnitPrice,
                                "inv_quantity": item.Quantity,
                                "inv_TotalAmountWithoutVat": item.TotalAmountWithoutVat,
                                "inv_vatPercentage": item.VatPercentage,
                                "inv_vatAmount": item.VatAmount,
                                "inv_TotalAmount": item.TotalAmount,
                                "inv_promotion": false,
                                "inv_discountPercentage": item.DiscountPercent,
                                "inv_discountAmount": item.DiscountAmount,
                                "ma_thue": "0"
                            });
                        }
                    });

                    dataSend.data[0]['inv_InvoiceAuth_id'] = formData.InvoiceId;
                    dataSend.data[0]['inv_InvoiceCode_id'] = $('#InvoiceSeries').val();
                    dataSend.data[0]['inv_invoiceType'] = $('#InvoiceType').val();
                    dataSend.data[0]['inv_invoiceSeries'] = $('#InvoiceSeries option:selected').text();
                    dataSend.data[0]['inv_buyerAddressLine'] = formData.CustomerAddress;
                    dataSend.data[0]['inv_buyerBankAccount'] = $('#CustomerAccountNumber').val();
                    dataSend.data[0]['inv_buyerBankName'] = $('#CustomerBank').val();
                    dataSend.data[0]['inv_buyerDisplayName'] = formData.CustomerFullName;
                    dataSend.data[0]['inv_buyerEmail'] = formData.CustomerEmail;
                    dataSend.data[0]['inv_buyerLegalName'] = formData.CustomerCompanyName;
                    dataSend.data[0]['inv_buyerTaxCode'] = formData.CustomerTaxIdentification;
                    dataSend.data[0]['inv_paymentMethodName'] = $('#PaymentId option:selected').text();
                    dataSend.data[0]['ma_dt'] = formData.CustomerCode;
                    dataSend.data[0]['inv_sellerBankAccount'] = formData.Account;
                    dataSend.data[0]['inv_sellerBankName'] = formData.Bank;
                    dataSend.data[0]['inv_invoiceIssuedDate'] = moment(formData.ContractDate, "DD/MM/YYYY").format("YYYY-MM-DD");
                    dataSend.companyId = Cookies.get('company');
                    DeleteInvoice(dataSend);
                }
            })

            if (isEditMode() == true) {
                Utilities.Loader.show();
                getDetail($('#InvoiceId').val(), function (data) {
                    Utilities.Loader.hide();
                    if (data.Code === 0) {
                        fillData(data.Data);
                    } else {
                        $.notification.show('error', data.Message);
                    }
                });
                $('#ExportInvoice').hide();
                $('#UpdateIdentificationInvoice').show();
                $('#UpdateHighInvoice').show();
                $('#UpdateLowInvoice').show();
                $('#ChangeInvoice').show();
                $('#DeleteInvoice').show();
            }
            else {
                $('#ExportInvoice').show();
                $('#UpdateIdentificationInvoice').hide();
                $('#UpdateHighInvoice').hide();
                $('#UpdateLowInvoice').hide();
                $('#ChangeInvoice').hide();
                $('#DeleteInvoice').hide();
            }

            $('#InvoiceDetail tbody').on('click', 'tr', function (e) {
                if (e.offsetX > this.offsetWidth) {
                    table.row('.selected').remove().draw(false);
                    calcTotal();
                }

                var target = e.toElement;
                if (target.tagName != 'TD') {
                    target = $(e.toElement).parents('TD')[0];
                }

                if (target) {
                    if ([0, 1, 2, 3, 4, 6, 7, 8, 9].indexOf(target.cellIndex) === -1) {
                        if ($(this).hasClass('selected')) {
                            $(this).removeClass('selected');
                        }
                        else {
                            table.$('tr.selected').removeClass('selected');
                            $(this).addClass('selected');
                        }
                    }
                }
            });
        })
    }

    function getCombobox(callback) {
        $.ajax({
            type: "GET",
            data: {},
            url: Config.AppUrl + '/Invoice/getCombobox/',
            success: function (data) {
                if (callback) {
                    callback(data);
                }
            }
        })
    }

    function LoadData() {
        Utilities.Loader.show();
        if (!listTable) {
            listTable = $('#ListInvoiceTable').DataTable({
                "serverSide": true,
                "processing": true,
                "paging": true,
                "searching": true,
                "lengthChange": false,
                "pageLength": 10,
                "sAjaxSource": Config.AppUrl + '/Invoice/getListInvoice',
                "sAjaxDataProp": "data",
                "fnServerData": function (sSource, aoData, fnCallback) {
                    aoData.push({ "name": "companyId", value: Cookies.get('company') });
                    $.ajax({
                        "type": "GET",
                        "dataType": 'json',
                        "contentType": "application/json; charset=utf-8",
                        "url": sSource,
                        "data": aoData,
                        "success": function (data) {
                            fnCallback(data);
                        }
                    });
                },
                "order": [[0, "desc"]],
                "aoColumns": [
                    {
                        "mData": "InvoiceNumber"
                    },
                    {
                        "mData": "OrderNumber"
                    },
                    {
                        "mData": "ContractDate",
                        "mRender": function (data, type, row) {
                            if (data) {
                                return new Date(parseInt(data.replace(/\D/g, ""))).toLocaleDateString()
                            }
                            return '';
                        },
                        "sClass": "align-right"
                    },
                    {
                        "mData": "CustomerName"
                    },
                    {
                        "mData": "CompanyName"
                    },
                    {
                        "mData": "Address"
                    },
                    {
                        "mData": "Email"
                    },
                    {
                        "mData": "TotalAmount",
                        "mRender": function (data, type, row) {
                            return data.format(0, 3, ',');
                        }
                    },
                    {
                        "mData": "PaymentType"
                    },
                    {
                        "mData": "InvoiceId",
                        "mRender": function (data, type, row) {
                            return '<a class="pull-left" href="' + Config.AppUrl + '/Invoice/Detail?id=' + data + '">Chi tiết</a>' +
                                '<a class="pull-right download-pdf" data-id="' + data + '" data-pdf="' + data + '.pdf" href="#">PDF</a>'
                        },
                        "sWidth": "110px",
                        "orderable": false
                    }],
                "oLanguage": {
                    "sProcessing": "Đang xử lý...",
                    "sLengthMenu": "Hiển thị _MENU_ bản ghi trong 1 trang",
                    "sZeroRecords": "Chưa có dữ liệu",
                    "sInfo": "Hiển thị _START_ - _END_ (trong _TOTAL_ bản ghi)",
                    "sInfoEmpty": "Hiển thị 0 - 0 (trong 0 bản ghi)",
                    "sInfoFiltered": "(lọc từ _MAX_ bản ghi)",
                    "sInfoPostFix": "",
                    "sSearch": "Tìm:",
                    "sSearchPlaceholder": "Số hóa đơn, người mua, tên đơn vị, số đơn hàng",
                    "sUrl": "",
                    "oPaginate": {
                        "sFirst": "Đầu",
                        "sPrevious": "Trước",
                        "sNext": "Tiếp",
                        "sLast": "Cuối"
                    }
                }
            }).on('click', function (evt) {
                if ($(evt.target).data("pdf")) {
                    var data = $(evt.target).data("pdf");
                    var id = $(evt.target).data("id");
                    checkExistPDF(id).success(function (result) {
                        if (result.Code == 0) {
                            window.open(window.UrlConfig.AppUrl + 'LocalPDF/' + data, '_blank');
                        }
                        else {
                            $.notification.show('error', result.Message);
                        }
                    }).error(function (result) {
                        $.notification.show('error', result.Message);
                    });
                }
            });
        }
        else {
            listTable.draw();
        }
        Utilities.Loader.hide();
    }

    function checkExistPDF(data) {
        return $.ajax({
            type: 'GET',
            url: window.UrlConfig.AppUrl + 'api/Invoice/CheckExistPDF',
            data: { id: data }
        });
    }

    function myCallbackFunction(updatedCell, updatedRow, oldValue) {
        calcAmount(updatedCell, updatedRow, oldValue)
    }

    $('#AddDetail').unbind().bind('click', function () {
        Utilities.createGuid().success(function (data) {
            var rowNode = table
                .row.add(['&nbsp;', '&nbsp;', '&nbsp;', '&nbsp;', '&nbsp;', '&nbsp;', '&nbsp;', ' ', '&nbsp;', '&nbsp;', '&nbsp;', data])
                .draw()
                .node();
            $(rowNode)
                .css('color', 'black')
                .animate({ color: 'black' });
        })
    });

    function calcAmount(updatedCell, updatedRow, oldValue) {
        var rowData = updatedRow.data();
        var SoLuong = isNaN(parseInt(rowData[3])) ? 0 : parseInt(rowData[3]);
        var DonGia = isNaN(parseFloat(rowData[4])) ? 0 : parseFloat(rowData[4]);
        var Tien = SoLuong * DonGia;
        var CK = isNaN(parseFloat(rowData[6])) ? "" : parseFloat(rowData[6]);
        var TienCK = 0;
        if (!isNaN(parseFloat(rowData[6]))) {
            TienCK = (Tien * CK) / 100;
        } else {
            TienCK = (isNaN(parseFloat(rowData[7])) || rowData[7] == 0) ? 0 : parseFloat(rowData[7].replace(/,/gi, ""));
        }
        var Thue = isNaN(parseFloat(rowData[8])) ? 0 : parseFloat(rowData[8]);
        var Tien_Thue = ((Tien - TienCK) * Thue) / 100;
        var ThanhTien = Tien - TienCK + Tien_Thue;
        $($(table.body()).find('tr').eq(updatedCell.index().row).find('td').eq(5)).text(Tien.format(0, 3, ','));
        $($(table.body()).find('tr').eq(updatedCell.index().row).find('td').eq(7).find('input')).val(TienCK);
        $($(table.body()).find('tr').eq(updatedCell.index().row).find('td').eq(9).find('input')).val(Tien_Thue);
        $($(table.body()).find('tr').eq(updatedCell.index().row).find('td').eq(10)).text(ThanhTien.format(0, 3, ','));
        rowData[5] = Tien.format(0, 3, ',');
        if (TienCK != null && TienCK != "" && TienCK != undefined) {
            rowData[7] = TienCK.format(0, 3, ',');
        }
        else {
            rowData[7] = 0;
        }
        rowData[9] = Tien_Thue.format(0, 3, ',');
        rowData[10] = ThanhTien.format(0, 3, ',');
        calcTotal();
    }

    function calcTotal() {
        var data = table.data();
        var rowCount = data.length;
        var TongThanhTien = 0;
        var TongCK = 0;
        var TongVAT = 0;
        var Total = 0;
        for (var i = 0; i < rowCount; i++) {
            var row = data[i];
            TongThanhTien += ((isNaN(parseFloat(row[5])) || parseFloat(row[5]) == 0) ? 0 : parseFloat(row[5].replace(/,/gi, "")));
            TongCK += ((isNaN(parseFloat(row[7])) || parseFloat(row[7]) == 0) ? 0 : parseFloat(row[7].replace(/,/gi, "")));
            TongVAT += ((isNaN(parseFloat(row[9])) || parseFloat(row[9]) == 0) ? 0 : parseFloat(row[9].replace(/,/gi, "")));
            Total += ((isNaN(parseFloat(row[10])) || parseFloat(row[10]) == 0) ? 0 : parseFloat(row[10].replace(/,/gi, "")));
        };
        $('#TotalAmount').text(TongThanhTien.format(0, 3, ','));
        $('#DiscountAmount').text(TongCK.format(0, 3, ','));
        $('#VatAmount').text(TongVAT.format(0, 3, ','));
        $('#Total').text(Total.format(0, 3, ','));
    }

    function renderPaymentType() {
        const select = $('#PaymentId').empty();
        paymentType_data.forEach(function (item) {
            select.append($('<option></option>').val(item.Value).text(item.Text));
        })
    }

    function checkValidForm() {
        var form = $('#InvoiceContainer');
        form.find('.error').removeClass('error');
        var er = 0;
        var elements = form.find('[required]');
        for (var i = 0; i < elements.length; i++) {
            var el = $(elements[i]);
            if (el[0].tagName == 'INPUT' && el.val().trim() === '') {
                er++;
                el.addClass('error');
            } else if (el[0].tagName === "SELECT") {
                if (el[0].hasAttribute('required') && (el.val() === null || el.val().length == 0)) {
                    el.addClass('error');
                    er++;
                } else {
                    if (el.val() == '') {
                        er++;
                    }
                }
            }
        }
        return er === 0;
    }

    function getFormData() {
        var data = {
            'InvoiceId': $('#InvoiceId').val().trim(),
            'CreatedBy': '',
            'CreatedDate': '',
            'LastModifiedBy': '',
            'LastModifiedDate': '',
            'InvoiceType': $('#InvoiceType').val().trim(),
            'InvoiceCodeId': '',
            'InvoiceSeries': $('#InvoiceSeries').val().trim(), // Ký hiệu
            'InvoiceNumber': '',
            'InvoiceName': '',
            'InvoiceIssuedDate': '',
            'CurrencyCode': '',
            'ExchangeRate': '',
            'AdjustmentType': '',
            'CustomerId': '', // Mã khách hàng
            'PaymentId': $('#PaymentId').val().trim(), // Hình thức thanh toán
            'SellerBankAccount': '',
            'SellerBankName': '',
            'Signer': '',
            'SecretNumber': '',
            'InvoiceStatus': '',
            'PrintStatus': '',
            'SignedDate': '',
            'PrintBy': '',
            'Code_CT': '',
            'SubmittedDate': '',
            'ContractNumber': $('#ContractNumber').val().trim(), // Số hợp đồng
            'ContractDate': $('#ContractDate').data('date'), // Ngày hợp đồng
            'InvoiceNote': '',
            'AdditionalReferenceDes': '',
            'AdditionalReferenceDate': '',
            'DiscountAmount': '',
            'OrderNumber': $('#OrderNumber').val().trim(), // Số đơn hàng
            'LocalPDF': '',
            'CompanyId': companyIdWork,
            'Account': $('#Account').val().trim(), // Tài khoản
            'Bank': $('#Bank').val().trim(), // Ngân hàng
            'so_benh_an': '',
            'sovb': '',
            'ngayvb': '',
            'so_hd_dc': '',
            'inv_originalId': '',
            'dieu_tri': '',
            'ma_dvcs': '',
            'in_chuyen_doi': '',
            'ngay_in_cdoi': '',
            'CustomerCode': $('#CustomerCode').val().trim(),
            'CustomerFullName': $('#CustomerFullName').val().trim(),
            'CustomerTaxIdentification': $('#CustomerTaxIdentification').val().trim(),
            'CustomerAddress': $('#CustomerAddress').val().trim(),
            'CustomerAccountNumber': $('#CustomerAccountNumber').val().trim(),
            'CustomerEmail': '',
            'CustomerBank': $('#CustomerBank').val().trim(),
            'CustomerCompanyName': $('#CustomerCompanyName').val().trim(),
            'paymentMethodName': $('#PaymentId option:selected').text(),
            'Details': []
        }

        var tbData = table.data();
        var rowCount = tbData.length;

        for (var i = 0; i < rowCount; i++) {
            const rowData = tbData[i];
            if (((!validateInterger(rowData[columnDefined.Quantity]) && rowData[columnDefined.Quantity] != "&nbsp;")
                || (!validateInterger(rowData[columnDefined.UnitPrice]) && rowData[columnDefined.UnitPrice] != "&nbsp;"))) {
                $.notification.show('error', 'Số lượng hoặc đơn giá phải là dạng số.');
                return null;
            }
            if (parseFloatOrDefault(rowData[columnDefined.TotalAmount].replace(/,/gi, ""))) {
                data.Details.push({
                    'ItemCode': rowData[columnDefined.ItemCode].trim(),
                    'ItemName': rowData[columnDefined.ItemName].trim(),
                    'UnitName': rowData[columnDefined.UnitName].trim(),
                    'Quantity': parseFloatOrDefault(rowData[columnDefined.Quantity]),
                    'UnitPrice': parseFloatOrDefault(rowData[columnDefined.UnitPrice]),
                    'TotalAmountWithoutVat': parseFloatOrDefault(rowData[columnDefined.TotalAmountWithoutVat].replace(/,/gi, "")),
                    'DiscountPercent': parseFloatOrDefault(rowData[columnDefined.DiscountPercent]),
                    'DiscountAmount': parseFloatOrDefault(rowData[columnDefined.DiscountAmount].toString().replace(/,/gi, "")),
                    'VatPercentage': parseFloatOrDefault(rowData[columnDefined.VatPercentage]),
                    'VatAmount': parseFloatOrDefault(rowData[columnDefined.VatAmount].replace(/,/gi, "")),
                    'TotalAmount': parseFloatOrDefault(rowData[columnDefined.TotalAmount].replace(/,/gi, "")),
                    "InvoiceDetailId": rowData[columnDefined.InvoiceDetailId]
                });
            }
        }
        return data;
    }

    function parseFloatOrDefault(value) {
        return isNaN(parseFloat(value)) ? 0 : parseFloat(value);
    }

    function AddInvoice(data) {
        Utilities.Loader.show();
        $.ajax({
            type: 'POST',
            url: window.UrlConfig.AppUrl + 'api/Invoice/AddInvoice',
            data: JSON.stringify(data),
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                Utilities.Loader.hide();
                if (data.Code === 0) {
                    $.notification.show('success', 'Thêm mới thành công');
                    setTimeout(function () {
                        window.location = Config.AppUrl + "/Invoice/Index"
                    }, 1500);
                } else {
                    $.notification.show('error', data.Message);
                }
            },
            error: function (result) {
                Utilities.Loader.hide();
                $.notification.show('error', result.responseJSON.Message);

            }
        });
    }

    function UpdateInvoice(data) {
        Utilities.Loader.show();
        $.ajax({
            type: 'POST',
            url: window.UrlConfig.AppUrl + 'api/Invoice/UpdateIdentificationInvoice',
            data: JSON.stringify(data),
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                Utilities.Loader.hide();
                if (data.Code === 0) {
                    $.notification.show('success', 'Cập nhật thành công');
                    setTimeout(function () {
                        window.location = Config.AppUrl + "/Invoice/Index"
                    }, 1500);
                } else {
                    $.notification.show('error', data.Message);
                }
            },
            error: function (result) {
                Utilities.Loader.hide();
                $.notification.show('error', result.responseJSON.Message);

            }
        });
    }

    function UpdateLowInvoice(data) {
        Utilities.Loader.show();
        $.ajax({
            type: 'POST',
            url: window.UrlConfig.AppUrl + 'api/Invoice/UpdateLowInvoice',
            data: JSON.stringify(data),
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                Utilities.Loader.hide();
                if (data.Code === 0) {
                    $.notification.show('success', 'Cập nhật thành công');
                    setTimeout(function () {
                        window.location = Config.AppUrl + "/Invoice/Index"
                    }, 1500);
                } else {
                    $.notification.show('error', data.Message);
                }
            },
            error: function (result) {
                Utilities.Loader.hide();
                $.notification.show('error', result.responseJSON.Message);

            }
        });
    }

    function UpdateHighInvoice(data) {
        Utilities.Loader.show();
        $.ajax({
            type: 'POST',
            url: window.UrlConfig.AppUrl + 'api/Invoice/UpdateHighInvoice',
            data: JSON.stringify(data),
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                Utilities.Loader.hide();
                if (data.Code === 0) {
                    $.notification.show('success', 'Cập nhật thành công');
                    setTimeout(function () {
                        window.location = Config.AppUrl + "/Invoice/Index"
                    }, 1500);
                } else {
                    $.notification.show('error', data.Message);
                }
            },
            error: function (result) {
                Utilities.Loader.hide();
                $.notification.show('error', result.responseJSON.Message);

            }
        });
    }

    function ChangeInvoice(data) {
        Utilities.Loader.show();
        $.ajax({
            type: 'POST',
            url: window.UrlConfig.AppUrl + 'api/Invoice/ChangeInvoice',
            data: JSON.stringify(data),
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                Utilities.Loader.hide();
                if (data.Code === 0) {
                    $.notification.show('success', 'Cập nhật thành công');
                    setTimeout(function () {
                        window.location = Config.AppUrl + "/Invoice/Index"
                    }, 1500);
                } else {
                    $.notification.show('error', data.Message);
                }
            },
            error: function (result) {
                Utilities.Loader.hide();
                $.notification.show('error', result.responseJSON.Message);

            }
        });
    }

    function DeleteInvoice(data) {
        Utilities.Loader.show();
        $.ajax({
            type: 'POST',
            url: window.UrlConfig.AppUrl + 'api/Invoice/DeleteInvoice',
            data: JSON.stringify(data),
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                Utilities.Loader.hide();
                if (data.Code === 0) {
                    $.notification.show('success', 'Cập nhật thành công');
                    setTimeout(function () {
                        window.location = Config.AppUrl + "/Invoice/Index"
                    }, 1500);
                } else {
                    $.notification.show('error', data.Message);
                }
            },
            error: function (result) {
                Utilities.Loader.hide();
                $.notification.show('error', result.responseJSON.Message);

            }
        });
    }

    function getCustomers(searchText) {
        $.ajax({
            type: 'get',
            url: Config.AppUrl + '/Customer/getCustomer',
            data: { searchString: searchText.trim() },
            success: function (data) {
                if (data.Code === 0) {
                    Customer = data.Data;
                }
            },
            contentType: false,
            error: function () {
                $.notification.show('error', "Whoops something went wrong!");
            }
        });
    }

    function isEditMode() {
        return $('#EditMode').val() == "true";
    }

    function getDetail(invoiceId, callback) {
        $.ajax({
            type: 'get',
            url: Config.AppUrl + '/Invoice/GetDetail',
            data: { id: invoiceId },
            success: function (data) {
                if (callback) {
                    callback(data);
                }
            },
            contentType: false,
            error: function () {
                $.notification.show('error', "Whoops something went wrong!");
            }
        });
    }

    function fillData(data) {
        $('#InvoiceType').val(data.InvoiceType);

        appendToInvoiceSeries(data.InvoiceType);

        $('#InvoiceSeries').val(data.InvoiceCodeId);
        $('#InvoiceNumber').val(data.InvoiceNumber);
        $('#ContractDate').data("DateTimePicker").date(moment(data.ContractDate).format("DD-MM-YYYY"));
        $('#ContractNumber').val(data.ContractNumber);
        $('#OrderNumber').val(data.OrderNumber);
        $('#PaymentId').val(data.PaymentId);
        $('#Account').val(data.Account);
        $('#Bank').val(data.Bank);

        $('#CustomerCode').val(data.CustomerCode);
        $('#CustomerFullName').val(data.CustomerFullName);
        $('#CustomerTaxIdentification').val(data.CustomerTaxIdentification);
        $('#CustomerAddress').val(data.CustomerAddress);
        $('#CustomerAccountNumber').val(data.CustomerAccountNumber);
        $('#CustomerBank').val(data.CustomerBank);
        $('#CustomerCompanyName').val(data.CustomerCompanyName);

        if (data.Details.length > 0) {
            for (var i = 0; i < data.Details.length; i++) {
                const row = data.Details[i];

                var vatAmount = row['VatAmount'];
                var vatPercentage = row['VatPercentage'];
                if (vatAmount)
                    vatAmount = row['VatAmount'].format(0, 3, ',')
                else
                    vatAmount = 0

                if (vatPercentage)
                    vatPercentage = row['VatPercentage']
                else
                    vatPercentage = 0;

                table.row.add([row['ItemCode'], row['ItemName'], row['UnitName'], row['Quantity'],
                row['UnitPrice'], row['TotalAmountWithoutVat'].format(0, 3, ','), row['DiscountPercent'], row['DiscountAmount'],
                    row['VatPercentage'], vatAmount , calcItemAmount(row).format(0, 3, ','), row['InvoiceDetailId']]).draw();
            }
            calcTotalItem(data.Details);
        }


        $('#PaymentId').trigger("change")
    }

    function calcItemAmount(row) {
        return ((row['Quantity'] * row['UnitPrice']) - row['DiscountAmount']) + row['VatAmount'];
    }

    function calcTotalItem(data) {
        var TotalAmountWithoutVat = 0;
        var TotalDiscount = 0;
        var TotalVAT = 0;
        var Total = 0;
        for (var i = 0; i < data.length; i++) {
            const row = data[i];
            TotalAmountWithoutVat += row['TotalAmountWithoutVat'];
            TotalDiscount += row['DiscountAmount'];
            TotalVAT += row['VatAmount'];
            Total += calcItemAmount(row);
        }
        $('#TotalAmount').text(TotalAmountWithoutVat.format(0, 3, ','));
        $('#DiscountAmount').text(TotalDiscount.format(0, 3, ','));
        $('#VatAmount').text(TotalVAT.format(0, 3, ','));
        $('#Total').text(Total.format(0, 3, ','));
    }

    function initAutoComplete() {
        $("#CustomerCode").autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: Config.AppUrl + '/Customer/getSuggestion',
                    dataType: "json",
                    data: {
                        seachString: request.term,
                        searchByCode: true
                    },
                    success: function (data) {
                        if (data.Code === 0) {
                            Customer = data.Data;
                            var temp = data.Data.map(function (item) {
                                return { id: item.CustomerId, value: item.CustomerCode, label: item.CustomerCode }
                            })
                            response(temp);
                        }
                    }
                });
            },
            minLength: 2,
            select: function (event, ui) {
                let CustomerId = ui.item.id;
                let _customer = _.find(Customer, { CustomerId: CustomerId });
                if (_customer) {
                    fillCustomerInfor(_customer);
                }
            }
        });

        $("#CustomerFullName").autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: Config.AppUrl + '/Customer/getSuggestion',
                    dataType: "json",
                    data: {
                        seachString: request.term,
                        searchByCode: false
                    },
                    success: function (data) {
                        if (data.Code === 0) {
                            Customer = data.Data;
                            var temp = data.Data.map(function (item) {
                                return { id: item.CustomerId, value: item.FullName, label: item.FullName }
                            })
                            response(temp);
                        }
                    }
                });
            },
            minLength: 2,
            select: function (event, ui) {
                let CustomerId = ui.item.id;
                let _customer = _.find(Customer, { CustomerId: CustomerId });
                if (_customer) {
                    fillCustomerInfor(_customer);
                }
            }
        });
    }

    function fillCustomerInfor(data) {
        $('#CustomerCode').val(data.CustomerCode);
        $('#CustomerFullName').val(data.FullName);
        $('#CustomerTaxIdentification').val(data.TaxIdentification);
        $('#CustomerAddress').val(data.Address);
        $('#CustomerAccountNumber').val(data.AccountNumber);
        $('#CustomerBank').val(data.Bank);
        $('#CustomerCompanyName').val(data.CustomerCompanyName);
    }

    function getTemplateData() {
        return {
            "userId": $('#UserId').val(),
            "windowid": "WIN00187", // default
            "editmode": 1, // 1: Add new, 2: Update
            "data": [{
                "inv_InvoiceAuth_id": "5C17ABD7-E7E2-4B0F-8626-089361ED6B79",
                "inv_invoiceType": "01GTKT",
                "inv_InvoiceCode_id": "cfe59cab-5115-47d6-8977-4475fc06538e",
                "inv_invoiceSeries": "AE/17E",
                "inv_invoiceNumber": null,  // default if create new
                "inv_invoiceName": "Hóa đơn giá trị gia tăng",
                "inv_invoiceIssuedDate": "2018-01-01",
                "inv_currencyCode": "VND", // default
                "inv_exchangeRate": 1, // default
                "inv_adjustmentType": 1, // default
                "inv_buyerDisplayName": "NAM VACOM",
                "ma_dt": "NVC",
                "inv_buyerLegalName": "Công ty cổ phần VACOM",
                "inv_buyerTaxCode": "0102236276",
                "inv_buyerAddressLine": "81 Lê Văn Lương",
                "inv_buyerEmail": "nampv@vacom.com.vn",
                "inv_buyerBankAccount": "",
                "inv_buyerBankName": "",
                "inv_paymentMethodName": "Tiền mặt/Chuyển khoản",
                "inv_sellerBankAccount": "",
                "inv_sellerBankName": "",
                "trang_thai": "Chờ ký",
                "nguoi_ky": "",
                "sobaomat": "",
                "trang_thai_hd": 1,
                "in_chuyen_doi": false,
                "ngay_ky": null,
                "nguoi_in_cdoi": "",
                "ngay_in_cdoi": null,
                "mau_hd": "01GTKT0/001",
                "ma_ct": "HDDT",
                "details": [{
                    "tab_id": " TAB00188 ", // default
                    "tab_table": "inv_InvoiceAuthDetail", // default
                    "data": [{
                        "inv_InvoiceAuthDetail_id": "E784BAD5-2047-4601-BA62-6D5825112D89",
                        "inv_InvoiceAuth_id": "5C17ABD7-E7E2-4B0F8626-089361ED6B79",
                        "stt_rec0": "00000001",
                        "inv_itemCode": "",
                        "inv_itemName": "Máy tính 001",
                        "inv_unitCode": "",
                        "inv_unitName": "Cái",
                        "inv_unitPrice": 1000000,
                        "inv_quantity": 1,
                        "inv_TotalAmountWithoutVat": 1000000,
                        "inv_vatPercentage": 0,
                        "inv_vatAmount": 0,
                        "inv_TotalAmount": 1000000,
                        "inv_promotion": false,
                        "inv_discountPercentage": 0,
                        "inv_discountAmount": 0,
                        "ma_thue": "0"
                    }]
                }]
            }]
        }
    }

    function formatStt(num) {
        const len = 8;
        const myLen = num.toString().length;
        var str = '';
        for (var _i = 0; _i < len - myLen; _i++) {
            str += "0";
        }
        return str += num.toString();
    }

    function appendToInvoiceType(invoiceTypes) {
        var invoiceType = $("#InvoiceType");
        for (var type in window.invoiceType) {
            var optionInvoiceType = $("<option value='" + type + "'>" + type + "</option>");
            $(invoiceType).append(optionInvoiceType);
        }

        $(invoiceType).trigger("change");
    }

    function appendToInvoiceSeries(invoiceType) {
        var InvoiceSeries = $("#InvoiceSeries");
        $(InvoiceSeries).empty();
        _.forEach(window.invoiceType[invoiceType], function (item) {
            var optionInvoiceSeries = $("<option value='" + item.id + "'>" + item.ky_hieu + "</option>");
            $(InvoiceSeries).append(optionInvoiceSeries);
        });
    }

    Number.prototype.format = function (n, x, s, c) {
        var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')',
            num = this.toFixed(Math.max(0, ~~n));
        return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
    };

    function validateVietnameseName(value) {
        var otherLetters = "[àáãạảăắằẳẵặâấầẩẫậèéẹẻẽêềếểễệđìíĩỉịòóõọỏôốồổỗộơớờởỡợùúũụủưứừửữựỳỵỷỹýÀÁÃẠẢĂẮẰẲẴẶÂẤẦẨẪẬÈÉẸẺẼÊỀẾỂỄỆĐÌÍĨỈỊÒÓÕỌỎÔỐỒỔỖỘƠỚỜỞỠỢÙÚŨỤỦƯỨỪỬỮỰỲỴỶỸÝ]".normalize("NFC"),
            regexString = otherLetters
        regexPattern = RegExp(regexString);
        return regexPattern.test(value);
    }

    function validateInterger(value) {
        var regexPattern = RegExp("^[0-9]*$");
        return regexPattern.test(value);
    }
} 