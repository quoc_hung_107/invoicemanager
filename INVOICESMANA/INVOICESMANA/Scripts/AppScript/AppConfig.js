﻿const Config = {
    AppUrl: '',
    LanguageDataTable: {
        "sProcessing": "Đang xử lý...",
        "sLengthMenu": "Hiển thị _MENU_ bản ghi trong 1 trang",
        "sZeroRecords": "Chưa có dữ liệu",
        "sInfo": "Hiển thị _START_ - _END_ (trong _TOTAL_ bản ghi)",
        "sInfoEmpty": "Hiển thị 0 - 0 (trong 0 bản ghi)",
        "sInfoFiltered": "(lọc từ _MAX_ bản ghi)",
        "sInfoPostFix": "",
        "sSearch": "Tìm:",
        "sSearchPlaceholder": "Tên đơn vị",
        "sUrl": "",
        "oPaginate": {
            "sFirst": "Đầu",
            "sPrevious": "Trước",
            "sNext": "Tiếp",
            "sLast": "Cuối"
        }
    },
    LanguageDataInvoiceTable: {
        "sZeroRecords": "(Chưa có sản phẩm)",
    },
    TooltipDateTimePicker: {
        today: 'Ngày hiện tại',
        clear: 'Clear selection',
        close: 'Đóng',
        selectMonth: 'Chọn tháng',
        prevMonth: 'Tháng trước',
        nextMonth: 'Tháng tới',
        selectYear: 'Chọn năm',
        prevYear: 'Năm trước',
        nextYear: 'Năm tới',
        selectDecade: 'Chọn thập kỷ',
        prevDecade: 'Thập kỷ trước',
        nextDecade: 'Thập kỷ tới',
        prevCentury: 'Thế kỷ trước',
        nextCentury: 'Thế kỷ tới',
        incrementHour: 'Tăng giờ',
        pickHour: 'Chọn giờ',
        decrementHour: 'Giảm giờ',
        incrementMinute: 'Tăng phút',
        pickMinute: 'Chọn phút',
        decrementMinute: 'Giảm phút',
        incrementSecond: 'Tăng giây',
        pickSecond: 'Chọn giây',
        decrementSecond: 'Giảm giây'
    },
    DateTimePickerIcons: {
        time: 'glyphicon glyphicon-time',
        date: 'glyphicon glyphicon-calendar',
        up: 'glyphicon glyphicon-chevron-up',
        down: 'glyphicon glyphicon-chevron-down',
        previous: 'glyphicon glyphicon-chevron-left',
        next: 'glyphicon glyphicon-chevron-right',
        today: 'glyphicon glyphicon-screenshot',
        clear: 'glyphicon glyphicon-trash',
        close: 'glyphicon glyphicon-remove'
    },
    ApiURL: {
        SaveInvoice: 'http://test.minvoice.vn/api/System/Save'
    }
}