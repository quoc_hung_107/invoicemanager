﻿var RolesManagement = function () {
    var self = this;
    self.initRolesTable = function () {
        $('#RoleTable').DataTable({
            "serverSide": true,
            "processing": true,
            "paging": false,
            "searching": false,
            "lengthChange": false,
            "pageLength": 10,
            "sAjaxSource": Config.AppUrl + '/Role/GetRoles',
            "sAjaxDataProp": "data",
            "aoColumns": [
                {
                    "mData": "Name"
                },
                {
                    "mData": "UsersInRole",
                    "sClass": "align-center"
                },
                {
                    "mData": "RoleId",
                    "mRender": function (data, type, row) {
                        return '<a href="' + Config.AppUrl + '/Role/RoleRights?RoleId=' + data + '">Phân quyền</a>'
                    },
                    "orderable": false,
                    "sClass": "align-center"
                }],
            "oLanguage": Config.LanguageDataTable
        });
    }

    self.initRoleRightsTable = function () {
        var dataTable = $('#RoleRightsTable').DataTable({
            "serverSide": true,
            "processing": true,
            "paging": false,
            "searching": false,
            "lengthMenu": [[10, 25, 50, 100], [10, 25, 50, 100]],
            "pageLength": 10,
            "sAjaxSource": Config.AppUrl + '/Role/GetRoleRights',
            "fnServerParams": function (sSource) {
                sSource.push({ name: "customData", value: $("#RoleId").val() });
            },
            "sAjaxDataProp": "data",
            "aoColumns": [
                {
                    "mData": "ModuleName"
                },
                {
                    "mData": "IsViewAllowed", "orderable": false,
                    "mRender": function (data, type, row) {
                        return '<input type="checkbox" data-key="IsViewAllowed"' + (data ? 'checked' : '') + '/>'
                    },
                    "sClass": "align-center"
                },
                {
                    "mData": "IsAddAllowed", "orderable": false,
                    "mRender": function (data, type, row) {
                        return '<input type="checkbox" data-key="IsAddAllowed"' + (data ? 'checked' : '') + '/>'
                    },
                    "sClass": "align-center"
                },
                {
                    "mData": "IsUpdateAllowed", "orderable": false,
                    "mRender": function (data, type, row) {
                        return '<input type="checkbox" data-key="IsUpdateAllowed"' + (data ? 'checked' : '') + '/>'
                    },
                    "sClass": "align-center"
                },
                {
                    "mData": "IsDeleteAllowed", "orderable": false,
                    "mRender": function (data, type, row) {
                        return '<input type="checkbox" data-key="IsDeleteAllowed"' + (data ? 'checked' : '') + '/>'
                    },
                    "sClass": "align-center"
                },
                {
                    "mData": "RoleId",
                    "mRender": function (data, type, row) {
                        return '<input data-key="IsAllAllowed" type="checkbox"' + ((row.IsAddAllowed && row.IsDeleteAllowed && row.IsUpdateAllowed && row.IsViewAllowed) ? 'checked' : '') + '/>'
                    },
                    "sClass": "align-center",
                    "orderable": false
                }],
            "oLanguage": Config.LanguageDataTable
        }).on('click', 'input[type="checkbox"]', function (evt) {
            var table = dataTable;
            var data = table.row($(this).closest('tr')).data()
            data[$(evt.currentTarget).attr('data-key')] = evt.currentTarget.checked;
            if ($(evt.currentTarget).attr('data-key') === 'IsAllAllowed') {
                $(this).closest('tr').find('input').prop('checked', evt.currentTarget.checked);
                data['IsViewAllowed'] = evt.currentTarget.checked
                data['IsAddAllowed'] = evt.currentTarget.checked;
                data['IsUpdateAllowed'] = evt.currentTarget.checked;
                data['IsDeleteAllowed'] = evt.currentTarget.checked;
            } else {
                var temp = $(this).closest('tr').find('input:not([data-key="IsAllAllowed"])');
                var checkedCount = 0;
                for (var i = 0; i < temp.length; i++) {
                    if (temp[i].checked) {
                        checkedCount++;
                    }
                }
                $(this).closest('tr').find('input[data-key="IsAllAllowed"]').prop('checked', checkedCount === temp.length);
                data['IsAllAllowed'] = (checkedCount === temp.length)
            }
        });

        $('#btnUpdate').unbind().bind('click', function () {
            Utilities.Loader.show();
            var model = [];
            var data = dataTable.data()
            for (var i = 0; i < data.length; i++) {
                model.push(data[i]);
            }
            $.ajax({
                type: 'POST',
                url: Config.AppUrl + '/Role/UpdateRoleRights/',
                data: JSON.stringify({ model: model }),
                contentType: "application/json; charset=utf-8",
                dataType: 'json',
                success: function (data) {
                    Utilities.Loader.hide();
                    if (data.Code === 0) {
                        window.location= Config.AppUrl + '/Role'
                    }
                }
            })
        })
    }
}