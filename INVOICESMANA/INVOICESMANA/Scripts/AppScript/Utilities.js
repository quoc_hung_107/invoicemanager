﻿var Utilities = {
    getPermisionOnPage: function (_moduleId, callback) {
        $.ajax({
            url: Config.AppUrl + '/Home/GetPermissionOnPage',
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            data: { moduleId: _moduleId },
            cache: false,
            type: 'GET',
            success: function (data) {
                if (data.Code === 0 && callback) {
                    callback(data.Data)
                }
            }
        })
    },
    checkPermisionByClass: function (data) {
        if (data.IsAddAllowed == false) {
            $('.IsAddAllowed').remove();
        }

        if (data.IsDeleteAllowed == false) {
            $('.IsDeleteAllowed').remove();
        }

        if (data.IsViewAllowed == false) {
            $('.IsViewAllowed').remove();
        }

        if (data.IsUpdateAllowed == false) {
            $('.IsUpdateAllowed').remove();
        }
    },
    getListRole: function (callback) {
        $.ajax({
            url: Config.AppUrl + '/Role/GetListRole',
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            data: {},
            cache: false,
            type: 'GET',
            success: function (data) {
                if (callback) {
                    callback(data)
                }
            }
        })
    },
    getCompanies: function (callback) {
        $.ajax({
            type: "GET",
            url: Config.AppUrl + '/Account/getCompanies/',
            success: function (data) {
                if (callback) {
                    callback(data);
                }
            }
        })
    },
    IsValidEmail: function (_email) {
        return /^(?:[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+\.)*[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+@(?:(?:(?:[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!\.)){0,61}[a-zA-Z0-9]?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!$)){0,61}[a-zA-Z0-9]?)|(?:\[(?:(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\.){3}(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\]))$/.test(_email)
    },
    getTokenMInvoice: function (username, password, callback) {
        $.ajax({
            type: "POST",
            data: { UserName: username, Password: password },
            url: Config.AppUrl + '/Account/getTokenMinvoice/',
            success: function (data) {
                if (callback) {
                    callback(data);
                }
            }
        })
    },
    getInvoiceType: function (companyId) {
        return $.ajax({
            url: '/api/Invoice/GetInvoiceType',
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            data: { companyId: companyId },
            type: 'GET'
        })
    },
    getViettelInvoiceType: function (companyId) {
        return $.ajax({
            url: '/api/Viettel/Invoice/GetInvoiceType',
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            data: { companyId: companyId },
            type: 'GET'
        })
    },
    IsSuperAdmin: function () {
        return $('#IsSuperAdmin').val() == "true";
    },
    Loader: {
        show: function () {
            $('.loader').show();
        },
        hide: function () {
            $('.loader').fadeOut(1000);
        }
    },
    getTokenByCompanyId(companyId) {
        return $.ajax({
            type: "GET",
            data: { id: companyId },
            url: Config.AppUrl + '/Company/getToken/'
        })
    },
    refreshToken(companyId) {
        return $.ajax({
            type: "POST",
            data: { id: companyId },
            url: Config.AppUrl + '/Company/refreshToken/'
        })
    },
    createGuid() {
        return $.ajax({
            type: "GET",
            data: {},
            url: Config.AppUrl + '/Invoice/CreateID/'
        })
    }
}