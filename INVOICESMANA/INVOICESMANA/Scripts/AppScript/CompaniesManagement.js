﻿var CompaniesManagement = function () {
    var self = this;
    var fileLogo = null;
    self.initCompaniesTable = function () {
        var dataTable = $('#CompaniesTable').DataTable({
            "serverSide": true,
            "processing": true,
            "paging": true,
            "searching": true,
            "lengthChange": false,
            "pageLength": 10,
            "sAjaxSource": Config.AppUrl + '/Company/GetCompanies',
            "sAjaxDataProp": "data",
            "aoColumns": [
                {
                    "mData": "CompanyCode"
                },
                {
                    "mData": "Name"
                },
                {
                    "mData": "Address"
                },
                {
                    "mData": "CreatedDate",
                    "mRender": function (data, type, row) {
                        if (data) {
                            return new Date(parseInt(data.replace(/\D/g, ""))).toLocaleDateString()
                        }
                        return '';
                    },
                    "sClass": "align-right"
                },
                {
                    "mData": "PMS"
                },
                {
                    "mData": "Status",
                    "mRender": function (data, type, row) {
                        if (data === true) {
                            return '<i class="fa fa-check" aria-hidden="true"></i>'
                        }
                        return '<i class="fa fa-circle" aria-hidden="true"></i>'
                    },
                    "sClass": "align-center"
                },
                {
                    "mData": "CompanyId",
                    "mRender": function (data, type, row) {
                        if (row.IsUses) {
                            return '<a class="pull-left" href="' + Config.AppUrl + '/Company/Update?companyid=' + data + '">Chi tiết</a>' +
                                '<a class="pull-right operation" data-type="' + row.InvoiceType +'" data-company="' + data + '" href="#">Thao tác</a>'
                        }
                        if (data == $("#CompanyId").val()) {
                            return '<a class="pull-left" href="' + Config.AppUrl + '/Company/Update?companyid=' + data + '">Chi tiết</a>';
                        }

                        return "";
                    },
                    "sWidth": "110px",
                    "orderable": false
                }],
            "fnCreatedRow": function (dom, data, row) {
                if (data.Status == false) {
                    $(dom).addClass('inactive')
                }
            },
            "oLanguage": Config.LanguageDataTable
        }).on('click', function (evt) {
            if ($(evt.target).data("company")) {
                var data = $(evt.target).data("company");
                var type = $(evt.target).data("type");
                Cookies.set('company', data);
                Cookies.set('type', type);
                if (!type) {
                    window.location.href = Config.AppUrl + '/Invoice/Invoice?companyid=' + data;
                }
                else {
                    window.location.href = Config.AppUrl + '/ViettelInvoice/Invoice?companyid=' + data;
                }
            }
        });
    }

    self.initDetail = function (isAdd) {
        $('#CreatedDatePicker').datetimepicker({
            viewMode: 'days',
            format: "DD/MM/YYYY",
            showTodayButton: true,
            showClose: true,
            tooltips: Config.TooltipDateTimePicker,
            locale: 'vi',
            icons: Config.DateTimePickerIcons,
            date: moment($("#createdDate").val()),
            defaultDate: new Date()
        });
        $('#ExpiryDatePicker').datetimepicker({
            viewMode: 'days',
            format: "DD/MM/YYYY",
            showTodayButton: true,
            showClose: true,
            tooltips: Config.TooltipDateTimePicker,
            locale: 'vi',
            icons: Config.DateTimePickerIcons,
            date: moment($("#expiryDate").val())
        });

        $("#InvoiceType").on("click", function () {
            if ($(this).val() == "2") {
                $("#invoiceTypeDiv").show();
            }
            else {
                $("#invoiceTypeDiv").hide();
            }
        });

        if (isAdd != true) {
            isAdd = false;
        } else {
            isAdd = true;
        }
        var companyId = $('#CompanyId').val();
        var url = Config.AppUrl + '/Company/Update';
        if (isAdd) {
            url = Config.AppUrl + '/Company/AddNewCompany';
        }
        $('.com-logo').unbind().bind('click', function () {
            $('#FileInput').click();
        })

        $('#FileInput').on('change', function (evt) {
            var tgt = evt.target || window.event.srcElement, files = tgt.files;

            // FileReader support
            if (FileReader && files && files.length) {
                var fr = new FileReader();
                fr.onload = function () {
                    document.getElementById('Logo').src = fr.result;
                }
                fr.readAsDataURL(files[0]);
                fileLogo = files[0];
            } else {
                fileLogo = null;
            }
        });

        $("#addRow").unbind().click(function (e) {

            e.preventDefault();
            var tr = $("<tr></tr>");

            var buttonRemove = $('<button type="button" class="remoteRow"><i class="fa fa-remove"></i></button>');
            removeRow(buttonRemove);
            var seriesTd = $("<td></td>").append($('<input type="text" class="invoiceSeries form-control" value=""/>'));
            var typeTd = $("<td></td>").append($('<input type="text" class="invoiceTypes form-control" value=""/>'));
            var removeTd = $("<td ></td>").append(buttonRemove);
            $(tr).append(seriesTd);
            $(tr).append(typeTd);
            $(tr).append(removeTd);
            $("#invoiceTypeTable tbody").append(tr);
        });

        $(".remoteRow").on("click", function () {
            $(this).parent().parent().remove()
        });

        function removeRow(element) {
            $(element).unbind().click(function (e) {
                e.preventDefault();
                $(this).parent().parent().remove()
            });
        }

        $('#Submit').unbind().bind('click', function () {
            if (!self.checkValidForm()) {
                $.notification.show('error', 'Vui lòng nhập đầy đủ và chính xác các thông tin');
                return;
            }

            if (!Utilities.IsValidEmail($('#Email').val().trim())) {
                $.notification.show('error', 'Email bạn nhập không hợp lệ, vui lòng nhập lại');
                $('#Email').addClass('error');
                return;
            }

            if ($('#Account').val().trim() === '' || $('#Password').val() == '') {
                $.notification.show('error', 'Bạn cần nhập đủ thông tin API (Account và Password)');
                return;
            }

            Utilities.Loader.show();
            if ($("#InvoiceType").val() == "2") {
                UpdateCompany(url);
            }
            else {
                Utilities.getTokenMInvoice($('#Account').val().trim(), $('#Password').val(), function (data) {
                    UpdateCompany(url, data)
                });
            }

        });

        function UpdateCompany(url, data) {
            Utilities.Loader.hide();
            if (!data || data.Code === 0) {
                Utilities.Loader.show();
                var formData = new FormData();
                var company = {};
                if (fileLogo) {
                    formData.append('file', fileLogo);
                }
                if (isAdd !== true) {
                    company.CompanyId = companyId;
                }
                company.Name = $('#Name').val();
                company.Address = $('#Address').val();
                company.Email = $('#Email').val();
                company.Phone = $('#Phone').val();
                company.TaxIdentification = $('#TaxIdentification').val();
                company.InvoiceType = $('#InvoiceType').val();
                company.Account = $('#Account').val();
                company.Password = $('#Password').val();
                company.Status = $('#Status').val();
                company.PMS = $('#PMS').val();
                company.CompanyCode = $('#CompanyCode').val();
                if (data && data.Data) {
                    company.Token = data.Data;
                }

                company.CreatedDate = moment($('#CreatedDatePicker').data('date'), "DD/MM/YYYY").format("YYYY-MM-DD");
                company.ExpiryDate = moment($('#ExpiryDatePicker').data('date'), "DD/MM/YYYY").format("YYYY-MM-DD");
                company.invoiceTypes = [];

                $("#invoiceTypeTable tbody tr").each(function () {
                    var invoicetype = {};
                    invoicetype.InvoiceSeries = $(this).find(".invoiceSeries").val();
                    invoicetype.InvoiceTypes = $(this).find(".invoiceTypes").val();

                    if (invoicetype.InvoiceSeries != null && invoicetype.InvoiceSeries != "" && invoicetype.InvoiceSeries != undefined
                        && invoicetype.InvoiceTypes != null && invoicetype.InvoiceTypes != "" && invoicetype.InvoiceTypes != undefined) {
                        company.invoiceTypes.push(invoicetype);
                    }
                });


                formData.append('comReq', JSON.stringify(company));

                $.ajax({
                    type: 'POST',
                    url: url,
                    data: formData,
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        Utilities.Loader.hide();
                        if (data.Code === 0) {
                            if (isAdd) {
                                $.notification.show('success', 'Thêm mới thành công');
                            } else {
                                $.notification.show('success', 'Cập nhật thành công');
                            }
                            setTimeout(function () {
                                window.location = Config.AppUrl + '/Company/Index'
                            }, 1500);
                        }
                    },
                    processData: false,
                    contentType: false,
                    error: function () {
                        $.notification.show('error', "Whoops something went wrong!");
                    }
                });
            }
            else {
                Utilities.Loader.hide();
                $.notification.show('error', "Thông tin API không đúng.");
            }
        }

        $('#Status').on('change', function () {
            if ($(this).val() !== "True") {
                $('#ExpiryDatePicker').data("DateTimePicker").date(moment(new Date()).format($('#sysFormat').val().toUpperCase()));
            } else {
                $('#ExpiryDatePicker').data("DateTimePicker").date(null);
            }
        })
    }

    self.checkValidForm = function () {
        var form = $('#CompanyDetailForm');
        form.find('.error').removeClass('error');
        var er = 0;
        var elements = form.find('[required]');
        for (var i = 0; i < elements.length; i++) {
            var el = $(elements[i]);
            if (el[0].tagName == 'INPUT' && el.val().trim() === '') {
                er++;
                el.addClass('error');
            } else if (el[0].tagName === "SELECT") {
                if (el[0].hasAttribute('required') && (el.val() === null || el.val().length == 0)) {
                    el.addClass('error');
                    er++;
                } else {
                    if (el.val() == '') {
                        er++;
                    }
                }
            }
        }
        return er === 0;
    }
}