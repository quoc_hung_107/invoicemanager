﻿var UserManagement = function () {
    var self = this;
    var Companies = [];
    var selectCompany = $('#CompanyId');
    var inputUrserId = $('#UserId');
    var selectRole = $('#Role');
    var editMyAcc = $('#EditMyAcc').val() || false;
    var _USER = {};

    self.init = function () {
        $('#UserTable').DataTable({
            "serverSide": true,
            "processing": true,
            "paging": true,
            "searching": true,
            "lengthChange": false,
            "pageLength": 10,
            "sAjaxSource": Config.AppUrl + '/Account/GetUsers',
            "sAjaxDataProp": "data",
            "fnServerParams": function (sSource) {
                sSource.push({ name: "customData", value: $("#CompanyId").val() });
            },

            "aoColumns": [
                { "mData": "UserName" },
                { "mData": "FullName" },
                { "mData": "CompanyName" },
                {
                    "mData": "RolesName",
                    "mRender": function (data, type, row) {
                        return data.join(', ')
                    }
                },
                {
                    "mData": "CreatedDate",
                    "mRender": function (data, type, row) {
                        return new Date(parseInt(data.replace(/\D/g, ""))).toLocaleDateString()
                    },
                    "sClass": "align-right"
                },
                {
                    "mData": "Status",
                    "mRender": function (data, type, row) {
                        if (data === true) {
                            return '<i class="fa fa-check" aria-hidden="true"></i>'
                        }
                        return '<i class="fa fa-circle" aria-hidden="true"></i>'
                    },
                    "sClass": "align-center"
                },
                {
                    "mData": "UserId",
                    "mRender": function (data, type, row) {
                        return '<a href="' + Config.AppUrl + '/Account/Update?userId=' + data + '">Chi tiết</a>'
                    },
                    "orderable": false,
                    "sClass": "align-center text-decoration-line-none"
                }],
            "fnCreatedRow": function (dom, data, row) {
                if (data.Status == false) {
                    $(dom).addClass('inactive')
                }
            },
            "oLanguage": Config.LanguageDataTable
        });
    }

    self.InitDetail = function () {
        if (inputUrserId.val() != '') {
            $('#Password').removeAttr('required');
            $('#ConfirmPassword').removeAttr('required');
            self.getUserDetail(inputUrserId.val(), function (data) {
                _USER = data.Data;
                selectRole.empty();
                self.fillData(data);
                self.createSelectRole(data.Data.Roles, data.Data.Role)
                self.createSelectCompany(data.Data.Companies, data.Data.CompanyId)
            })
        }
        else {
            Utilities.getCompanies(function (data) {
                selectCompany.empty();
                if (data.length > 0) {
                    self.createSelectCompany(data)
                }
            })

            Utilities.getListRole(function (data) {
                selectRole.empty();
                if (data.Code == 0) {
                    if (data.Data.length > 0) {
                        data.Data.forEach(function (item, index) {
                            selectRole.append($('<option></option>').val(item.Value).text(item.Text));
                        })
                    }
                }
            })
        }


        if (inputUrserId.val() == '') {
            $('#Submit').unbind().bind('click', function () {
                self.AddUser();
            })
        } else {
            $('#Submit').unbind().bind('click', function () {
                self.UpdateUser();
            })
        }
    }

    self.createSelectRole = function (data, selectItem) {
        if (editMyAcc.toString() == true.toString()) {
            if (selectItem.length > 0) {
                var role = _.filter(data, function (obj) {
                    return selectItem.indexOf(obj.Value);
                })
                role = role.map(function (r) { return r.Text });
                selectRole.val(role.join(', '));
            }
        } else {
            data.forEach(function (item, index) {
                selectRole.append($('<option></option>').val(item.Value).text(item.Text));
            });
            selectRole.val(selectItem).change();
        }
    }

    self.createSelectCompany = function (data, selectItem) {
        selectCompany.unbind().bind('change', function () {
            var select = _.filter(data, { Value: selectCompany.find('option:selected').val() })
            if (select.length > 0) {
                $('#CompanyTaxIdentification').val(select[0].Attributes.TaxIdentification);
                $('#CompanyAddress').val(select[0].Attributes.Address);
            } else {
                $('#CompanyTaxIdentification').val('');
                $('#CompanyAddress').val('');
            }
        })
        data.forEach(function (item, index) {
            selectCompany.append($('<option></option>').val(item.Value).text(item.Text));
        });
        selectCompany.val(selectItem).change();
    }

    self.getUserDetail = function (UserId, callback) {
        $.ajax({
            url: Config.AppUrl + '/Account/getUserDetail',
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            data: { userId: UserId },
            cache: false,
            type: 'GET',
            success: function (data) {
                if (callback) {
                    callback(data)
                }
            }
        })
    }

    self.RoleToName = function () {

    }

    self.AddUser = function () {
        if (self.checkValidForm() == true) {
            var data = {
                UserId: $('#UserId').val(),
                UserName: $('#UserName').val(),
                Password: $('#Password').val(),
                ConfirmPassword: $('#ConfirmPassword').val(),
                FullName: $('#FullName').val(),
                CreatedBy: $('#UserId').val(),
                CreatedDate: $('#UserId').val(),
                LastModifiedBy: $('#UserId').val(),
                LastModifiedDate: $('#UserId').val(),
                Email: $('#Email').val(),
                CompanyId: $('#CompanyId').val(),
                Status: $('#Status').val(),
                Role: $('#Role').val()
            }
            if (data.ConfirmPassword != data.Password) {
                $.notification.show('error', "Nhập lại mật khẩu không khớp");
                return;
            }
            if (!Utilities.IsValidEmail(data.Email)) {
                $.notification.show('error', 'Vui lòng nhập đúng định dạng email');
                return;
            }
            $.ajax({
                url: Config.AppUrl + '/Account/AddUser',
                contentType: "application/json; charset=utf-8",
                dataType: 'json',
                data: JSON.stringify(data),
                cache: false,
                type: 'POST',
                success: function (data) {
                    if (data.Code != 0) {
                        $.notification.show('error', data.Message);
                        return;
                    }
                    $.notification.show('success', 'Thêm mới thành công');
                    setTimeout(function () {
                        window.location = Config.AppUrl + "/Account/Users"
                    }, 1500)
                }
            })
        } else {
            $.notification.show('error', 'Vui lòng nhập đầy đủ thông tin')
            return;
        }
    }

    self.fillData = function (data) {
        if (data.Code === 0) {
            user = data.Data;
            $('#UserId').val(user.UserId);
            $('#UserName').val(user.UserName);
            $('#Password').val(user.Password),
            $('#ConfirmPassword').val(user.Password),
            $('#FullName').val(user.FullName),
            $('#Email').val(user.Email),
            $('#CompanyId').val(user.CompanyId),
            $('#Status').val(user.Status.toString()).change(),
            $('#Role').val(user.Role);
        } else {
            self.clearForm();
        }
    }

    self.clearForm = function () {
        $('#UserId').val('');
        $('#UserName').val('');
        $('#Password').val(''),
        $('#ConfirmPassword').val(''),
        $('#FullName').val(''),
        $('#Email').val(''),
        $('#CompanyId').val(''),
        $('#Status').val(''),
        $('#Role').val('');
    }

    self.UpdateUser = function () {
        if (self.checkValidForm() == true) {
            var data = {
                UserId: $('#UserId').val(),
                UserName: $('#UserName').val(),
                Password: $('#Password').val(),
                ConfirmPassword: $('#ConfirmPassword').val(),
                FullName: $('#FullName').val(),
                CreatedBy: $('#UserId').val(),
                CreatedDate: $('#UserId').val(),
                LastModifiedBy: $('#UserId').val(),
                LastModifiedDate: $('#UserId').val(),
                Email: $('#Email').val(),
                CompanyId: $('#CompanyId').val(),
                Status: $('#Status').val(),
                Role: (editMyAcc.toString() == true.toString() ? _USER.Role : $('#Role').val())
            }
            if (data.ConfirmPassword !== '' && data.Password !== '') {
                if (data.ConfirmPassword != data.Password) {
                    $.notification.show('error', "Nhập lại mật khẩu không khớp");
                    return;
                }
            }
            if (!Utilities.IsValidEmail(data.Email)) {
                $.notification.show('error', 'Vui lòng nhập đúng định dạng email');
                return;
            }
            $.ajax({
                url: Config.AppUrl + '/Account/Update',
                contentType: "application/json; charset=utf-8",
                dataType: 'json',
                data: JSON.stringify(data),
                cache: false,
                type: 'POST',
                success: function (data) {
                    if (data.Code != 0) {
                        $.notification.show('error', data.Message);
                        return;
                    }
                    if (editMyAcc.toString() != "true") {
                        $.notification.show('success', 'Cập nhật thành công');
                        setTimeout(function () {
                            window.location = Config.AppUrl + "/Account/Users"
                        }, 1500)
                    } else {
                        $.notification.show('success', 'Cập nhật thành công')
                    }
                }
            })
        } else {
            $.notification.show('error', "Vui lòng nhập đầy đủ thông tin")
            return;
        }
    }

    self.checkValidForm = function () {
        var form = $('#FormDetail');
        form.find('.error').removeClass('error');
        var er = 0;
        var elements = form.find('[required]');
        for (var i = 0; i < elements.length; i++) {
            var el = $(elements[i]);
            if (el[0].tagName == 'INPUT' && el.val().trim() === '') {
                er++;
                el.addClass('error');
            } else if (el[0].tagName === "SELECT") {
                if (el[0].hasAttribute('required') && (el.val() === null || el.val().length == 0)) {
                    el.addClass('error');
                    er++;
                } else {
                    if (el.val() == '') {
                        er++;
                    }
                }
            }
        }
        return er === 0;
    }
}