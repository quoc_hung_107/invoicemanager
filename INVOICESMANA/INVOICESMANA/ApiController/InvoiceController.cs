﻿using INVOICESMANA.DATA;
using INVOICESMANA.DATA.Common;
using INVOICESMANA.Helper;
using INVOICESMANA.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json.Linq;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.IO;
using System.Web.Hosting;
using System.Net.Http.Headers;
using System.Text;
using System.Web.Script.Serialization;

namespace INVOICESMANA
{
    public class InvoiceController : ApiController
    {
        #region mInvoices

        [HttpPost]
        [Route("api/Invoice/AddInvoice")]
        public IHttpActionResult AddInvoice(InvoiceData model)
        {
            Result result = Add(model, 1);
            return Ok(result);
        }

        [HttpPost]
        [Route("api/Invoice/UpdateIdentificationInvoice")]
        public IHttpActionResult UpdateIdentificationInvoice(InvoiceData model)
        {
            Result result = new Result();
            var url = ConfigurationManager.AppSettings["URL_SaveInvoice"];
            var tokens = "";
            try
            {

                var session = System.Web.HttpContext.Current.Session;
                using (var unitWork = new UnitOfWork())
                {
                    var resultCheck = CheckCompany(model.companyId, ref tokens);

                    if (resultCheck.Code != 0)
                    {
                        return Ok(resultCheck);
                    }

                    if (string.IsNullOrEmpty(tokens))
                    {
                        result.Code = -1;
                        result.Message = "Hết phiên làm việc rồi.";
                        return Ok(result);
                    }

                    List<DetailModel> dataDetail = new List<DetailModel>();
                    var invoice = new Invoice();
                    var oldInvoice = new Invoice();

                    foreach (var item in model.data)
                    {
                        if (!item.paymentId.HasValue)
                        {
                            result.Code = -1;
                            result.Message = "Thông tin thanh toán chưa được nhập.";
                            return Ok(result);
                        }


                        var id = new Guid(item.inv_InvoiceAuth_id);
                        oldInvoice = unitWork.InvoicesRepository.GetById(id);
                    }

                    var data = ApiHelper.ConvertToJson(model, 5, oldInvoice);

                    var rs = ApiHelper.PostAPI(data, url, tokens);

                    var resultObject = JsonConvert.DeserializeObject<dynamic>(rs);

                    if (resultObject.ok == "true")
                    {
                        var dataResult = resultObject.data;

                        InvoicedModel invoiceData = model.data.FirstOrDefault();
                        if (invoiceData != null)
                        {
                            var id = new Guid(invoiceData.inv_InvoiceAuth_id);
                            var invoiceOld = unitWork.InvoicesRepository.GetById(id);
                            invoiceOld.InvoiceStatus = 5;
                            unitWork.InvoicesRepository.Save();

                            List<InvoiceDetail> details = new List<InvoiceDetail>();

                            var customer = new Customer()
                            {
                                CustomerId = Guid.NewGuid(),
                                CustomerCode = invoiceData.ma_dt,
                                CompanyId = new Guid(model.companyId),
                                FullName = invoiceData.inv_buyerDisplayName,
                                Email = invoiceData.inv_buyerEmail,
                                TaxIdentification = invoiceData.inv_buyerTaxCode,
                                Address = invoiceData.inv_buyerAddressLine,
                                AccountNumber = invoiceData.inv_buyerBankAccount,
                                Bank = invoiceData.inv_buyerBankName,
                                Status = true,
                                CustomerCompanyName = invoiceData.inv_buyerLegalName
                            };

                            unitWork.CustomersRepository.Add(customer);

                            //thêm hóa đơn
                            invoice = new Invoice();
                            invoice.InvoiceId = dataResult.inv_InvoiceAuth_id;
                            invoice.InvoiceCodeId = dataResult.inv_InvoiceCode_id; // required
                            invoice.InvoiceType = invoiceData.inv_invoiceType; // required
                            invoice.InvoiceSeries = invoiceData.inv_invoiceSeries; // required
                            invoice.InvoiceNumber = dataResult.inv_invoiceNumber; // required
                            invoice.ContractDate = dataResult.inv_invoiceIssuedDate;
                            invoice.ContractNumber = dataResult.inv_invoiceNumber;
                            invoice.OrderNumber = invoiceData.orderNumber;
                            invoice.PaymentId = invoiceData.paymentId;
                            invoice.Account = invoiceData.inv_sellerBankAccount;
                            invoice.Bank = invoiceData.inv_sellerBankName;
                            invoice.CustomerId = customer.CustomerId;
                            invoice.inv_originalId = oldInvoice.InvoiceId;

                            //invoice.CreatedBy = Identity.UserId;
                            invoice.CreatedDate = DateTime.Now;
                            invoice.InvoiceName = dataResult.inv_invoiceName == null ? string.Empty : dataResult.inv_invoiceName; // required
                            invoice.InvoiceIssuedDate = dataResult.inv_invoiceIssuedDate; // required
                            invoice.CurrencyCode = dataResult.inv_currencyCode == null ? "VND" : dataResult.inv_currencyCode; // required
                            invoice.ExchangeRate = dataResult.inv_exchangeRate; // required
                            invoice.AdjustmentType = dataResult.inv_adjustmentType; // required
                            invoice.CompanyId = new Guid(model.companyId);
                            invoice.so_hd_dc = invoiceData.so_hd_dc;
                            invoice.dieu_tri = invoiceData.dieu_tri;
                            invoice.in_chuyen_doi = invoiceData.in_chuyen_doi.ToString();
                            invoice.ngay_in_cdoi = invoiceData.ngay_in_cdoi;

                            //chi tiết hóa đơn
                            if (oldInvoice.InvoiceDetails.Any())
                            {
                                foreach (var item in oldInvoice.InvoiceDetails)
                                {
                                    InvoiceDetail detail = new InvoiceDetail();
                                    detail.InvoiceDetailId = Guid.NewGuid();
                                    detail.InvoiceId = invoice.InvoiceId;
                                    detail.ItemCode = item.ItemCode;
                                    detail.ItemName = item.ItemName;
                                    detail.Promotion = item.Promotion;
                                    detail.CompanyId = new Guid(model.companyId);
                                    detail.DiscountPercent = item.DiscountPercent;
                                    detail.DiscountAmount = item.DiscountAmount;
                                    detail.Quantity = item.Quantity;
                                    detail.TotalAmount = item.TotalAmount;
                                    detail.VatAmount = item.VatAmount;
                                    detail.VatPercentage = item.VatPercentage;
                                    detail.TotalAmountWithoutVat = item.TotalAmountWithoutVat;
                                    detail.UnitCode = item.UnitCode;
                                    detail.UnitName = item.UnitName;
                                    detail.UnitPrice = item.UnitPrice;

                                    unitWork.InvoiceDetailsRepository.Add(detail);
                                }
                            }

                            unitWork.InvoicesRepository.Add(invoice);

                            unitWork.SaveChanges();

                            result.InvoiceId = invoice.InvoiceId.ToString();
                            result.InvoiceNumber = invoice.InvoiceNumber.ToString();

                            string inv_InvoiceAuth_id = dataResult.inv_InvoiceAuth_id;
                            var a = CopyPDFToLocal(inv_InvoiceAuth_id, tokens);
                        }
                        else
                        {
                            result.Code = -1;
                            result.Message = "Không có thông tin hóa đơn.";
                            return Ok(result);
                        }
                    }
                    else
                    {
                        result.Code = -1;
                        result.Message = resultObject.error;
                        return Ok(result);
                    }

                    result.Data = invoice.InvoiceNumber;
                    result.Code = 0;
                    result.Message = "Điều chỉnh thành công hóa đơn.";

                    return Ok(result);
                }
            }
            catch (Exception ex)
            {
                result.Code = -1;
                result.Message = ex.Message;
                return Ok(result);
            }
        }

        [HttpPost]
        [Route("api/Invoice/UpdateLowInvoice")]
        public IHttpActionResult UpdateLowInvoice(InvoiceData model)
        {
            Result result = new Result();
            var url = ConfigurationManager.AppSettings["URL_SaveInvoice"];
            var tokens = "";
            try
            {
                var session = System.Web.HttpContext.Current.Session;
                using (var unitWork = new UnitOfWork())
                {
                    var resultCheck = CheckCompany(model.companyId, ref tokens);

                    if (resultCheck.Code != 0)
                    {
                        return Ok(resultCheck);
                    }

                    if (string.IsNullOrEmpty(tokens))
                    {
                        result.Code = -1;
                        result.Message = "Hết phiên làm việc rồi.";
                        return Ok(result);
                    }

                    foreach (var item in model.data)
                    {
                        if (!item.paymentId.HasValue)
                        {
                            result.Code = -1;
                            result.Message = "Thông tin thanh toán chưa được nhập.";
                            return Ok(result);
                        }

                        foreach (var detail in item.details)
                        {
                            foreach (var d in detail.data)
                            {
                                if (d.inv_TotalAmount <= 0)
                                {
                                    result.Code = -1;
                                    result.Message = "Số tiền của bạn phải lớn hơn 0.";
                                    return Ok(result);
                                }

                                if (string.IsNullOrEmpty(d.inv_itemName))
                                {
                                    result.Code = -1;
                                    result.Message = "Tên sản phẩm không được để trống";
                                    return Ok(result);
                                }
                            }
                        }
                    }

                    var invoice = new Invoice();
                    var oldInvoice = new Invoice();

                    foreach (var item in model.data)
                    {
                        var id = new Guid(item.inv_InvoiceAuth_id);
                        oldInvoice = unitWork.InvoicesRepository.GetById(id);
                    }

                    var data = ApiHelper.ConvertToJson(model, 21, oldInvoice);

                    var rs = ApiHelper.PostAPI(data, url, tokens);

                    var resultObject = JsonConvert.DeserializeObject<dynamic>(rs);
                    if (resultObject.ok == "true")
                    {
                        var invoiceResult = resultObject.data;
                        InvoicedModel invoiceData = model.data.FirstOrDefault();
                        if (invoiceData != null)
                        {
                            //hủy hóa đơn cũ
                            var id = new Guid(invoiceData.inv_InvoiceAuth_id);
                            var invoiceOld = unitWork.InvoicesRepository.GetById(id);
                            invoiceOld.InvoiceStatus = 21;
                            unitWork.InvoicesRepository.Save();

                            //tạo hóa đơn mới
                            List<InvoiceDetail> details = new List<InvoiceDetail>();
                            var customer = new Customer()
                            {
                                CustomerId = Guid.NewGuid(),
                                CustomerCode = invoiceData.ma_dt,
                                CompanyId = new Guid(model.companyId),
                                FullName = invoiceData.inv_buyerDisplayName,
                                Email = invoiceData.inv_buyerEmail,
                                TaxIdentification = invoiceData.inv_buyerTaxCode,
                                Address = invoiceData.inv_buyerAddressLine,
                                AccountNumber = invoiceData.inv_buyerBankAccount,
                                Bank = invoiceData.inv_buyerBankName,
                                Status = true,
                                CustomerCompanyName = invoiceData.inv_buyerLegalName
                            };

                            unitWork.CustomersRepository.Add(customer);


                            invoice = new Invoice();
                            invoice.InvoiceId = invoiceResult.inv_InvoiceAuth_id;
                            invoice.InvoiceCodeId = oldInvoice.InvoiceCodeId; // required
                            invoice.InvoiceType = oldInvoice.InvoiceType;
                            invoice.InvoiceSeries = oldInvoice.InvoiceSeries;
                            invoice.ContractDate = oldInvoice.InvoiceIssuedDate;
                            invoice.ContractNumber = invoiceResult.inv_invoiceNumber;
                            invoice.OrderNumber = oldInvoice.OrderNumber;
                            invoice.PaymentId = oldInvoice.PaymentId;
                            invoice.Account = oldInvoice.Account;
                            invoice.Bank = oldInvoice.Bank;
                            invoice.CustomerId = oldInvoice.CustomerId;
                            //invoice.CreatedBy = Identity.UserId;
                            invoice.CreatedDate = DateTime.Now;
                            invoice.inv_originalId = oldInvoice.InvoiceId;
                            invoice.InvoiceNumber = invoiceResult.inv_invoiceNumber;
                            invoice.InvoiceName = invoiceData.inv_invoiceName == null ? string.Empty : invoiceData.inv_invoiceName; // required
                            invoice.InvoiceIssuedDate = invoiceData.inv_invoiceIssuedDate; // required
                            invoice.CurrencyCode = invoiceData.inv_currencyCode == null ? "VND" : invoiceData.inv_currencyCode; // required
                            invoice.ExchangeRate = invoiceData.inv_exchangeRate; // required
                            invoice.AdjustmentType = invoiceResult.inv_adjustmentType; // required
                            invoice.CompanyId = new Guid(model.companyId);
                            invoice.so_hd_dc = invoiceData.so_hd_dc;
                            invoice.inv_originalId = invoiceData.inv_originalId;
                            invoice.dieu_tri = invoiceData.dieu_tri;
                            invoice.in_chuyen_doi = invoiceData.in_chuyen_doi.ToString();
                            invoice.ngay_in_cdoi = invoiceData.ngay_in_cdoi;
                            invoice.InvoiceStatus = invoiceResult.trang_thai_hd;

                            if (invoiceData.details != null && invoiceData.details.Any())
                            {
                                foreach (var d in invoiceData.details)
                                {
                                    foreach (var item in d.data)
                                    {
                                        InvoiceDetail detail = new InvoiceDetail();
                                        detail.InvoiceDetailId = Guid.NewGuid();
                                        detail.InvoiceId = invoice.InvoiceId;
                                        detail.ItemCode = item.inv_itemCode;
                                        detail.ItemName = item.inv_itemName;
                                        detail.Promotion = item.inv_promotion;
                                        detail.CompanyId = new Guid(model.companyId);
                                        detail.DiscountPercent = item.inv_discountPercentage;
                                        detail.DiscountAmount = item.inv_discountAmount;
                                        detail.Quantity = item.inv_quantity;
                                        detail.TotalAmount = item.inv_TotalAmount;
                                        detail.VatAmount = item.inv_vatAmount;
                                        detail.VatPercentage = item.inv_vatPercentage;
                                        detail.TotalAmountWithoutVat = item.inv_TotalAmountWithoutVat;
                                        detail.UnitCode = item.inv_unitCode;
                                        detail.UnitName = item.inv_unitName;
                                        detail.UnitPrice = item.inv_unitPrice;

                                        unitWork.InvoiceDetailsRepository.Add(detail);
                                    }
                                }
                            }

                            unitWork.InvoicesRepository.Add(invoice);

                            unitWork.SaveChanges();

                            result.InvoiceId = invoice.InvoiceId.ToString();
                            result.InvoiceNumber = invoice.InvoiceNumber.ToString();

                            string inv_InvoiceAuth_id = invoiceResult.inv_InvoiceAuth_id;
                            var a = CopyPDFToLocal(inv_InvoiceAuth_id, tokens);
                        }
                        else
                        {
                            result.Code = -1;
                            result.Message = "Không tìm thấy hóa đơn.";
                            return Ok(result);
                        }
                    }
                    else
                    {
                        result.Code = -1;
                        result.Message = resultObject.error;
                        return Ok(result);
                    }

                    result.Data = invoice.InvoiceNumber;
                    result.Code = 0;
                    result.Message = "Điều chỉnh thành công hóa đơn.";

                    return Ok(result);
                }
            }
            catch (Exception ex)
            {
                result.Code = -1;
                result.Message = ex.Message;
                return Ok(result);
            }
        }

        [HttpPost]
        [Route("api/Invoice/UpdateHighInvoice")]
        public IHttpActionResult UpdateHighInvoice(InvoiceData model)
        {
            Result result = new Result();
            var url = ConfigurationManager.AppSettings["URL_SaveInvoice"];
            var tokens = "";
            try
            {
                var session = System.Web.HttpContext.Current.Session;
                using (var unitWork = new UnitOfWork())
                {
                    var resultCheck = CheckCompany(model.companyId, ref tokens);

                    if (resultCheck.Code != 0)
                    {
                        return Ok(resultCheck);
                    }

                    if (string.IsNullOrEmpty(tokens))
                    {
                        result.Code = -1;
                        result.Message = "Hết phiên làm việc rồi.";
                        return Ok(result);
                    }

                    foreach (var item in model.data)
                    {
                        if (!item.paymentId.HasValue)
                        {
                            result.Code = -1;
                            result.Message = "Thông tin thanh toán chưa được nhập.";
                            return Ok(result);
                        }

                        foreach (var detail in item.details)
                        {
                            foreach (var d in detail.data)
                            {
                                if (d.inv_TotalAmount <= 0)
                                {
                                    result.Code = -1;
                                    result.Message = "Số tiền của bạn phải lớn hơn 0.";
                                    return Ok(result);
                                }

                                if (string.IsNullOrEmpty(d.inv_itemName))
                                {
                                    result.Code = -1;
                                    result.Message = "Tên sản phẩm không được để trống";
                                    return Ok(result);
                                }
                            }
                        }
                    }

                    var invoice = new Invoice();
                    var oldInvoice = new Invoice();

                    foreach (var item in model.data)
                    {
                        var id = new Guid(item.inv_InvoiceAuth_id);
                        oldInvoice = unitWork.InvoicesRepository.GetById(id);
                    }

                    var data = ApiHelper.ConvertToJson(model, 19, oldInvoice);

                    var rs = ApiHelper.PostAPI(data, url, tokens);

                    var resultObject = JsonConvert.DeserializeObject<dynamic>(rs);
                    if (resultObject.ok == "true")
                    {
                        var invoiceResult = resultObject.data;
                        InvoicedModel invoiceData = model.data.FirstOrDefault();
                        if (invoiceData != null)
                        {
                            //hủy hóa đơn cũ
                            var id = new Guid(invoiceData.inv_InvoiceAuth_id);
                            var invoiceOld = unitWork.InvoicesRepository.GetById(id);
                            invoiceOld.InvoiceStatus = 19;
                            unitWork.InvoicesRepository.Save();

                            //tạo hóa đơn mới
                            List<InvoiceDetail> details = new List<InvoiceDetail>();
                            var customer = new Customer()
                            {
                                CustomerId = Guid.NewGuid(),
                                CustomerCode = invoiceData.ma_dt,
                                CompanyId = new Guid(model.companyId),
                                FullName = invoiceData.inv_buyerDisplayName,
                                Email = invoiceData.inv_buyerEmail,
                                TaxIdentification = invoiceData.inv_buyerTaxCode,
                                Address = invoiceData.inv_buyerAddressLine,
                                AccountNumber = invoiceData.inv_buyerBankAccount,
                                Bank = invoiceData.inv_buyerBankName,
                                Status = true,
                                CustomerCompanyName = invoiceData.inv_buyerLegalName
                            };

                            unitWork.CustomersRepository.Add(customer);


                            invoice = new Invoice();
                            invoice.InvoiceId = invoiceResult.inv_InvoiceAuth_id;
                            invoice.InvoiceCodeId = oldInvoice.InvoiceCodeId; // required
                            invoice.InvoiceType = oldInvoice.InvoiceType;
                            invoice.InvoiceSeries = oldInvoice.InvoiceSeries;
                            invoice.ContractDate = oldInvoice.InvoiceIssuedDate;
                            invoice.ContractNumber = invoiceResult.inv_invoiceNumber;
                            invoice.OrderNumber = oldInvoice.OrderNumber;
                            invoice.PaymentId = oldInvoice.PaymentId;
                            invoice.Account = oldInvoice.Account;
                            invoice.Bank = oldInvoice.Bank;
                            invoice.CustomerId = oldInvoice.CustomerId;
                            //invoice.CreatedBy = Identity.UserId;
                            invoice.CreatedDate = DateTime.Now;
                            invoice.inv_originalId = oldInvoice.InvoiceId;
                            invoice.InvoiceNumber = invoiceResult.inv_invoiceNumber;
                            invoice.InvoiceName = invoiceData.inv_invoiceName == null ? string.Empty : invoiceData.inv_invoiceName; // required
                            invoice.InvoiceIssuedDate = invoiceData.inv_invoiceIssuedDate; // required
                            invoice.CurrencyCode = invoiceData.inv_currencyCode == null ? "VND" : invoiceData.inv_currencyCode; // required
                            invoice.ExchangeRate = invoiceData.inv_exchangeRate; // required
                            invoice.AdjustmentType = invoiceResult.inv_adjustmentType; // required
                            invoice.CompanyId = new Guid(model.companyId);
                            invoice.so_hd_dc = invoiceData.so_hd_dc;
                            invoice.inv_originalId = invoiceData.inv_originalId;
                            invoice.dieu_tri = invoiceData.dieu_tri;
                            invoice.in_chuyen_doi = invoiceData.in_chuyen_doi.ToString();
                            invoice.ngay_in_cdoi = invoiceData.ngay_in_cdoi;
                            invoice.InvoiceStatus = invoiceResult.trang_thai_hd;

                            if (invoiceData.details != null && invoiceData.details.Any())
                            {
                                foreach (var d in invoiceData.details)
                                {
                                    foreach (var item in d.data)
                                    {
                                        InvoiceDetail detail = new InvoiceDetail();
                                        detail.InvoiceDetailId = Guid.NewGuid();
                                        detail.InvoiceId = invoice.InvoiceId;
                                        detail.ItemCode = item.inv_itemCode;
                                        detail.ItemName = item.inv_itemName;
                                        detail.Promotion = item.inv_promotion;
                                        detail.CompanyId = new Guid(model.companyId);
                                        detail.DiscountPercent = item.inv_discountPercentage;
                                        detail.DiscountAmount = item.inv_discountAmount;
                                        detail.Quantity = item.inv_quantity;
                                        detail.TotalAmount = item.inv_TotalAmount;
                                        detail.VatAmount = item.inv_vatAmount;
                                        detail.VatPercentage = item.inv_vatPercentage;
                                        detail.TotalAmountWithoutVat = item.inv_TotalAmountWithoutVat;
                                        detail.UnitCode = item.inv_unitCode;
                                        detail.UnitName = item.inv_unitName;
                                        detail.UnitPrice = item.inv_unitPrice;

                                        unitWork.InvoiceDetailsRepository.Add(detail);
                                    }
                                }
                            }

                            unitWork.InvoicesRepository.Add(invoice);

                            unitWork.SaveChanges();

                            result.InvoiceId = invoice.InvoiceId.ToString();
                            result.InvoiceNumber = invoice.InvoiceNumber.ToString();
                            string inv_InvoiceAuth_id = invoiceResult.inv_InvoiceAuth_id;
                            var a = CopyPDFToLocal(inv_InvoiceAuth_id, tokens);
                        }
                        else
                        {
                            result.Code = -1;
                            result.Message = "Không tìm thấy hóa đơn.";
                            return Ok(result);
                        }
                    }
                    else
                    {
                        result.Code = -1;
                        result.Message = resultObject.error;
                        return Ok(result);
                    }

                    result.Data = invoice.InvoiceNumber;
                    result.Code = 0;
                    result.Message = "Điều chỉnh tăng thành công hóa đơn.";

                    return Ok(result);
                }
            }
            catch (Exception ex)
            {
                result.Code = -1;
                result.Message = ex.Message;
                return Ok(result);
            }
        }

        [HttpPost]
        [Route("api/Invoice/ChangeInvoice")]
        public IHttpActionResult ChangeInvoice(InvoiceData model)
        {
            Result result = new Result();
            var url = ConfigurationManager.AppSettings["URL_SaveInvoice"];
            var tokens = "";
            try
            {
                using (var unitWork = new UnitOfWork())
                {
                    var resultCheck = CheckCompany(model.companyId, ref tokens);

                    if (resultCheck.Code != 0)
                    {
                        return Ok(resultCheck);
                    }

                    if (string.IsNullOrEmpty(tokens))
                    {
                        result.Code = -1;
                        result.Message = "Hết phiên làm việc rồi.";
                        return Ok(result);
                    }

                    foreach (var item in model.data)
                    {
                        if (!item.paymentId.HasValue)
                        {
                            result.Code = -1;
                            result.Message = "Thông tin thanh toán chưa được nhập.";
                            return Ok(result);
                        }

                        foreach (var detail in item.details)
                        {
                            foreach (var d in detail.data)
                            {
                                if (d.inv_TotalAmount <= 0)
                                {
                                    result.Code = -1;
                                    result.Message = "Số tiền của bạn phải lớn hơn 0.";
                                    return Ok(result);
                                }

                                if (string.IsNullOrEmpty(d.inv_itemName))
                                {
                                    result.Code = -1;
                                    result.Message = "Tên sản phẩm không được để trống";
                                    return Ok(result);
                                }
                            }
                        }
                    }

                    var invoice = new Invoice();
                    var oldInvoice = new Invoice();

                    foreach (var item in model.data)
                    {
                        var id = new Guid(item.inv_InvoiceAuth_id);
                        oldInvoice = unitWork.InvoicesRepository.GetById(id);
                    }

                    var data = ApiHelper.ConvertToJson(model, 17, oldInvoice);

                    var rs = ApiHelper.PostAPI(data, url, tokens);

                    var resultObject = JsonConvert.DeserializeObject<dynamic>(rs);
                    if (resultObject.ok == "true")
                    {
                        var invoiceResult = resultObject.data;
                        InvoicedModel invoiceData = model.data.FirstOrDefault();
                        if (invoiceData != null)
                        {
                            //thay đổi trạng thái hóa đơn cũ
                            var id = new Guid(invoiceData.inv_InvoiceAuth_id);
                            oldInvoice = unitWork.InvoicesRepository.GetById(id);
                            oldInvoice.InvoiceStatus = 17;
                            unitWork.InvoicesRepository.Save();

                            result = AddViettel(model, 3, null, oldInvoice);
                            return Ok(result);
                        }
                        else
                        {
                            result.Code = -1;
                            result.Message = "Không tìm thấy hóa đơn.";
                            return Ok(result);
                        }
                    }
                    else
                    {
                        result.Code = -1;
                        result.Message = resultObject.error;
                        return Ok(result);
                    }
                }
            }
            catch (Exception ex)
            {
                result.Code = -1;
                result.Message = ex.Message;
                return Ok(result);
            }
        }

        [HttpPost]
        [Route("api/Invoice/DeleteInvoice")]
        public IHttpActionResult DeleteInvoice(InvoiceData model)
        {
            Result result = new Result();
            var url = ConfigurationManager.AppSettings["URL_SaveInvoice"];
            var tokens = "";
            try
            {
                var session = System.Web.HttpContext.Current.Session;
                using (var unitWork = new UnitOfWork())
                {
                    var resultCheck = CheckCompany(model.companyId, ref tokens);

                    if (resultCheck.Code != 0)
                    {
                        return Ok(resultCheck);
                    }

                    if (string.IsNullOrEmpty(tokens))
                    {
                        result.Code = -1;
                        result.Message = "Hết phiên làm việc rồi.";
                        return Ok(result);
                    }

                    var invoiceData = model.data.FirstOrDefault();
                    if (invoiceData != null)
                    {
                        var id = new Guid(invoiceData.inv_InvoiceAuth_id);
                        var invoice = unitWork.InvoicesRepository.GetById(id);

                        if (invoice != null)
                        {
                            var data = ApiHelper.ConvertToJson(model, 15, invoice);
                            var rs = ApiHelper.PostAPI(data, url, tokens);

                            var resultObject = JsonConvert.DeserializeObject<dynamic>(rs);
                            if (resultObject.ok == "true")
                            {
                                var dataResult = resultObject.data;
                                invoice.InvoiceStatus = dataResult.inv_adjustmentType;
                                unitWork.InvoicesRepository.Save();

                                result.InvoiceId = invoice.InvoiceId.ToString();
                                result.InvoiceNumber = invoice.InvoiceNumber.ToString();
                            }
                            else
                            {
                                result.Code = -1;
                                result.Message = resultObject.error;
                                return Ok(result);
                            }
                        }
                        else
                        {
                            result.Code = -1;
                            result.Message = "Không tìm thấy hóa đơn.";
                            return Ok(result);
                        }
                    }
                }

                result.Code = 0;
                result.Message = "Hủy thành công hóa đơn.";
                return Ok(result);
            }
            catch (Exception ex)
            {
                result.Code = -1;
                result.Message = ex.Message;
                return Ok(result);
            }
        }

        [HttpGet]
        [Route("api/Invoice/GetInvoiceType")]
        public IHttpActionResult GetInvoiceType(string companyId)
        {
            try
            {
                var tokens = "";
                using (var uow = new UnitOfWork())
                {
                    Guid company = new Guid(companyId);
                    var com = uow.CompaniesRepository.GetById(company);
                    if (com != null && com.Token != null)
                    {
                        tokens = ApiHelper.GetToken(ConfigurationManager.AppSettings["URL_LoginMinvoice"], com.Account, com.Password);
                    }
                }
                var rs = ApiHelper.GetAPI(ConfigurationManager.AppSettings["URL_InvoiceType"], tokens);

                return Ok(rs);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        [Route("api/Invoice/CheckExistPDF")]
        public IHttpActionResult CheckExistPDF(string id)
        {
            Result result = new Result();
            MemoryStream memStream = new MemoryStream();
            try
            {
                using (var unitWork = new UnitOfWork())
                {
                    var tokens = "";
                    var invoiceId = new Guid(id);
                    var invoice = unitWork.InvoicesRepository.GetById(invoiceId);
                    if (invoice != null)
                    {
                        var filePath = HostingEnvironment.MapPath(ConfigurationManager.AppSettings["localPDF"] + invoice.LocalPDF);

                        if (!File.Exists(filePath))
                        {
                            if (invoice.CompanyId.HasValue)
                            {
                                var com = unitWork.CompaniesRepository.GetById(invoice.CompanyId.Value);
                                if (com != null && com.Token != null)
                                {
                                    tokens = ApiHelper.GetToken(ConfigurationManager.AppSettings["URL_LoginMinvoice"], com.Account, com.Password);
                                }
                                invoice = CopyPDFToLocal(invoice.InvoiceId.ToString(), tokens);
                                if (invoice != null)
                                {
                                    filePath = HostingEnvironment.MapPath(ConfigurationManager.AppSettings["localPDF"] + invoice.LocalPDF);
                                }
                            }
                        }

                        if (invoice == null)
                        {
                            result.Code = -1;
                            result.Message = "Không tìm thấy hóa đơn.";
                            return Ok(result);
                        }

                        using (FileStream fileStream = File.OpenRead(filePath))
                        {
                            //create new MemoryStream object
                            memStream.SetLength(fileStream.Length);
                            //read file to MemoryStream
                            fileStream.Read(memStream.GetBuffer(), 0, (int)fileStream.Length);
                        }

                        result.Code = 0;
                        return Ok(result);

                        //var result = new HttpResponseMessage(HttpStatusCode.OK)
                        //{
                        //    Content = new ByteArrayContent(memStream.GetBuffer())
                        //};
                        //result.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment")
                        //{
                        //    FileName = $"{invoice.InvoiceName}-{invoice.InvoiceNumber}.pdf"
                        //};
                        //result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");

                        //var response = ResponseMessage(result);

                        //return response;
                    }
                    else
                    {
                        result.Code = -1;
                        result.Message = "Phát sinh lỗi.";
                        return Ok(result);
                        //return BadRequest("Không tìm thấy hóa đơn.");
                    }
                }
            }
            catch (Exception ex)
            {
                result.Code = -1;
                result.Message = ex.Message;
                return Ok(result);
            }

        }

        [HttpGet]
        [Route("api/Invoice/DownLoadPDF")]
        public IHttpActionResult DownLoadPDF(string id)
        {
            MemoryStream memStream = new MemoryStream();
            try
            {
                using (var unitWork = new UnitOfWork())
                {
                    var tokens = "";
                    var invoiceId = new Guid(id);
                    var invoice = unitWork.InvoicesRepository.GetById(invoiceId);
                    if (invoice != null)
                    {
                        var filePath = HostingEnvironment.MapPath(ConfigurationManager.AppSettings["localPDF"] + invoice.LocalPDF);

                        if (!File.Exists(filePath))
                        {
                            if (invoice.CompanyId.HasValue)
                            {
                                var com = unitWork.CompaniesRepository.GetById(invoice.CompanyId.Value);
                                if (com != null && com.Token != null)
                                {
                                    tokens = ApiHelper.GetToken(ConfigurationManager.AppSettings["URL_LoginMinvoice"], com.Account, com.Password);
                                }
                                invoice = CopyPDFToLocal(invoice.InvoiceId.ToString(), tokens);
                                if (invoice != null)
                                {
                                    filePath = HostingEnvironment.MapPath(ConfigurationManager.AppSettings["localPDF"] + invoice.LocalPDF);
                                }
                            }
                        }

                        if (invoice == null)
                        {
                            return BadRequest("Không tìm thấy hóa đơn.");
                        }

                        using (FileStream fileStream = File.OpenRead(filePath))
                        {
                            //create new MemoryStream object
                            memStream.SetLength(fileStream.Length);
                            //read file to MemoryStream
                            fileStream.Read(memStream.GetBuffer(), 0, (int)fileStream.Length);
                        }

                        var result = new HttpResponseMessage(HttpStatusCode.OK)
                        {
                            Content = new ByteArrayContent(memStream.GetBuffer())
                        };
                        result.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment")
                        {
                            FileName = $"{invoice.InvoiceName}-{invoice.InvoiceNumber}.pdf"
                        };
                        result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");

                        var response = ResponseMessage(result);

                        return response;
                    }
                    else
                    {
                        return BadRequest("Không tìm thấy hóa đơn.");
                    }
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }

        private Result Add(InvoiceData model, int status, Invoice oldInvoice = null)
        {
            Result result = new Result();
            var url = ConfigurationManager.AppSettings["URL_SaveInvoice"];
            var tokens = "";
            try
            {
                var session = System.Web.HttpContext.Current.Session;
                using (var unitWork = new UnitOfWork())
                {
                    var resultCheck = CheckCompany(model.companyId, ref tokens);

                    if (resultCheck.Code != 0)
                    {
                        return resultCheck;
                    }


                    if (string.IsNullOrEmpty(tokens))
                    {
                        result.Code = -1;
                        result.Message = "Hết phiên làm việc rồi.";
                        return result;
                    }

                    foreach (var item in model.data)
                    {
                        if (!item.paymentId.HasValue)
                        {
                            result.Code = -1;
                            result.Message = "Thông tin thanh toán chưa được nhập.";
                            return result;
                        }

                        foreach (var detail in item.details)
                        {
                            foreach (var d in detail.data)
                            {
                                if (d.inv_TotalAmount <= 0)
                                {
                                    result.Code = -1;
                                    result.Message = "Số tiền của bạn phải lớn hơn 0.";
                                    return result;
                                }

                                if (string.IsNullOrEmpty(d.inv_itemName))
                                {
                                    result.Code = -1;
                                    result.Message = "Tên sản phẩm không được để trống";
                                    return result;
                                }
                            }
                        }
                    }

                    var data = ApiHelper.ConvertToJson(model, status, oldInvoice);

                    var rs = ApiHelper.PostAPI(data, url, tokens);

                    var resultObject = JsonConvert.DeserializeObject<dynamic>(rs);

                    if (resultObject.ok == "true")
                    {
                        var dataResult = resultObject.data;

                        InvoicedModel invoiceData = model.data.FirstOrDefault();
                        if (invoiceData != null)
                        {
                            List<InvoiceDetail> details = new List<InvoiceDetail>();
                            var customer = new Customer()
                            {
                                CustomerId = Guid.NewGuid(),
                                CustomerCode = invoiceData.ma_dt,
                                CompanyId = new Guid(model.companyId),
                                FullName = invoiceData.inv_buyerDisplayName,
                                Email = invoiceData.inv_buyerEmail,
                                TaxIdentification = invoiceData.inv_buyerTaxCode,
                                Address = invoiceData.inv_buyerAddressLine,
                                AccountNumber = invoiceData.inv_buyerBankAccount,
                                Bank = invoiceData.inv_buyerBankName,
                                Status = true,
                                CustomerCompanyName = invoiceData.inv_buyerLegalName
                            };

                            unitWork.CustomersRepository.Add(customer);

                            Invoice invoice = new Invoice();
                            invoice.InvoiceId = dataResult.inv_InvoiceAuth_id;
                            invoice.InvoiceCodeId = dataResult.inv_InvoiceCode_id; // required
                            invoice.InvoiceType = invoiceData.inv_invoiceType; // required
                            invoice.InvoiceSeries = invoiceData.inv_invoiceSeries; // required
                            invoice.InvoiceNumber = dataResult.inv_invoiceNumber; // required
                            invoice.ContractDate = invoiceData.inv_invoiceIssuedDate;
                            invoice.ContractNumber = invoiceData.inv_invoiceNumber;
                            invoice.OrderNumber = invoiceData.orderNumber;
                            invoice.PaymentId = invoiceData.paymentId;
                            invoice.Account = invoiceData.inv_sellerBankAccount;
                            invoice.Bank = invoiceData.inv_sellerBankName;
                            invoice.CustomerId = customer.CustomerId;
                            //invoice.CreatedBy = Identity.UserId;
                            invoice.CreatedDate = DateTime.Now;
                            invoice.InvoiceName = dataResult.inv_invoiceName == null ? string.Empty : dataResult.inv_invoiceName; // required
                            invoice.InvoiceIssuedDate = dataResult.inv_invoiceIssuedDate; // required
                            invoice.CurrencyCode = dataResult.inv_currencyCode == null ? "VND" : dataResult.inv_currencyCode; // required
                            invoice.ExchangeRate = dataResult.inv_exchangeRate; // required
                            invoice.AdjustmentType = dataResult.inv_adjustmentType; // required
                            invoice.CompanyId = new Guid(model.companyId);
                            invoice.so_hd_dc = invoiceData.so_hd_dc;
                            invoice.dieu_tri = invoiceData.dieu_tri;
                            invoice.in_chuyen_doi = invoiceData.in_chuyen_doi.ToString();
                            invoice.ngay_in_cdoi = invoiceData.ngay_in_cdoi;

                            if (invoiceData.details != null && invoiceData.details.Any())
                            {
                                foreach (var d in invoiceData.details)
                                {
                                    foreach (var item in d.data)
                                    {
                                        InvoiceDetail detail = new InvoiceDetail();
                                        detail.InvoiceDetailId = Guid.NewGuid();
                                        detail.InvoiceId = dataResult.inv_InvoiceAuth_id;
                                        detail.ItemCode = item.inv_itemCode;
                                        detail.ItemName = item.inv_itemName;
                                        detail.Promotion = item.inv_promotion;
                                        detail.CompanyId = new Guid(model.companyId);
                                        detail.DiscountPercent = item.inv_discountPercentage;
                                        detail.DiscountAmount = item.inv_discountAmount;
                                        detail.Quantity = item.inv_quantity;
                                        detail.TotalAmount = item.inv_TotalAmount;
                                        detail.VatAmount = item.inv_vatAmount;
                                        detail.VatPercentage = item.inv_vatPercentage;
                                        detail.TotalAmountWithoutVat = item.inv_TotalAmountWithoutVat;
                                        detail.UnitCode = item.inv_unitCode;
                                        detail.UnitName = item.inv_unitName;
                                        detail.UnitPrice = item.inv_unitPrice;

                                        unitWork.InvoiceDetailsRepository.Add(detail);
                                    }
                                }
                            }
                            invoice.SupplierType = (int)InvoiceTypes.mInvoice;
                            unitWork.InvoicesRepository.Add(invoice);

                            unitWork.SaveChanges();

                            result.InvoiceId = invoice.InvoiceId.ToString();
                            result.InvoiceNumber = invoice.InvoiceNumber.ToString();
                            string inv_InvoiceAuth_id = dataResult.inv_InvoiceAuth_id;
                            var a = CopyPDFToLocal(inv_InvoiceAuth_id, tokens);
                        }
                        else
                        {
                            result.Code = -1;
                            result.Message = "Không có thông tin hóa đơn.";
                            return result;
                        }
                    }
                    else
                    {
                        result.Code = -1;
                        result.Message = resultObject.error;
                        return result;
                    }

                    result.Code = 0;
                    if (status == 19)
                    {
                        result.Message = "Đổi hóa đơn thành công.";
                    }
                    else
                    {
                        result.Message = "Phát hành thành công hóa đơn.";
                    }


                    return result;
                }
            }
            //catch (Exception e)
            //{
            //    result.Code = -1;
            //    result.Message = e.Message;
            //    return result;
            //}
            catch (DbEntityValidationException ex)
            {
                foreach (var eve in ex.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }

                result.Code = -1;
                result.Message = ex.Message;
                return result;
            }
        }

        private Result CheckCompany(string companyId, ref string tokens)
        {
            Result result = new Result();
            try
            {
                using (var unitWork = new UnitOfWork())
                {
                    if (!string.IsNullOrEmpty(companyId))
                    {
                        Guid company = new Guid(companyId);
                        var com = unitWork.CompaniesRepository.GetById(company);
                        if (com != null && com.Token != null)
                        {
                            if (com.Status.Value && com.ExpiryDate >= DateTime.Now)
                            {
                                tokens = ApiHelper.GetToken(ConfigurationManager.AppSettings["URL_LoginMinvoice"], com.Account, com.Password);

                                result.Code = 0;
                            }
                            else
                            {
                                result.Code = -1;
                                result.Message = "Đơn vị ngừng hoạt động hoặc đã hết hạn.";
                            }
                        }
                    }
                    else
                    {
                        result.Code = -1;
                        result.Message = "Bạn chưa chọn đơn vị thao tác.";
                    }

                    return result;
                }
            }
            catch (Exception ex)
            {
                result.Code = -1;
                result.Message = ex.Message;
                return result;
            }
        }

        private Invoice CopyPDFToLocal(string InvoiceId, string tokens)
        {
            var folder = ConfigurationManager.AppSettings["localPDF"];

            if (!Directory.Exists(HostingEnvironment.MapPath(folder)))
            {
                Directory.CreateDirectory(HostingEnvironment.MapPath(folder));
            }


            var url = $"{ConfigurationManager.AppSettings["URL_PDF"]}{InvoiceId}";
            string fileName = InvoiceId + ".pdf";
            string pathToSave = HostingEnvironment.MapPath(folder + fileName);
            ApiHelper.Download(url, tokens, pathToSave);
            if (System.IO.File.Exists(pathToSave))
            {
                using (var uow = new UnitOfWork())
                {
                    var invoice = uow.InvoicesRepository.GetById(new Guid(InvoiceId));
                    if (invoice != null)
                    {
                        invoice.LocalPDF = fileName;
                        uow.SaveChanges();
                    }
                    return invoice;
                }
            }
            else
            {
                return null;
            }
        }

        #endregion

        #region sInvoices

        [HttpGet]
        [Route("api/Viettel/Invoice/GetListCompanyInfomation/")]
        public IHttpActionResult GetListCompanyInfomation()
        {
            Result result = new Result
            {
                Code = 0,
                Message = "Lấy thông tin thành công"
            };
            try
            {
                using (var unitWork = new UnitOfWork())
                {
                    InfomationModel infomation = new InfomationModel();
                    infomation.Companies = unitWork.CompaniesRepository.GetWhere(x => x.Status != false && x.InvoiceType == (int)InvoiceTypes.viettel).Select(x => new CompanyModel
                    {
                        CompanyId = x.CompanyId,
                        CompanyCode = x.CompanyCode,
                        Name = x.Name,
                        TaxIdentification = x.TaxIdentification,
                    }).ToList();

                    // infomation.PaymentTypes = unitWork.PaymentTypeRepository.GetAll().Select(x => new PaymentTypeModel
                    // {
                        // PaymentId = x.PaymentId,
                        // Code = x.Code,
                        // Name = x.Name,
                        // Description = x.Description
                    // }).ToList();

                    result.Data = infomation;
                }
            }
            catch (Exception ex)
            {
                result.Code = -1;
                result.Message = ex.Message;
            }

            return Ok(result);
        }
		
		[HttpGet]
        [Route("api/Viettel/Invoice/GetListPaymentType/")]
        public IHttpActionResult GetListPaymentType()
        {
            Result result = new Result
            {
                Code = 0,
                Message = "Lấy thông tin thành công"
            };
            try
            {
                using (var unitWork = new UnitOfWork())
                {
                    result.Data = unitWork.PaymentTypeRepository.GetAll().Select(x => new PaymentTypeModel
                    {
                        PaymentId = x.PaymentId,
                        Code = x.Code,
                        Name = x.Name,
                        Description = x.Description
                    }).ToList();
                }
            }
            catch (Exception ex)
            {
                result.Code = -1;
                result.Message = ex.Message;
            }

            return Ok(result);
        }

        [HttpGet]
        [Route("api/Viettel/Invoice/GetCompanyInfomation/")]
        public IHttpActionResult GetCompanyInfomation(string id)
        {
            Result result = new Result
            {
                Code = 0,
                Message = "Lấy thông tin thành công"
            };
            try
            {
                if (string.IsNullOrEmpty(id))
                {
                    result.Code = -1;
                    result.Message = "Không có thông tin mã công ty.";
                    return Ok(result);
                }
                using (var unitWork = new UnitOfWork())
                {

                    InfomationModel infomation = new InfomationModel();
                    var company = unitWork.CompaniesRepository.GetById(new Guid(id));

                    if (company == null)
                    {
                        result.Code = -1;
                        result.Message = "Không có thông tin mã công ty.";
                        return Ok(result);
                    }

                    CompanyModel companyModel = new CompanyModel();
                    companyModel.CompanyId = company.CompanyId;
                    companyModel.CompanyCode = company.CompanyCode;
                    companyModel.Name = company.Name;
                    companyModel.Address = company.Address;
                    companyModel.Email = company.Email;
                    companyModel.Phone = company.Phone;
                    companyModel.TaxIdentification = company.TaxIdentification;
                    companyModel.InvoiceType = company.InvoiceType;
                    companyModel.PMS = company.PMS;
                    companyModel.Status = company.Status;
                    companyModel.invoiceTypes = company.InvoiceTypes != null ? company.InvoiceTypes.Select(y => new InvoiceTypeModel
                    {
                        InvoiceSeries = y.InvoiceSeries,
                        InvoiceTypes = y.InvoiceTypes
                    }).ToList() : new List<InvoiceTypeModel>();

                    result.Data = companyModel;
                }
            }
            catch (Exception ex)
            {
                result.Code = -1;
                result.Message = ex.Message;
            }

            return Ok(result);
        }

        [HttpPost]
        [Route("api/Viettel/Invoice/AddInvoice/")]
        public IHttpActionResult AddInvoiceViettel(InvoiceData model)
        {
            Result result = AddViettel(model, 1);
            return Ok(result);
        }

        [HttpPost]
        [Route("api/Viettel/Invoice/UpdateIdentificationInvoice")]
        public IHttpActionResult UpdateIdentificationInvoiceViettel(InvoiceData model)
        {
            Result result = new Result();
            var url = ConfigurationManager.AppSettings["Api_Viettel"] + ConfigurationManager.AppSettings["CreateInvoiceViettel"];
            var tokens = "";
            try
            {
                var session = System.Web.HttpContext.Current.Session;
                using (var unitWork = new UnitOfWork())
                {
                    var resultCheck = CheckCompanyViettel(model.companyId, ref tokens);

                    if (resultCheck.Code != 0)
                    {
                        return Ok(resultCheck);
                    }

                    if (string.IsNullOrEmpty(tokens))
                    {
                        result.Code = -1;
                        result.Message = "Hết phiên làm việc rồi.";
                        return Ok(result);
                    }

                    List<DetailModel> dataDetail = new List<DetailModel>();
                    var invoice = new Invoice();
                    var oldInvoice = new Invoice();

                    foreach (var item in model.data)
                    {
                        if (!item.paymentId.HasValue)
                        {
                            result.Code = -1;
                            result.Message = "Thông tin thanh toán chưa được nhập.";
                            return Ok(result);
                        }

                        if (string.IsNullOrEmpty(item.inv_invoiceType))
                        {
                            result.Code = -1;
                            result.Message = "Thông tin mẫu hóa đơn chưa được nhập.";
                            return Ok(result);
                        }

                        var id = new Guid(item.inv_InvoiceAuth_id);
                        oldInvoice = unitWork.InvoicesRepository.GetById(id);
                    }

                    var invoiceModel = model.data.FirstOrDefault();


                    var company = (Company)resultCheck.Data;
                    var paymentType = unitWork.PaymentTypeRepository.GetById(invoiceModel.paymentId.Value);
                    var data = ConvertModelToViettelInvoice(invoiceModel, paymentType, company, 5, 2, oldInvoice);
                    url = string.Format(url, company.TaxIdentification);
                    var rs = ApiHelper.PostAPIViettel(JsonConvert.SerializeObject(data), url, tokens);

                    var resultObject = JsonConvert.DeserializeObject<dynamic>(rs);

                    if (resultObject.errorCode == null)
                    {
                        var dataResult = resultObject.result;

                        InvoicedModel invoiceData = model.data.FirstOrDefault();
                        if (invoiceData != null)
                        {
                            var id = new Guid(invoiceData.inv_InvoiceAuth_id);
                            var invoiceOld = oldInvoice;
                            invoiceOld.InvoiceStatus = 5;
                            unitWork.InvoicesRepository.Save();

                            List<InvoiceDetail> details = new List<InvoiceDetail>();

                            var customer = new Customer()
                            {
                                CustomerId = Guid.NewGuid(),
                                CustomerCode = invoiceData.ma_dt,
                                CompanyId = new Guid(model.companyId),
                                FullName = invoiceData.inv_buyerDisplayName,
                                Email = invoiceData.inv_buyerEmail,
                                TaxIdentification = invoiceData.inv_buyerTaxCode,
                                Address = invoiceData.inv_buyerAddressLine,
                                AccountNumber = invoiceData.inv_buyerBankAccount,
                                Bank = invoiceData.inv_buyerBankName,
                                Status = true,
                                CustomerCompanyName = invoiceData.inv_buyerLegalName
                            };

                            unitWork.CustomersRepository.Add(customer);

                            //thêm hóa đơn
                            invoice = new Invoice();
                            invoice.InvoiceId = new Guid(data.generalInvoiceInfo.transactionUuid);
                            invoice.InvoiceCodeId = new Guid(data.generalInvoiceInfo.transactionUuid); // required
                            invoice.InvoiceType = data.generalInvoiceInfo.invoiceType; // required
                            invoice.TemplaceCode = data.generalInvoiceInfo.templateCode; // required
                            invoice.InvoiceSeries = data.generalInvoiceInfo.invoiceSeries; // required
                            invoice.InvoiceNumber = dataResult.invoiceNo; // required
                            invoice.ContractDate = invoiceData.inv_invoiceIssuedDate;
                            invoice.ContractNumber = dataResult.inv_invoiceNumber;
                            invoice.OrderNumber = invoiceData.orderNumber;
                            invoice.PaymentId = invoiceData.paymentId;
                            invoice.Account = invoiceData.inv_sellerBankAccount;
                            invoice.Bank = invoiceData.inv_sellerBankName;
                            invoice.CustomerId = customer.CustomerId;
                            invoice.inv_originalId = oldInvoice.InvoiceId;

                            //invoice.CreatedBy = Identity.UserId;
                            invoice.CreatedDate = DateTime.Now;
                            invoice.InvoiceName = dataResult.inv_invoiceName == null ? string.Empty : dataResult.inv_invoiceName; // required
                            invoice.InvoiceIssuedDate = invoiceModel.inv_invoiceIssuedDate; // required
                            invoice.CurrencyCode = data.generalInvoiceInfo.currencyCode;
                            invoice.ExchangeRate = 1; // required
                            invoice.AdjustmentType = int.Parse(data.generalInvoiceInfo.adjustmentType); // required
                            invoice.CompanyId = new Guid(model.companyId);
                            invoice.so_hd_dc = invoiceData.so_hd_dc;
                            invoice.dieu_tri = invoiceData.dieu_tri;
                            invoice.in_chuyen_doi = invoiceData.in_chuyen_doi.ToString();
                            invoice.ngay_in_cdoi = invoiceData.ngay_in_cdoi;
                            invoice.SupplierType = (int)InvoiceTypes.viettel;

                            //chi tiết hóa đơn
                            if (oldInvoice.InvoiceDetails.Any())
                            {
                                foreach (var item in oldInvoice.InvoiceDetails.ToList())
                                {
                                    InvoiceDetail detail = new InvoiceDetail();
                                    detail.InvoiceDetailId = Guid.NewGuid();
                                    detail.InvoiceId = invoice.InvoiceId;
                                    detail.ItemCode = item.ItemCode;
                                    detail.ItemName = item.ItemName;
                                    detail.Promotion = item.Promotion;
                                    detail.CompanyId = new Guid(model.companyId);
                                    detail.DiscountPercent = item.DiscountPercent;
                                    detail.DiscountAmount = item.DiscountAmount;
                                    detail.Quantity = item.Quantity;
                                    detail.TotalAmount = item.TotalAmount;
                                    detail.VatAmount = item.VatAmount;
                                    detail.VatPercentage = item.VatPercentage;
                                    detail.TotalAmountWithoutVat = item.TotalAmountWithoutVat;
                                    detail.UnitCode = item.UnitCode;
                                    detail.UnitName = item.UnitName;
                                    detail.UnitPrice = item.UnitPrice;

                                    unitWork.InvoiceDetailsRepository.Add(detail);
                                }
                            }

                            unitWork.InvoicesRepository.Add(invoice);

                            unitWork.SaveChanges();

                            result.InvoiceId = invoice.InvoiceId.ToString();
                            result.InvoiceNumber = invoice.InvoiceNumber.ToString();

                            DownloadViettelModel download = new DownloadViettelModel();
                            download.transactionUuid = invoice.InvoiceId.ToString();
                            download.invoiceNo = invoice.InvoiceNumber;
                            download.pattern = invoice.TemplaceCode;
                            download.fileType = "PDF";
                            download.supplierTaxCode = company.TaxIdentification;

                            var a = CopyPDFToLocalViettel(download, tokens);
                        }
                        else
                        {
                            result.Code = -1;
                            result.Message = "Không có thông tin hóa đơn.";
                            return Ok(result);
                        }
                    }
                    else
                    {
                        result.Code = -1;
                        result.Message = resultObject.description;
                        return Ok(result);
                    }

                    result.Data = invoice.InvoiceNumber;
                    result.Code = 0;
                    result.Message = "Điều chỉnh thành công hóa đơn.";

                    return Ok(result);
                }
            }
            catch (Exception ex)
            {
                result.Code = -1;
                result.Message = ex.Message;
                return Ok(result);
            }
        }

        [HttpPost]
        [Route("api/Viettel/Invoice/UpdateMoneyInvoice")]
        public IHttpActionResult UpdateMoneyInvoiceViettel(InvoiceData model)
        {
            Result result = new Result();
            var url = ConfigurationManager.AppSettings["Api_Viettel"] + ConfigurationManager.AppSettings["CreateInvoiceViettel"];
            var tokens = "";
            try
            {
                var session = System.Web.HttpContext.Current.Session;
                using (var unitWork = new UnitOfWork())
                {
                    var resultCheck = CheckCompanyViettel(model.companyId, ref tokens);

                    if (resultCheck.Code != 0)
                    {
                        return Ok(resultCheck);
                    }

                    if (string.IsNullOrEmpty(tokens))
                    {
                        result.Code = -1;
                        result.Message = "Hết phiên làm việc rồi.";
                        return Ok(result);
                    }

                    foreach (var item in model.data)
                    {
                        if (!item.paymentId.HasValue)
                        {
                            result.Code = -1;
                            result.Message = "Thông tin thanh toán chưa được nhập.";
                            return Ok(result);
                        }

                        foreach (var detail in item.details)
                        {
                            foreach (var d in detail.data)
                            {
                                if (d.inv_TotalAmount <= 0)
                                {
                                    result.Code = -1;
                                    result.Message = "Số tiền của bạn phải lớn hơn 0.";
                                    return Ok(result);
                                }

                                if (string.IsNullOrEmpty(item.inv_invoiceType))
                                {
                                    result.Code = -1;
                                    result.Message = "Thông tin mẫu hóa đơn chưa được nhập.";
                                    return Ok(result);
                                }

                                if (string.IsNullOrEmpty(d.inv_itemName))
                                {
                                    result.Code = -1;
                                    result.Message = "Tên sản phẩm không được để trống";
                                    return Ok(result);
                                }
                            }
                        }
                    }

                    var invoice = new Invoice();
                    var oldInvoice = new Invoice();

                    foreach (var item in model.data)
                    {
                        var id = new Guid(item.inv_InvoiceAuth_id);
                        oldInvoice = unitWork.InvoicesRepository.GetById(id);
                    }

                    var invoiceModel = model.data.FirstOrDefault();
                    var company = (Company)resultCheck.Data;
                    var paymentType = unitWork.PaymentTypeRepository.GetById(invoiceModel.paymentId.Value);
                    var data = ConvertModelToViettelInvoice(invoiceModel, paymentType, company, 5, 1, oldInvoice);
                    url = string.Format(url, company.TaxIdentification);

                    var rs = ApiHelper.PostAPIViettel(JsonConvert.SerializeObject(data), url, tokens);

                    var resultObject = JsonConvert.DeserializeObject<dynamic>(rs);
                    if (resultObject.errorCode == null)
                    {
                        var invoiceResult = resultObject.result;
                        InvoicedModel invoiceData = model.data.FirstOrDefault();
                        if (invoiceData != null)
                        {
                            //hủy hóa đơn cũ
                            var id = new Guid(invoiceData.inv_InvoiceAuth_id);
                            var invoiceOld = unitWork.InvoicesRepository.GetById(id);
                            invoiceOld.InvoiceStatus = 21;
                            unitWork.InvoicesRepository.Save();

                            //tạo hóa đơn mới
                            List<InvoiceDetail> details = new List<InvoiceDetail>();
                            var customer = new Customer()
                            {
                                CustomerId = Guid.NewGuid(),
                                CustomerCode = invoiceData.ma_dt,
                                CompanyId = new Guid(model.companyId),
                                FullName = invoiceData.inv_buyerDisplayName,
                                Email = invoiceData.inv_buyerEmail,
                                TaxIdentification = invoiceData.inv_buyerTaxCode,
                                Address = invoiceData.inv_buyerAddressLine,
                                AccountNumber = invoiceData.inv_buyerBankAccount,
                                Bank = invoiceData.inv_buyerBankName,
                                Status = true,
                                CustomerCompanyName = invoiceData.inv_buyerLegalName
                            };

                            unitWork.CustomersRepository.Add(customer);


                            invoice = new Invoice();
                            invoice.InvoiceId = new Guid(data.generalInvoiceInfo.transactionUuid);
                            invoice.InvoiceCodeId = oldInvoice.InvoiceCodeId; // required
                            invoice.InvoiceType = oldInvoice.InvoiceType; // required
                            invoice.TemplaceCode = oldInvoice.TemplaceCode; // required
                            invoice.InvoiceSeries = oldInvoice.InvoiceSeries; // required
                            invoice.ContractDate = oldInvoice.InvoiceIssuedDate;
                            invoice.ContractNumber = invoiceResult.inv_invoiceNumber;
                            invoice.InvoiceNumber = invoiceResult.invoiceNo; // required
                            invoice.OrderNumber = oldInvoice.OrderNumber;
                            invoice.PaymentId = oldInvoice.PaymentId;
                            invoice.Account = oldInvoice.Account;
                            invoice.Bank = oldInvoice.Bank;
                            invoice.CustomerId = oldInvoice.CustomerId;
                            //invoice.CreatedBy = Identity.UserId;
                            invoice.CreatedDate = DateTime.Now;
                            invoice.InvoiceName = invoiceData.inv_invoiceName == null ? string.Empty : invoiceData.inv_invoiceName;
                            invoice.InvoiceIssuedDate = invoiceData.inv_invoiceIssuedDate; // required
                            invoice.ContractDate = invoiceData.inv_invoiceIssuedDate;
                            invoice.CurrencyCode = data.generalInvoiceInfo.currencyCode; // required
                            invoice.ExchangeRate = 1; // required
                            invoice.AdjustmentType = int.Parse(data.generalInvoiceInfo.adjustmentType); // required
                            invoice.CompanyId = new Guid(model.companyId);
                            invoice.so_hd_dc = invoiceData.so_hd_dc;
                            invoice.dieu_tri = invoiceData.dieu_tri;
                            invoice.in_chuyen_doi = invoiceData.in_chuyen_doi.ToString();
                            invoice.ngay_in_cdoi = invoiceData.ngay_in_cdoi;
                            invoice.SupplierType = (int)InvoiceTypes.viettel;

                            if (invoiceData.details != null && invoiceData.details.Any())
                            {
                                foreach (var d in invoiceData.details)
                                {
                                    foreach (var item in d.data)
                                    {
                                        InvoiceDetail detail = new InvoiceDetail();
                                        detail.InvoiceDetailId = Guid.NewGuid();
                                        detail.InvoiceId = invoice.InvoiceId;
                                        detail.ItemCode = item.inv_itemCode;
                                        detail.ItemName = item.inv_itemName;
                                        detail.Promotion = item.inv_promotion;
                                        detail.CompanyId = new Guid(model.companyId);
                                        detail.DiscountPercent = item.inv_discountPercentage;
                                        detail.DiscountAmount = item.inv_discountAmount;
                                        detail.Quantity = item.inv_quantity;
                                        detail.TotalAmount = item.inv_TotalAmount;
                                        detail.VatAmount = item.inv_vatAmount;
                                        detail.VatPercentage = item.inv_vatPercentage;
                                        detail.TotalAmountWithoutVat = item.inv_TotalAmountWithoutVat;
                                        detail.UnitCode = item.inv_unitCode;
                                        detail.UnitName = item.inv_unitName;
                                        detail.UnitPrice = item.inv_unitPrice;

                                        unitWork.InvoiceDetailsRepository.Add(detail);
                                    }
                                }
                            }

                            unitWork.InvoicesRepository.Add(invoice);

                            unitWork.SaveChanges();

                            DownloadViettelModel download = new DownloadViettelModel();
                            download.transactionUuid = data.generalInvoiceInfo.transactionUuid;
                            download.invoiceNo = invoice.InvoiceNumber;
                            download.pattern = invoice.TemplaceCode;
                            download.fileType = "PDF";
                            download.supplierTaxCode = company.TaxIdentification;

                            var a = CopyPDFToLocalViettel(download, tokens);

                            result.InvoiceId = data.generalInvoiceInfo.transactionUuid;
                            result.InvoiceNumber = invoice.InvoiceNumber;
                            if (invoiceData.details != null && invoiceData.details.Any())
                            {
                                result.Detail = new DATA.Common.Detail()
                                {
                                    DetailItems = invoiceData.details.FirstOrDefault().data.Select(x => new DetailItem
                                    {
                                        InvoiceDetailId = x.inv_InvoiceAuthDetail_id.ToString(),
                                        InvoiceDetailName = x.inv_itemName
                                    }).ToList()
                                };
                            }
                        }
                        else
                        {
                            result.Code = -1;
                            result.Message = "Không tìm thấy hóa đơn.";
                            return Ok(result);
                        }
                    }
                    else
                    {
                        result.Code = -1;
                        result.Message = resultObject.description;
                        return Ok(result);
                    }

                    result.Data = invoice.InvoiceNumber;
                    result.Code = 0;
                    result.Message = "Điều chỉnh thành công hóa đơn.";

                    return Ok(result);
                }
            }
            catch (Exception ex)
            {
                result.Code = -1;
                result.Message = ex.Message;
                return Ok(result);
            }
        }

        [HttpPost]
        [Route("api/Viettel/Invoice/ChangeInvoice")]
        public IHttpActionResult ChangeInvoiceViettel(InvoiceData model)
        {
            Result result = new Result();
            var url = ConfigurationManager.AppSettings["Api_Viettel"] + ConfigurationManager.AppSettings["CreateInvoiceViettel"];
            var tokens = "";
            try
            {
                using (var unitWork = new UnitOfWork())
                {
                    var resultCheck = CheckCompanyViettel(model.companyId, ref tokens);

                    if (resultCheck.Code != 0)
                    {
                        return Ok(resultCheck);
                    }

                    if (string.IsNullOrEmpty(tokens))
                    {
                        result.Code = -1;
                        result.Message = "Hết phiên làm việc rồi.";
                        return Ok(result);
                    }

                    foreach (var item in model.data)
                    {
                        if (!item.paymentId.HasValue)
                        {
                            result.Code = -1;
                            result.Message = "Thông tin thanh toán chưa được nhập.";
                            return Ok(result);
                        }

                        foreach (var detail in item.details)
                        {
                            foreach (var d in detail.data)
                            {
                                if (d.inv_TotalAmount <= 0)
                                {
                                    result.Code = -1;
                                    result.Message = "Số tiền của bạn phải lớn hơn 0.";
                                    return Ok(result);
                                }

                                if (string.IsNullOrEmpty(d.inv_itemName))
                                {
                                    result.Code = -1;
                                    result.Message = "Tên sản phẩm không được để trống";
                                    return Ok(result);
                                }

                                if (string.IsNullOrEmpty(item.inv_invoiceType))
                                {
                                    result.Code = -1;
                                    result.Message = "Thông tin mẫu hóa đơn chưa được nhập.";
                                    return Ok(result);
                                }

                                if (string.IsNullOrEmpty(item.inv_invoiceSeries))
                                {
                                    result.Code = -1;
                                    result.Message = "Thông tin mẫu hóa đơn chưa được nhập.";
                                    return Ok(result);
                                }
                            }
                        }
                    }

                    var invoice = new Invoice();
                    var oldInvoice = new Invoice();

                    foreach (var item in model.data)
                    {
                        var id = new Guid(item.inv_InvoiceAuth_id);
                        oldInvoice = unitWork.InvoicesRepository.GetById(id);
                    }

                    var invoiceModel = model.data.FirstOrDefault();
                    var company = (Company)resultCheck.Data;
                    var paymentType = unitWork.PaymentTypeRepository.GetById(invoiceModel.paymentId.Value);
                    var data = ConvertModelToViettelInvoice(invoiceModel, paymentType, company, 3, null, oldInvoice);
                    url = string.Format(url, company.TaxIdentification);
                    var rs = ApiHelper.PostAPIViettel(JsonConvert.SerializeObject(data), url, tokens);

                    var resultObject = JsonConvert.DeserializeObject<dynamic>(rs);
                    if (resultObject.errorCode == null)
                    {
                        var dataResult = resultObject.result;
                        InvoicedModel invoiceData = model.data.FirstOrDefault();
                        if (invoiceData != null)
                        {
                            var id = new Guid(invoiceData.inv_InvoiceAuth_id);
                            var invoiceOld = oldInvoice;
                            invoiceOld.InvoiceStatus = 17;
                            unitWork.InvoicesRepository.Save();

                            List<InvoiceDetail> details = new List<InvoiceDetail>();

                            var customer = new Customer()
                            {
                                CustomerId = Guid.NewGuid(),
                                CustomerCode = invoiceData.ma_dt,
                                CompanyId = new Guid(model.companyId),
                                FullName = invoiceData.inv_buyerDisplayName,
                                Email = invoiceData.inv_buyerEmail,
                                TaxIdentification = invoiceData.inv_buyerTaxCode,
                                Address = invoiceData.inv_buyerAddressLine,
                                AccountNumber = invoiceData.inv_buyerBankAccount,
                                Bank = invoiceData.inv_buyerBankName,
                                Status = true,
                                CustomerCompanyName = invoiceData.inv_buyerLegalName
                            };

                            unitWork.CustomersRepository.Add(customer);

                            //thêm hóa đơn
                            invoice = new Invoice();
                            invoice.InvoiceId = new Guid(data.generalInvoiceInfo.transactionUuid);
                            invoice.InvoiceCodeId = new Guid(data.generalInvoiceInfo.transactionUuid); // required
                            invoice.InvoiceType = data.generalInvoiceInfo.invoiceType; // required
                            invoice.TemplaceCode = data.generalInvoiceInfo.templateCode; // required
                            invoice.InvoiceSeries = data.generalInvoiceInfo.invoiceSeries; // required
                            invoice.InvoiceNumber = dataResult.invoiceNo; // required
                            invoice.ContractDate = invoiceData.inv_invoiceIssuedDate;
                            invoice.ContractNumber = dataResult.inv_invoiceNumber;
                            invoice.OrderNumber = invoiceData.orderNumber;
                            invoice.PaymentId = invoiceData.paymentId;
                            invoice.Account = invoiceData.inv_sellerBankAccount;
                            invoice.Bank = invoiceData.inv_sellerBankName;
                            invoice.CustomerId = customer.CustomerId;
                            invoice.inv_originalId = oldInvoice.InvoiceId;

                            //invoice.CreatedBy = Identity.UserId;
                            invoice.CreatedDate = DateTime.Now;
                            invoice.InvoiceName = dataResult.inv_invoiceName == null ? string.Empty : dataResult.inv_invoiceName; // required
                            invoice.InvoiceIssuedDate = invoiceModel.inv_invoiceIssuedDate; // required
                            invoice.CurrencyCode = data.generalInvoiceInfo.currencyCode;
                            invoice.ExchangeRate = 1; // required
                            invoice.AdjustmentType = int.Parse(data.generalInvoiceInfo.adjustmentType); // required
                            invoice.CompanyId = new Guid(model.companyId);
                            invoice.so_hd_dc = invoiceData.so_hd_dc;
                            invoice.dieu_tri = invoiceData.dieu_tri;
                            invoice.in_chuyen_doi = invoiceData.in_chuyen_doi.ToString();
                            invoice.ngay_in_cdoi = invoiceData.ngay_in_cdoi;
                            invoice.SupplierType = (int)InvoiceTypes.viettel;

                            //chi tiết hóa đơn
                            if (invoiceData.details != null && invoiceData.details.Any())
                            {
                                foreach (var d in invoiceData.details)
                                {
                                    foreach (var item in d.data)
                                    {
                                        InvoiceDetail detail = new InvoiceDetail();
                                        detail.InvoiceDetailId = Guid.NewGuid();
                                        detail.InvoiceId = invoice.InvoiceId;
                                        detail.ItemCode = item.inv_itemCode;
                                        detail.ItemName = item.inv_itemName;
                                        detail.Promotion = item.inv_promotion;
                                        detail.CompanyId = new Guid(model.companyId);
                                        detail.DiscountPercent = item.inv_discountPercentage;
                                        detail.DiscountAmount = item.inv_discountAmount;
                                        detail.Quantity = item.inv_quantity;
                                        detail.TotalAmount = item.inv_TotalAmount;
                                        detail.VatAmount = item.inv_vatAmount;
                                        detail.VatPercentage = item.inv_vatPercentage;
                                        detail.TotalAmountWithoutVat = item.inv_TotalAmountWithoutVat;
                                        detail.UnitCode = item.inv_unitCode;
                                        detail.UnitName = item.inv_unitName;
                                        detail.UnitPrice = item.inv_unitPrice;

                                        unitWork.InvoiceDetailsRepository.Add(detail);
                                    }
                                }
                            }

                            unitWork.InvoicesRepository.Add(invoice);

                            unitWork.SaveChanges();

                            result.InvoiceId = invoice.InvoiceId.ToString();
                            result.InvoiceNumber = invoice.InvoiceNumber.ToString();

                            DownloadViettelModel download = new DownloadViettelModel();
                            download.transactionUuid = invoice.InvoiceId.ToString();
                            download.invoiceNo = invoice.InvoiceNumber;
                            download.pattern = invoice.TemplaceCode;
                            download.fileType = "PDF";
                            download.supplierTaxCode = company.TaxIdentification;

                            var a = CopyPDFToLocalViettel(download, tokens);
                        }
                        else
                        {
                            result.Code = -1;
                            result.Message = "Không tìm thấy hóa đơn.";
                            return Ok(result);
                        }
                    }
                    else
                    {
                        result.Code = -1;
                        result.Message = resultObject.description;
                        return Ok(result);
                    }

                    result.Data = invoice.InvoiceNumber;
                    result.Code = 0;
                    result.Message = "thay thế thành công hóa đơn.";

                    return Ok(result);
                }
            }
            catch (Exception ex)
            {
                result.Code = -1;
                result.Message = ex.Message;
                return Ok(result);
            }
        }

        [HttpPost]
        [Route("api/Viettel/Invoice/DeleteInvoice")]
        public IHttpActionResult DeleteInvoiceViettel(InvoiceData model)
        {
            Result result = new Result();
            var url = ConfigurationManager.AppSettings["Api_Viettel"] + ConfigurationManager.AppSettings["CancelInvoiceViettel"];
            var tokens = "";
            try
            {
                var session = System.Web.HttpContext.Current.Session;
                using (var unitWork = new UnitOfWork())
                {
                    var resultCheck = CheckCompanyViettel(model.companyId, ref tokens);

                    if (resultCheck.Code != 0)
                    {
                        return Ok(resultCheck);
                    }

                    if (string.IsNullOrEmpty(tokens))
                    {
                        result.Code = -1;
                        result.Message = "Hết phiên làm việc rồi.";
                        return Ok(result);
                    }

                    var invoiceData = model.data.FirstOrDefault();
                    if (invoiceData != null)
                    {
                        var id = new Guid(invoiceData.inv_InvoiceAuth_id);
                        var invoice = unitWork.InvoicesRepository.GetById(id);

                        if (invoice != null)
                        {
                            var company = (Company)resultCheck.Data;
                            var data = new CancelViettelModel();
                            data.invoiceNo = invoice.InvoiceNumber;
                            data.supplierTaxCode = company.TaxIdentification;
                            data.strIssueDate = invoice.InvoiceIssuedDate.ToString("yyyyMMddHHmmss");
                            data.additionalReferenceDate = DateTime.Now.ToString("yyyyMMddHHmmss");
                            data.additionalReferenceDesc = $"Hủy hóa đơn {invoice.InvoiceNumber}";

                            var rs = ApiHelper.PostAPIViettelByFormData(data, url, tokens);
                            var resultObject = JsonConvert.DeserializeObject<ResultViettelModel>(rs);
                            if (string.IsNullOrEmpty(resultObject.errorCode))
                            {
                                invoice.InvoiceStatus = 15;
                                unitWork.InvoicesRepository.Save();

                                result.InvoiceId = invoice.InvoiceId.ToString();
                                result.InvoiceNumber = invoice.InvoiceNumber.ToString();
                            }
                            else
                            {
                                result.Code = -1;
                                result.Message = resultObject.description;
                                return Ok(result);
                            }
                        }
                        else
                        {
                            result.Code = -1;
                            result.Message = "Không tìm thấy hóa đơn.";
                            return Ok(result);
                        }
                    }
                }

                result.Code = 0;
                result.Message = "Hủy thành công hóa đơn.";
                return Ok(result);
            }
            catch (Exception ex)
            {
                result.Code = -1;
                result.Message = ex.Message;
                return Ok(result);
            }
        }

        [HttpGet]
        [Route("api/Viettel/Invoice/DownLoadPDF")]
        public IHttpActionResult DownLoadPDFViettel(string id)
        {
            MemoryStream memStream = new MemoryStream();
            try
            {
                using (var unitWork = new UnitOfWork())
                {
                    var tokens = "";
                    var invoiceId = new Guid(id);
                    var invoice = unitWork.InvoicesRepository.GetById(invoiceId);
                    if (invoice != null)
                    {
                        var filePath = HostingEnvironment.MapPath(ConfigurationManager.AppSettings["localPDFViettel"] + invoice.LocalPDF);

                        if (!File.Exists(filePath))
                        {
                            if (invoice.CompanyId.HasValue)
                            {
                                var resultCheck = CheckCompanyViettel(invoice.CompanyId.ToString(), ref tokens);
                                if (resultCheck.Code != 0)
                                {
                                    return Ok(resultCheck);
                                }

                                var company = (Company)resultCheck.Data;
                                DownloadViettelModel data = new DownloadViettelModel();
                                data.transactionUuid = invoice.InvoiceId.ToString();
                                data.invoiceNo = invoice.InvoiceNumber;
                                data.pattern = invoice.TemplaceCode;
                                data.fileType = "PDF";
                                data.supplierTaxCode = company.TaxIdentification;
                                invoice = CopyPDFToLocalViettel(data, tokens);
                                if (invoice != null)
                                {
                                    filePath = HostingEnvironment.MapPath(ConfigurationManager.AppSettings["localPDFViettel"] + invoice.LocalPDF);
                                }
                            }
                        }

                        if (invoice == null)
                        {
                            return BadRequest("Không tìm thấy hóa đơn.");
                        }

                        using (FileStream fileStream = File.OpenRead(filePath))
                        {
                            //create new MemoryStream object
                            memStream.SetLength(fileStream.Length);
                            //read file to MemoryStream
                            fileStream.Read(memStream.GetBuffer(), 0, (int)fileStream.Length);
                        }

                        var result = new HttpResponseMessage(HttpStatusCode.OK)
                        {
                            Content = new ByteArrayContent(memStream.GetBuffer())
                        };
                        result.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment")
                        {
                            FileName = $"{invoice.InvoiceName}-{invoice.InvoiceNumber}.pdf"
                        };
                        result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");

                        var response = ResponseMessage(result);

                        return response;
                    }
                    else
                    {
                        return BadRequest("Không tìm thấy hóa đơn.");
                    }
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        [Route("api/Viettel/Invoice/GetInvoiceType")]
        public IHttpActionResult GetInvoiceTypeViettel(string companyId)
        {
            try
            {
                using (var uow = new UnitOfWork())
                {
                    Guid company = new Guid(companyId);
                    var com = uow.CompaniesRepository.GetById(company);
                    if (com != null)
                    {
                        var types = com.InvoiceTypes.Select(x => new
                        {
                            id = x.Id,
                            mau_so = x.InvoiceTypes,
                            ky_hieu = x.InvoiceSeries
                        });

                        return Ok(JsonConvert.SerializeObject(types));
                    }
                }
                return BadRequest("Đơn vị chưa đăng ký dịch vụ hóa đơn của Viettel.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        [Route("api/Viettel/Invoice/CheckExistPDF")]
        public IHttpActionResult CheckExistPDFViettel(string id)
        {
            Result result = new Result();
            MemoryStream memStream = new MemoryStream();
            try
            {
                using (var unitWork = new UnitOfWork())
                {
                    var tokens = "";
                    var invoiceId = new Guid(id);
                    var invoice = unitWork.InvoicesRepository.GetById(invoiceId);
                    if (invoice != null)
                    {
                        var filePath = HostingEnvironment.MapPath($"{ConfigurationManager.AppSettings["localPDFViettel"]}" + invoice.LocalPDF);

                        if (!File.Exists(filePath))
                        {
                            if (invoice.CompanyId.HasValue)
                            {
                                var com = unitWork.CompaniesRepository.GetById(invoice.CompanyId.Value);
                                if (com != null)
                                {
                                    tokens = Convert.ToBase64String(Encoding.UTF8.GetBytes(com.Account + ":" + com.Password));

                                    DownloadViettelModel download = new DownloadViettelModel();
                                    download.transactionUuid = invoice.InvoiceId.ToString();
                                    download.invoiceNo = invoice.InvoiceNumber;
                                    download.pattern = invoice.TemplaceCode;
                                    download.fileType = "PDF";
                                    download.supplierTaxCode = com.TaxIdentification;

                                    invoice = CopyPDFToLocalViettel(download, tokens);
                                    if (invoice != null)
                                    {
                                        filePath = HostingEnvironment.MapPath($"{ConfigurationManager.AppSettings["localPDFViettel"]}" + invoice.LocalPDF);
                                    }
                                }
                            }
                        }

                        if (invoice == null)
                        {
                            result.Code = -1;
                            result.Message = "Không tìm thấy hóa đơn.";
                            return Ok(result);
                        }

                        using (FileStream fileStream = File.OpenRead(filePath))
                        {
                            //create new MemoryStream object
                            memStream.SetLength(fileStream.Length);
                            //read file to MemoryStream
                            fileStream.Read(memStream.GetBuffer(), 0, (int)fileStream.Length);
                        }

                        result.Code = 0;
                        return Ok(result);


                    }
                    else
                    {
                        result.Code = -1;
                        result.Message = "Phát sinh lỗi.";
                        return Ok(result);
                        //return BadRequest("Không tìm thấy hóa đơn.");
                    }
                }
            }
            catch (Exception ex)
            {
                result.Code = -1;
                result.Message = ex.Message;
                return Ok(result);
            }
        }

        private Result CheckCompanyViettel(string companyId, ref string tokens)
        {
            Result result = new Result();
            try
            {
                using (var unitWork = new UnitOfWork())
                {
                    if (!string.IsNullOrEmpty(companyId))
                    {
                        Guid company = new Guid(companyId);
                        var com = unitWork.CompaniesRepository.GetById(company);
                        if (com != null)
                        {
                            if (com.Status.Value)
                            {
                                tokens = Convert.ToBase64String(Encoding.UTF8.GetBytes(com.Account + ":" + com.Password));
                                result.Data = com;
                                result.Code = 0;
                            }
                            else
                            {
                                result.Code = -1;
                                result.Message = "Đơn vị ngừng hoạt động hoặc đã hết hạn.";
                            }
                        }
                    }
                    else
                    {
                        result.Code = -1;
                        result.Message = "Bạn chưa chọn đơn vị thao tác.";
                    }

                    return result;
                }
            }
            catch (Exception ex)
            {
                result.Code = -1;
                result.Message = ex.Message;
                return result;
            }
        }

        private Invoice CopyPDFToLocalViettel(DownloadViettelModel invoiceViettel, string tokens)
        {
            var folder = ConfigurationManager.AppSettings["localPDFViettel"];

            if (!Directory.Exists(HostingEnvironment.MapPath(folder)))
            {
                Directory.CreateDirectory(HostingEnvironment.MapPath(folder));
            }

            var url = $"{ConfigurationManager.AppSettings["Api_Viettel"]}{ConfigurationManager.AppSettings["FileInvoiceViettel"]}";
            string fileName = invoiceViettel.transactionUuid + ".pdf";
            string pathToSave = HostingEnvironment.MapPath(folder + fileName);

            var data = new JavaScriptSerializer().Serialize(invoiceViettel);
            var rs = ApiHelper.PostAPIViettel(data, url, tokens);
            var fileRs = JsonConvert.DeserializeObject<ResultViettelModel>(rs);
            if (fileRs.errorCode == null)
            {
                if (System.IO.File.Exists(pathToSave))
                {
                    using (var uow = new UnitOfWork())
                    {
                        var invoice = uow.InvoicesRepository.GetById(new Guid(invoiceViettel.transactionUuid));
                        if (invoice != null)
                        {
                            invoice.LocalPDF = fileName;
                            uow.SaveChanges();
                        }
                        return invoice;
                    }
                }
                else
                {
                    File.WriteAllBytes(pathToSave, fileRs.fileToBytes);
                    using (var uow = new UnitOfWork())
                    {
                        var invoice = uow.InvoicesRepository.GetById(new Guid(invoiceViettel.transactionUuid));
                        if (invoice != null)
                        {
                            invoice.LocalPDF = fileName;
                            uow.SaveChanges();
                        }
                        return invoice;
                    }
                }
            }
            else
            {
                return null;
            }

        }

        private Result AddViettel(InvoiceData model, int adjustmentType, int? adjustmentInvoiceType = null, Invoice oldInvoice = null)
        {
            Result result = new Result();
            var url = ConfigurationManager.AppSettings["Api_Viettel"] + ConfigurationManager.AppSettings["CreateInvoiceViettel"];

            var tokens = "";
            try
            {
                var session = System.Web.HttpContext.Current.Session;
                using (var unitWork = new UnitOfWork())
                {
                    var resultCheck = CheckCompanyViettel(model.companyId, ref tokens);

                    if (resultCheck.Code != 0)
                    {
                        return resultCheck;
                    }


                    if (string.IsNullOrEmpty(tokens))
                    {
                        result.Code = -1;
                        result.Message = "Hết phiên làm việc rồi.";
                        return result;
                    }

                    foreach (var item in model.data)
                    {
                        if (!item.paymentId.HasValue)
                        {
                            result.Code = -1;
                            result.Message = "Thông tin thanh toán chưa được nhập.";
                            return result;
                        }

                        foreach (var detail in item.details)
                        {
                            foreach (var d in detail.data)
                            {
                                if (d.inv_TotalAmount <= 0)
                                {
                                    result.Code = -1;
                                    result.Message = "Số tiền của bạn phải lớn hơn 0.";
                                    return result;
                                }

                                if (string.IsNullOrEmpty(d.inv_itemName))
                                {
                                    result.Code = -1;
                                    result.Message = "Tên sản phẩm không được để trống";
                                    return result;
                                }
                            }
                        }
                    }
                    var invoiceModel = model.data.FirstOrDefault();
                    var company = (Company)resultCheck.Data;
                    var paymentType = unitWork.PaymentTypeRepository.GetById(invoiceModel.paymentId.Value);
                    var data = ConvertModelToViettelInvoice(invoiceModel, paymentType, company, adjustmentType, adjustmentInvoiceType, oldInvoice);
                    url = string.Format(url, company.TaxIdentification);
                    var rs = ApiHelper.webRequest(new Uri(url).ToString(), JsonConvert.SerializeObject(data, Formatting.None, new JsonSerializerSettings
                    {
                        NullValueHandling = NullValueHandling.Ignore
                    }), tokens, "POST", "application/json");

                    var resultObject = JsonConvert.DeserializeObject<dynamic>(rs);

                    if (resultObject.errorCode == null)
                    {
                        var dataResult = resultObject.result;

                        InvoicedModel invoiceData = model.data.FirstOrDefault();
                        if (invoiceData != null)
                        {
                            List<InvoiceDetail> details = new List<InvoiceDetail>();
                            var customer = new Customer()
                            {
                                CustomerId = Guid.NewGuid(),
                                CustomerCode = invoiceData.ma_dt,
                                CompanyId = new Guid(model.companyId),
                                FullName = invoiceData.inv_buyerDisplayName,
                                Email = invoiceData.inv_buyerEmail,
                                TaxIdentification = invoiceData.inv_buyerTaxCode,
                                Address = invoiceData.inv_buyerAddressLine,
                                AccountNumber = invoiceData.inv_buyerBankAccount,
                                Bank = invoiceData.inv_buyerBankName,
                                Status = true,
                                CustomerCompanyName = invoiceData.inv_buyerLegalName
                            };

                            unitWork.CustomersRepository.Add(customer);

                            Invoice invoice = new Invoice();
                            invoice.InvoiceId = new Guid(data.generalInvoiceInfo.transactionUuid);
                            invoice.InvoiceCodeId = new Guid(data.generalInvoiceInfo.transactionUuid); // required
                            invoice.InvoiceType = data.generalInvoiceInfo.invoiceType; // required
                            invoice.TemplaceCode = data.generalInvoiceInfo.templateCode; // required
                            invoice.InvoiceSeries = data.generalInvoiceInfo.invoiceSeries; // required
                            invoice.InvoiceNumber = dataResult.invoiceNo; // required
                            invoice.ContractDate = invoiceData.inv_invoiceIssuedDate;
                            invoice.ContractNumber = dataResult.transactionID;
                            invoice.OrderNumber = invoiceData.orderNumber;
                            invoice.PaymentId = invoiceData.paymentId;
                            invoice.Account = invoiceData.inv_sellerBankAccount;
                            invoice.Bank = invoiceData.inv_sellerBankName;
                            invoice.CustomerId = customer.CustomerId;
                            //invoice.CreatedBy = Identity.UserId;
                            invoice.CreatedDate = DateTime.Now;
                            invoice.InvoiceName = dataResult.inv_invoiceName == null ? string.Empty : dataResult.inv_invoiceName; // required
                            invoice.InvoiceIssuedDate = invoiceData.inv_invoiceIssuedDate; // required
                            invoice.CurrencyCode = data.generalInvoiceInfo.currencyCode; // required
                            invoice.ExchangeRate = 1; // required
                            invoice.AdjustmentType = int.Parse(data.generalInvoiceInfo.adjustmentType); // required
                            invoice.CompanyId = new Guid(model.companyId);
                            invoice.so_hd_dc = invoiceData.so_hd_dc;
                            invoice.dieu_tri = invoiceData.dieu_tri;
                            invoice.in_chuyen_doi = invoiceData.in_chuyen_doi.ToString();
                            invoice.ngay_in_cdoi = invoiceData.ngay_in_cdoi;
                            invoice.SupplierType = (int)InvoiceTypes.viettel;
                            if (invoiceData.details != null && invoiceData.details.Any())
                            {
                                foreach (var d in invoiceData.details)
                                {
                                    foreach (var item in d.data)
                                    {
                                        InvoiceDetail detail = new InvoiceDetail();
                                        detail.InvoiceDetailId = Guid.NewGuid();
                                        detail.InvoiceId = new Guid(data.generalInvoiceInfo.transactionUuid);
                                        detail.ItemCode = item.inv_itemCode;
                                        detail.ItemName = item.inv_itemName;
                                        detail.Promotion = item.inv_promotion;
                                        detail.CompanyId = new Guid(model.companyId);
                                        detail.DiscountPercent = item.inv_discountPercentage;
                                        detail.DiscountAmount = item.inv_discountAmount;
                                        detail.Quantity = item.inv_quantity;
                                        detail.TotalAmount = item.inv_TotalAmount;
                                        detail.VatAmount = item.inv_vatAmount;
                                        detail.VatPercentage = item.inv_vatPercentage;
                                        detail.TotalAmountWithoutVat = item.inv_TotalAmountWithoutVat;
                                        detail.UnitCode = item.inv_unitCode;
                                        detail.UnitName = item.inv_unitName;
                                        detail.UnitPrice = item.inv_unitPrice;

                                        unitWork.InvoiceDetailsRepository.Add(detail);
                                    }
                                }
                            }

                            unitWork.InvoicesRepository.Add(invoice);

                            unitWork.SaveChanges();

                            result.InvoiceId = invoice.InvoiceId.ToString();
                            result.InvoiceNumber = invoice.InvoiceNumber.ToString();

                            if (invoiceData.details != null && invoiceData.details.Any())
                            {
                                result.Detail = new DATA.Common.Detail()
                                {
                                    DetailItems = invoiceData.details.FirstOrDefault().data.Select(x => new DetailItem
                                    {
                                        InvoiceDetailId = x.inv_InvoiceAuthDetail_id.ToString(),
                                        InvoiceDetailName = x.inv_itemName
                                    }).ToList()
                                };
                            }

                            string inv_InvoiceAuth_id = invoice.InvoiceId.ToString();

                            DownloadViettelModel download = new DownloadViettelModel();
                            download.transactionUuid = data.generalInvoiceInfo.transactionUuid;
                            download.invoiceNo = invoice.InvoiceNumber;
                            download.pattern = invoice.TemplaceCode;
                            download.fileType = "PDF";
                            download.supplierTaxCode = company.TaxIdentification;

                            var a = CopyPDFToLocalViettel(download, tokens);
                        }
                        else
                        {
                            result.Code = -1;
                            result.Message = "Không có thông tin hóa đơn.";
                            return result;
                        }
                    }
                    else
                    {
                        result.Code = -1;
                        result.Message = resultObject.description;
                        return result;
                    }

                    result.Code = 0;
                    if (adjustmentType == 3)
                    {
                        result.Message = "Đổi hóa đơn thành công.";
                    }
                    else
                    {
                        result.Message = "Phát hành thành công hóa đơn.";
                    }

                    return result;
                }
            }
            catch (Exception e)
            {
                result.Code = -1;
                result.Message = e.Message;
                return result;
            }
        }

        private CreateInvoiceViettelModel ConvertModelToViettelInvoice(InvoicedModel invoice, PaymentType paymentType, Company company, int adjustmentType, int? adjustmentInvoiceType = null, Invoice oldInvoice = null)
        {
            CreateInvoiceViettelModel createInvoiceViettelModel = new CreateInvoiceViettelModel();

            #region thông tin chung
            createInvoiceViettelModel.generalInvoiceInfo = new generalInvoiceInfo();
            createInvoiceViettelModel.generalInvoiceInfo.invoiceType = invoice.inv_invoiceType.Substring(0, invoice.inv_invoiceType.Length - 5);
            createInvoiceViettelModel.generalInvoiceInfo.templateCode = invoice.inv_invoiceType;
            createInvoiceViettelModel.generalInvoiceInfo.transactionUuid = Guid.NewGuid().ToString();
            createInvoiceViettelModel.generalInvoiceInfo.invoiceSeries = invoice.inv_invoiceSeries;
            createInvoiceViettelModel.generalInvoiceInfo.invoiceIssuedDate = (long)invoice.inv_invoiceIssuedDate.Subtract(new DateTime(1970, 1, 1)).TotalMilliseconds;
            createInvoiceViettelModel.generalInvoiceInfo.currencyCode = "VND";
            createInvoiceViettelModel.generalInvoiceInfo.adjustmentType = adjustmentType.ToString();
            createInvoiceViettelModel.generalInvoiceInfo.paymentStatus = true;
            createInvoiceViettelModel.generalInvoiceInfo.paymentType = paymentType.Code;
            createInvoiceViettelModel.generalInvoiceInfo.paymentTypeName = paymentType.Name;
            createInvoiceViettelModel.generalInvoiceInfo.cusGetInvoiceRight = true;
            if (adjustmentType > 1)
            {
                if (adjustmentType == 3)
                {
                    createInvoiceViettelModel.generalInvoiceInfo.invoiceNote = "Thay thế hóa đơn";
                }
                else
                {
                    if (adjustmentInvoiceType.Value == 2)
                    {
                        createInvoiceViettelModel.generalInvoiceInfo.invoiceNote = $"Điều chỉnh thông tin khách hàng cho hóa đơn điện tử số {oldInvoice.InvoiceNumber} lập ngày {oldInvoice.InvoiceIssuedDate.ToString("dd/MM/yyyy")}";
                    }
                    else
                    {
                        createInvoiceViettelModel.generalInvoiceInfo.invoiceNote = $"Điều chỉnh tiền hàng cho hóa đơn điện tử số {oldInvoice.InvoiceNumber} lập ngày {oldInvoice.InvoiceIssuedDate.ToString("dd/MM/yyyy")}";
                    }
                }

                if (adjustmentInvoiceType.HasValue)
                {
                    createInvoiceViettelModel.generalInvoiceInfo.adjustmentInvoiceType = adjustmentInvoiceType.ToString();
                }

                createInvoiceViettelModel.generalInvoiceInfo.originalInvoiceId = oldInvoice.InvoiceNumber.ToString();
                createInvoiceViettelModel.generalInvoiceInfo.originalInvoiceIssueDate = (long)oldInvoice.InvoiceIssuedDate.Subtract(new DateTime(1970, 1, 1)).TotalMilliseconds;
                createInvoiceViettelModel.generalInvoiceInfo.additionalReferenceDesc = adjustmentType == 3 ? "Thay thế hóa đơn" : "Điều chỉnh hóa đơn";
                createInvoiceViettelModel.generalInvoiceInfo.additionalReferenceDate = (long)invoice.inv_invoiceIssuedDate.Subtract(new DateTime(1970, 1, 1)).TotalMilliseconds;
            }
            #endregion

            #region thông tin người mua
            createInvoiceViettelModel.buyerInfo = new buyerInfo();
            createInvoiceViettelModel.buyerInfo.buyerName = invoice.inv_buyerDisplayName;
            createInvoiceViettelModel.buyerInfo.buyerLegalName = invoice.inv_buyerLegalName;
            createInvoiceViettelModel.buyerInfo.buyerTaxCode = invoice.inv_buyerTaxCode;
            createInvoiceViettelModel.buyerInfo.buyerAddressLine = invoice.inv_buyerAddressLine;
            createInvoiceViettelModel.buyerInfo.buyerPhoneNumber = invoice.inv_buyerPhone;
            createInvoiceViettelModel.buyerInfo.buyerEmail = invoice.inv_buyerEmail;
            createInvoiceViettelModel.buyerInfo.buyerBankName = invoice.inv_buyerBankName;
            createInvoiceViettelModel.buyerInfo.buyerBankAccount = invoice.inv_buyerBankAccount;
            #endregion

            #region thông tin người bán
            createInvoiceViettelModel.sellerInfo = new sellerInfo();
            createInvoiceViettelModel.sellerInfo.sellerLegalName = company.Name;
            createInvoiceViettelModel.sellerInfo.sellerTaxCode = company.TaxIdentification;
            createInvoiceViettelModel.sellerInfo.sellerAddressLine = company.Address;
            createInvoiceViettelModel.sellerInfo.sellerPhoneNumber = company.Phone;
            createInvoiceViettelModel.sellerInfo.sellerEmail = company.Email;
            #endregion

            #region thông tin phương thức thanh toán
            createInvoiceViettelModel.payments = new List<payments>();
            createInvoiceViettelModel.payments.Add(new payments { paymentMethodName = paymentType.Code });
            #endregion

            #region thông tin chi tiết hóa đơn
            var detail = invoice.details.FirstOrDefault();
            if (detail != null)
            {
                createInvoiceViettelModel.itemInfo = new List<itemInfo>();
                if (adjustmentInvoiceType.HasValue && adjustmentInvoiceType.Value == 2)
                {
                    var itemInfo = new itemInfo();
                    itemInfo.lineNumber = "1";
                    itemInfo.itemName = $"Điều chỉnh thông tin khách hàng cho hóa đơn điện tử số {oldInvoice.InvoiceNumber} lập ngày {oldInvoice.InvoiceIssuedDate.ToString("dd/MM/yyyy")}";
                    createInvoiceViettelModel.itemInfo.Add(itemInfo);
                }
                else
                {
                    int index = 1;
                    foreach (var item in detail.data)
                    {
                        var itemInfo = new itemInfo();
                        itemInfo.lineNumber = index.ToString();
                        itemInfo.itemCode = item.inv_itemCode;
                        itemInfo.itemName = item.inv_itemName;
                        itemInfo.unitCode = item.inv_unitCode;
                        itemInfo.unitName = item.inv_unitName;
                        if (item.inv_unitPrice.HasValue)
                        {
                            itemInfo.unitPrice = decimal.ToInt64(item.inv_unitPrice.Value);
                        }
                        itemInfo.quantity = decimal.ToInt64(item.inv_quantity);
                        if (item.inv_TotalAmountWithoutVat.HasValue)
                        {
                            itemInfo.itemTotalAmountWithoutTax = decimal.ToInt64(item.inv_TotalAmountWithoutVat.Value);
                        }

                        if (item.inv_TotalAmount.HasValue)
                        {
                            itemInfo.itemTotalAmountWithTax = decimal.ToInt64(item.inv_TotalAmount.Value);
                        }

                        if (item.inv_discountAmount.HasValue)
                        {
                            itemInfo.discount = decimal.ToInt64(item.inv_discountAmount.Value);
                        }

                        if (item.inv_discountAmount.HasValue)
                        {
                            itemInfo.itemTotalAmountAfterDiscount = decimal.ToInt64(item.inv_TotalAmountWithoutVat.Value) - decimal.ToInt64(item.inv_discountAmount.Value);
                        }

                        if (item.inv_vatPercentage.HasValue)
                        {
                            itemInfo.taxPercentage = item.inv_vatPercentage.Value;
                        }

                        if (item.inv_vatAmount.HasValue)
                        {
                            itemInfo.taxAmount = decimal.ToInt64(item.inv_vatAmount.Value);
                        }

                        if (adjustmentInvoiceType.HasValue && adjustmentInvoiceType == 1)
                        {
                            itemInfo.adjustmentTaxAmount = 1;
                            if (oldInvoice != null)
                            {
                                var olderMoney = oldInvoice.InvoiceDetails.FirstOrDefault(x => x.InvoiceDetailId == new Guid(item.inv_InvoiceAuthDetail_id));
                                if (olderMoney != null)
                                {
                                    if (olderMoney.TotalAmount <= item.inv_TotalAmount)
                                    {
                                        itemInfo.isIncreaseItem = true;
                                    }
                                    else
                                    {
                                        itemInfo.isIncreaseItem = false;
                                    }
                                }
                                else
                                {
                                    itemInfo.isIncreaseItem = true;
                                }
                            }
                            else
                            {
                                itemInfo.isIncreaseItem = true;
                            }
                        }


                        createInvoiceViettelModel.itemInfo.Add(itemInfo);
                        index++;
                    }
                    if (oldInvoice != null)
                    {
                        if (adjustmentInvoiceType.HasValue && adjustmentInvoiceType == 1)
                        {

                            var totals = invoice.details.FirstOrDefault().data.Sum(x => x.inv_TotalAmount.HasValue ? decimal.ToDouble(x.inv_TotalAmount.Value) : 0);
                            var oldTotals = oldInvoice.InvoiceDetails.Sum(x => x.TotalAmount.HasValue ? decimal.ToDouble(x.TotalAmount.Value) : 0);
                            if (totals > oldTotals)
                            {
                                var itemInfo = new itemInfo();
                                itemInfo.lineNumber = index.ToString();
                                itemInfo.selection = 2;
                                itemInfo.itemName = $"Điều chỉnh tăng tiền cho hóa đơn điện tử số {oldInvoice.InvoiceNumber} lập ngày {oldInvoice.InvoiceIssuedDate.ToString("dd/MM/yyyy")}";
                                createInvoiceViettelModel.itemInfo.Add(itemInfo);
                            }
                            else
                            {
                                var itemInfo = new itemInfo();
                                itemInfo.lineNumber = index.ToString();
                                itemInfo.selection = 2;
                                itemInfo.itemName = $"Điều chỉnh giảm tiền cho hóa đơn điện tử số {oldInvoice.InvoiceNumber} lập ngày {oldInvoice.InvoiceIssuedDate.ToString("dd/MM/yyyy")}";
                                createInvoiceViettelModel.itemInfo.Add(itemInfo);
                            }

                        }
                        if (adjustmentInvoiceType.HasValue && adjustmentInvoiceType == 2)
                        {
                            var itemInfo = new itemInfo();
                            itemInfo.lineNumber = index.ToString();
                            itemInfo.selection = 2;
                            itemInfo.itemName = $"Điều chỉnh thông tin cho hóa đơn điện tử số {oldInvoice.InvoiceNumber} lập ngày {oldInvoice.InvoiceIssuedDate.ToString("dd/MM/yyyy")}";
                            createInvoiceViettelModel.itemInfo.Add(itemInfo);
                        }
                    }
                }

            }
            #endregion

            #region thông tin chi tiết giảm giá
            if (detail != null)
            {
                createInvoiceViettelModel.discountItemInfo = new List<discountItemInfo>();
                if (!adjustmentInvoiceType.HasValue || (adjustmentInvoiceType.HasValue && adjustmentInvoiceType.Value != 2))
                {
                    foreach (var item in detail.data)
                    {
                        if (item.inv_discountAmount.HasValue && item.inv_discountAmount.Value > 0)
                        {
                            var discountItemInfo = new discountItemInfo();
                            discountItemInfo.lineNumber = item.stt_rec0;
                            discountItemInfo.itemCode = item.inv_itemCode;
                            discountItemInfo.itemName = item.inv_itemName;
                            discountItemInfo.unitCode = item.inv_unitCode;
                            discountItemInfo.unitName = item.inv_unitName;
                            if (item.inv_discountAmount.HasValue)
                            {
                                discountItemInfo.unitPrice = decimal.ToInt64(item.inv_discountAmount.Value);
                            }
                            discountItemInfo.quantity = decimal.ToInt64(item.inv_quantity);
                            if (item.inv_TotalAmountWithoutVat.HasValue)
                            {
                                discountItemInfo.itemTotalAmountWithoutTax = decimal.ToInt64(item.inv_TotalAmountWithoutVat.Value);
                            }
                            if (item.inv_vatPercentage.HasValue)
                            {
                                discountItemInfo.taxPercentage = item.inv_vatPercentage.Value;
                            }

                            if (item.inv_vatAmount.HasValue)
                            {
                                discountItemInfo.taxAmount = decimal.ToInt64(item.inv_vatAmount.Value);
                            }

                            createInvoiceViettelModel.discountItemInfo.Add(discountItemInfo);
                        }
                    }
                }
            }
            createInvoiceViettelModel.discountItemInfo = new List<discountItemInfo>();
            #endregion

            #region thuế suất

            createInvoiceViettelModel.taxBreakdowns = new List<taxBreakdowns>();
            foreach (var item in createInvoiceViettelModel.itemInfo.GroupBy(x => x.taxPercentage))
            {
                taxBreakdowns tax = new taxBreakdowns();
                tax.taxPercentage = item.Key;
                tax.taxableAmount = item.Sum(x => x.itemTotalAmountWithoutTax);
                tax.taxAmount = item.Sum(x => x.taxAmount);

                createInvoiceViettelModel.taxBreakdowns.Add(tax);
            }

            #endregion

            #region thông tin tiền thanh toán
            if (adjustmentType == 5 && adjustmentInvoiceType.HasValue && adjustmentInvoiceType == 2)
            {
                createInvoiceViettelModel.summarizeInfo = new summarizeInfo();
                createInvoiceViettelModel.summarizeInfo.sumOfTotalLineAmountWithoutTax = invoice.details.FirstOrDefault().data.Sum(x => x.inv_TotalAmountWithoutVat.HasValue ? decimal.ToInt64(x.inv_TotalAmountWithoutVat.Value) : 0);
                createInvoiceViettelModel.summarizeInfo.totalAmountWithoutTax = invoice.details.FirstOrDefault().data.Sum(x => x.inv_TotalAmountWithoutVat.HasValue ? decimal.ToInt64(x.inv_TotalAmountWithoutVat.Value) : 0);
                createInvoiceViettelModel.summarizeInfo.totalAmountWithTax = invoice.details.FirstOrDefault().data.Sum(x => x.inv_TotalAmount.HasValue ? decimal.ToInt64(x.inv_TotalAmount.Value) : 0);
                createInvoiceViettelModel.summarizeInfo.totalTaxAmount = invoice.details.FirstOrDefault().data.Sum(x => x.inv_vatAmount.HasValue ? decimal.ToInt64(x.inv_vatAmount.Value) : 0);
                createInvoiceViettelModel.summarizeInfo.totalAmountWithTaxInWords = MoneyToText.DocTienBangChu(invoice.details.FirstOrDefault().data.Sum(x => x.inv_vatAmount.HasValue ? decimal.ToInt64(x.inv_vatAmount.Value) : 0), "Đồng");


                createInvoiceViettelModel.discountItemInfo = new List<discountItemInfo>();
                createInvoiceViettelModel.extAttribute = new List<extAttribute>();
                createInvoiceViettelModel.metadata = new List<metadata>();
            }
            else
            {
                long totalAmountWithoutTax = 0;
                long totalTaxAmount = 0;
                long totalAmount = 0;
                long discountAmount = 0;

                if (oldInvoice != null)
                {
                    totalTaxAmount = oldInvoice.InvoiceDetails.Sum(x => x.VatAmount.HasValue ? decimal.ToInt64(x.VatAmount.Value) : 0);
                    totalAmount = oldInvoice.InvoiceDetails.Sum(x => x.TotalAmount.HasValue ? decimal.ToInt64(x.TotalAmount.Value) : 0);
                    totalAmountWithoutTax = oldInvoice.InvoiceDetails.Sum(x => x.TotalAmountWithoutVat.HasValue ? decimal.ToInt64(x.TotalAmountWithoutVat.Value) : 0);
                    discountAmount = oldInvoice.InvoiceDetails.Sum(x => x.DiscountAmount.HasValue ? decimal.ToInt64(x.DiscountAmount.Value) : 0);
                }

                createInvoiceViettelModel.summarizeInfo = new summarizeInfo();
                createInvoiceViettelModel.summarizeInfo.sumOfTotalLineAmountWithoutTax = createInvoiceViettelModel.itemInfo.Sum(x => x.itemTotalAmountWithoutTax.HasValue ? decimal.ToInt64(x.itemTotalAmountWithoutTax.Value) : 0);
                createInvoiceViettelModel.summarizeInfo.totalAmountWithoutTax = createInvoiceViettelModel.itemInfo.Sum(x => x.itemTotalAmountWithoutTax.HasValue ? decimal.ToInt64(x.itemTotalAmountWithoutTax.Value) : 0);
                createInvoiceViettelModel.summarizeInfo.totalAmountWithTax = createInvoiceViettelModel.itemInfo.Sum(x => x.itemTotalAmountWithTax.HasValue ? decimal.ToInt64(x.itemTotalAmountWithTax.Value) : 0);
                createInvoiceViettelModel.summarizeInfo.totalTaxAmount = createInvoiceViettelModel.itemInfo.Sum(x => x.taxAmount.HasValue ? decimal.ToInt64(x.taxAmount.Value) : 0);
                createInvoiceViettelModel.summarizeInfo.totalAmountWithTaxInWords = MoneyToText.DocTienBangChu(createInvoiceViettelModel.itemInfo.Sum(x => x.itemTotalAmountWithTax.HasValue ? decimal.ToInt64(x.itemTotalAmountWithTax.Value) : 0), " Đồng");
                createInvoiceViettelModel.summarizeInfo.discountAmount = createInvoiceViettelModel.itemInfo.Sum(x => x.discount.HasValue ? decimal.ToInt64(x.discount.Value) : 0);
                createInvoiceViettelModel.summarizeInfo.totalAmountAfterDiscount = createInvoiceViettelModel.summarizeInfo.sumOfTotalLineAmountWithoutTax - createInvoiceViettelModel.summarizeInfo.discountAmount;

                if (adjustmentInvoiceType.HasValue && adjustmentInvoiceType == 1)
                {
                    if (createInvoiceViettelModel.summarizeInfo.totalAmountWithoutTax < totalAmountWithoutTax)
                    {
                        createInvoiceViettelModel.summarizeInfo.isTotalAmtWithoutTaxPos = false;
                    }
                    else
                    {
                        createInvoiceViettelModel.summarizeInfo.isTotalAmtWithoutTaxPos = true;
                    }


                    if (createInvoiceViettelModel.summarizeInfo.totalAmountWithTax < totalAmount)
                    {
                        createInvoiceViettelModel.summarizeInfo.isTotalAmountPos = false;
                    }
                    else
                    {
                        createInvoiceViettelModel.summarizeInfo.isTotalAmountPos = true;
                    }

                    if (createInvoiceViettelModel.summarizeInfo.totalTaxAmount < totalTaxAmount)
                    {
                        createInvoiceViettelModel.summarizeInfo.isTotalTaxAmountPos = false;
                    }
                    else
                    {
                        createInvoiceViettelModel.summarizeInfo.isTotalTaxAmountPos = true;
                    }

                    if (createInvoiceViettelModel.summarizeInfo.discountAmount < discountAmount)
                    {
                        createInvoiceViettelModel.summarizeInfo.isDiscountAmtPos = false;
                    }
                    else
                    {
                        createInvoiceViettelModel.summarizeInfo.isDiscountAmtPos = true;
                    }
                }



                createInvoiceViettelModel.extAttribute = new List<extAttribute>();
                createInvoiceViettelModel.metadata = new List<metadata>();
            }

            #endregion

            return createInvoiceViettelModel;
        }

        #endregion
    }
}