﻿using System;
using System.Collections.Generic;
using System.Configuration;
using INVOICESMANA.DATA.Common;
using System.Net;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using System.Web.Http;

namespace INVOICESMANA
{
    public class CompanyController : ApiController
    {
        [HttpGet]
        [Route("api/Company/RefreshToken")]
        public IHttpActionResult RefreshToken(string id)
        {
            Result result = new Result();
            try
            {
                string UserName = string.Empty;
                string Password = string.Empty;
                using (var uow = new UnitOfWork())
                {
                    var com = uow.CompaniesRepository.GetById(new Guid(id));
                    if (com != null)
                    {
                        UserName = com.Account;
                        Password = com.Password;
                    }
                }
                var request = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings["URL_LoginMinvoice"]);

                string json = new JavaScriptSerializer().Serialize(new
                {
                    username = UserName,
                    password = Password,
                    ma_dvcs = ConfigurationManager.AppSettings["ma_dvcs"]
                });

                var data = System.Text.Encoding.ASCII.GetBytes(json);

                request.Method = "POST";
                request.ContentType = "application/json";
                request.ContentLength = data.Length;

                using (var stream = request.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }

                var response = (HttpWebResponse)request.GetResponse();
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var responseString = new System.IO.StreamReader(response.GetResponseStream()).ReadToEnd();
                    var temp = JsonConvert.DeserializeObject<Dictionary<string, string>>(responseString);
                    using (var uow = new UnitOfWork())
                    {
                        var com = uow.CompaniesRepository.GetById(new Guid(id));
                        if (com != null)
                        {
                            com.Token = temp["token"].ToString();
                            uow.SaveChanges();
                        }
                    }
                    result.Data = string.Format("{0}", temp["token"].ToString());
                }
                else
                {
                    result.Message = response.StatusDescription;
                    result.Code = -2;
                }
            }
            catch (Exception ex)
            {

                result.Code = -1;
                result.Message = ex.Message;
            }
            return Ok(result);
        }
    }
}