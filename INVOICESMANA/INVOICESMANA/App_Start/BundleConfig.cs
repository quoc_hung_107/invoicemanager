﻿using System.Web;
using System.Web.Optimization;

namespace INVOICESMANA
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-2.2.4.min.js",
                        "~/Scripts/jquery-ui.js",
                        "~/Scripts/jquery.notification.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/appscript").Include(
                        "~/Scripts/js.cookie.js",
                        "~/Scripts/jquery.inputmask.js",
                        "~/Scripts/inputmask.numeric.extensions.js",
                        "~/Scripts/AppScript/AppConfig.js",
                        "~/Scripts/AppScript/Utilities.js",
                        "~/Scripts/lodash.js",
                        "~/Scripts/moment.js",
                        "~/Scripts/moment-with-locales.min.js",
                        "~/Scripts/bootstrap-datetimepicker.min.js",
                        "~/Scripts/jquery.dataTables.min.js",
                        "~/Scripts/dataTables.bootstrap.min.js",
                        "~/Scripts/dataTables.cellEdit.js",
                        "~/Scripts/AppScript/UsersManagement.js",
                        "~/Scripts/AppScript/RolesManagement.js",
                        "~/Scripts/AppScript/CompaniesManagement.js",
                        "~/Scripts/AppScript/InvoicesManagement.js",
                        "~/Scripts/AppScript/InvoiceViettelManagement.js"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/Font-Awesome-4.7.0/css/font-awesome.min.css",
                      "~/Content/bootstrap.css",
                      //"~/Content/jquery.dataTables.min.css",
                      "~/Content/bootstrap-datetimepicker.min.css",
                      "~/Content/dataTables.bootstrap.min.css",
                      "~/Content/jquery-ui.css",
                      "~/Content/jquery.notification.css",
                      "~/Content/site.css"));
        }
    }
}
