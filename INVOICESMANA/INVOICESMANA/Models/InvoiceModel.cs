﻿using INVOICESMANA.DATA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace INVOICESMANA.Models
{
    public class InvoiceData
    {
        public string companyId { get; set; }
        public string windowid { get; set; } = "WIN00187";
        public string userId { get; set; }
        public string editmode { get; set; } = "1";
        public List<InvoicedModel> data { get; set; }
    }

    public class InvoicedModel
    {
        public string inv_InvoiceAuth_id { get; set; }
        public string inv_invoiceType { get; set; }
        public string inv_invoiceSeries { get; set; }
        public string inv_InvoiceCode_id { get; set; }
        public string inv_invoiceNumber { get; set; }
        public string inv_invoiceName { get; set; }
        public string inv_currencyCode { get; set; }
        public decimal inv_exchangeRate { get; set; }
        public int inv_adjustmentType { get; set; }
        public string inv_buyerAddressLine { get; set; }
        public string inv_buyerBankAccount { get; set; }
        public string inv_buyerBankName { get; set; }
        public string inv_buyerDisplayName { get; set; }
        public string inv_buyerEmail { get; set; }
        public string inv_buyerLegalName { get; set; }
        public string inv_buyerTaxCode { get; set; }
        public string inv_buyerPhone { get; set; }
        public string inv_paymentMethodName { get; set; }
        public Guid? inv_originalId { get; set; }
        public string ma_dt { get; set; }
        public string inv_sellerBankAccount { get; set; }
        public string inv_sellerBankName { get; set; }
        public DateTime inv_invoiceIssuedDate { get; set; }
        public string trang_thai { get; set; }
        public string nguoi_ky { get; set; }
        public string sobaomat { get; set; }
        public int trang_thai_hd { get; set; }
        public bool in_chuyen_doi { get; set; }
        public string ngay_ky { get; set; }
        public string nguoi_in_cdoi { get; set; }
        public DateTime? ngay_in_cdoi { get; set; }
        public string mau_hd { get; set; }
        public string dieu_tri { get; set; }
        public string so_hd_dc { get; set; }
        public string ngayvb { get; set; }
        public string sovb { get; set; }
        public string ma_ct { get; set; }
        public Guid? paymentId { get; set; }
        public string orderNumber { get; set; }
        public string inv_contractNumber { get; set; }
        public DateTime inv_contractDate { get; set; }
        public string ma_dvcs { get; set; }
        public List<InvoicedDetailModel> details { get; set; }
    }

    public class InvoicedDetailModel
    {
        public string tab_id { get; set; }
        public string tab_table { get; set; }
        public List<DetailModel> data { get; set; }
    }

    public class DetailModel
    {
        public string inv_InvoiceAuthDetail_id { get; set; }
        public string inv_InvoiceAuth_id { get; set; }
        public string stt_rec0 { get; set; }
        public string inv_itemCode { get; set; }
        public string inv_itemName { get; set; }
        public string inv_unitCode { get; set; }
        public string inv_unitName { get; set; }
        public decimal? inv_unitPrice { get; set; }
        public decimal inv_quantity { get; set; }
        public decimal? inv_TotalAmountWithoutVat { get; set; }
        public int? inv_vatPercentage { get; set; }
        public decimal? inv_vatAmount { get; set; }
        public decimal? inv_TotalAmount { get; set; }
        public bool inv_promotion { get; set; }
        public decimal? inv_discountPercentage { get; set; }
        public decimal? inv_discountAmount { get; set; }
        public string ma_thue { get; set; }
    }
}