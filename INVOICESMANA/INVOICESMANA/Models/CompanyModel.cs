﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace INVOICESMANA.Models
{
    public class CompanyModel
    {
        public Guid CompanyId { get; set; }
        public string CompanyCode { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string TaxIdentification { get; set; }
        public string Logo { get; set; }
        public bool? Status { get; set; }
        public string PMS { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public string Account { get; set; }
        public string Password { get; set; }
        public Guid? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string Token { get; set; }
        public int? InvoiceType { get; set; }
        public bool IsUses { get; set; }

        public List<InvoiceTypeModel> invoiceTypes { get; set; }
    }
}