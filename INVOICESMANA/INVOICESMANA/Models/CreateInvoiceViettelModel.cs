﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace INVOICESMANA.Models
{
    public class CreateInvoiceViettelModel
    {
        public CreateInvoiceViettelModel()
        {
            generalInvoiceInfo generalInvoiceInfo = new generalInvoiceInfo();
            buyerInfo buyerInfo = new buyerInfo();
            List<extAttribute> extAttribute = new List<extAttribute>();
            List<payments> payments = new List<payments>();
            deliveryInfo deliveryInfo = new deliveryInfo();
            List<itemInfo> itemInfo = new List<itemInfo>();
            List<discountItemInfo> discountItemInfo = new List<discountItemInfo>();
            List<metadata> metadata = new List<metadata>();
            invoiceFile invoiceFile = new invoiceFile();
            List<taxBreakdowns> taxBreakdowns = new List<taxBreakdowns>();
            summarizeInfo summarizeInfo = new summarizeInfo();
        }
        public generalInvoiceInfo generalInvoiceInfo { get; set; }
        public buyerInfo buyerInfo { get; set; }
        public sellerInfo sellerInfo { get; set; }
        public List<extAttribute> extAttribute { get; set; }
        public List<payments> payments { get; set; }
        public deliveryInfo deliveryInfo { get; set; }
        public List<itemInfo> itemInfo { get; set; }
        public List<discountItemInfo> discountItemInfo { get; set; }
        public List<metadata> metadata { get; set; }
        public invoiceFile invoiceFile { get; set; }
        public List<taxBreakdowns> taxBreakdowns { get; set; }
        public summarizeInfo summarizeInfo { get; set; }
    }

    public class generalInvoiceInfo
    {
        public string invoiceType { get; set; }
        public string templateCode { get; set; }
        public string invoiceSeries { get; set; }
        public long invoiceIssuedDate { get; set; }
        public string currencyCode { get; set; }
        /// <summary>
        /// Lập hóa đơn điều chỉnh: adjustmentType = ‘5’
        /// Lập hóa đơn thay thế: adjustmentType = ‘3’
        /// Tạo hóa đơn: adjustmentType = ‘1’
        /// </summary>
        public string adjustmentType { get; set; }
        public string invoiceNo { get; set; }

        #region bắt buộc khi thay thế hoặc điều chỉnh thông tin
        public string invoiceNote { get; set; }
        public string originalInvoiceId { get; set; }
        /// <summary>
        /// adjustmentInvoiceType =’1’ nếu là hóa đơn điều chỉnh tiền
        /// adjustmentInvoiceType = ‘2’ nếu là hóa đơn điều chỉnh thông tin
        /// </summary>
        public string adjustmentInvoiceType { get; set; }
        public long? originalInvoiceIssueDate { get; set; }
        public string additionalReferenceDesc { get; set; }
        public long? additionalReferenceDate { get; set; }
        #endregion

        public bool? paymentStatus { get; set; }
        public string paymentType { get; set; }
        public string paymentTypeName { get; set; }
        public bool? cusGetInvoiceRight { get; set; }
        public string buyerIdNo { get; set; }
        public string buyerIdType { get; set; }
        public string transactionUuid { get; set; }
        public string userName { get; set; }
    }
    public class buyerInfo
    {
        public string buyerName { get; set; }
        public string buyerLegalName { get; set; }
        public string buyerTaxCode { get; set; }
        public string buyerAddressLine { get; set; }
        public string buyerPhoneNumber { get; set; }
        public string buyerFaxNumber { get; set; }
        public string buyerEmail { get; set; }
        public string buyerBankName { get; set; }
        public string buyerBankAccount { get; set; }
        public string buyerDistrictName { get; set; }
        public string buyerCityName { get; set; }
        public string buyerCountryCode { get; set; }
        public string buyerIdNo { get; set; }
        public string buyerIdType { get; set; }
    }
    public class sellerInfo
    {
        public string sellerLegalName { get; set; }
        public string sellerTaxCode { get; set; }
        public string sellerAddressLine { get; set; }
        public string sellerPhoneNumber { get; set; }
        public string sellerEmail { get; set; }
        public string sellerBankName { get; set; }
        public string sellerBankAccount { get; set; }
        public string sellerDistrictName { get; set; }
        public string sellerCityName { get; set; }
        public string sellerCountryCode { get; set; }
    }
    public class extAttribute
    {

    }
    public class payments
    {
        public string paymentMethodName { get; set; }
    }
    public class itemInfo
    {
        public string lineNumber { get; set; }
        public string itemCode { get; set; }
        public string itemName { get; set; }
        public string unitCode { get; set; }
        public string unitName { get; set; }
        public long? unitPrice { get; set; }
        public long? quantity { get; set; }
        public long? itemTotalAmountWithoutTax { get; set; }
        public long? itemTotalAmountWithTax { get; set; }
        public long? itemTotalAmountAfterDiscount { get; set; }
        public long? discount { get; set; }
        public long? taxPercentage { get; set; }
        public long? taxAmount { get; set; }
        public long? adjustmentTaxAmount { get; set; }
        public bool? isIncreaseItem { get; set; }
        public string itemNote { get; set; }
        public string batchNo { get; set; }
        public string expDate { get; set; }
        public int? selection { get; set; }

    }
    public class deliveryInfo
    {

    }
    public class discountItemInfo
    {
        public string lineNumber { get; set; }
        public string itemCode { get; set; }
        public string itemName { get; set; }
        public string unitCode { get; set; }
        public string unitName { get; set; }
        public double? unitPrice { get; set; }
        public double? quantity { get; set; }
        public double? itemTotalAmountWithoutTax { get; set; }
        public double? taxPercentage { get; set; }
        public double? taxAmount { get; set; }
        public double? adjustmentTaxAmount { get; set; }
        public bool? isIncreaseItem { get; set; }
    }
    public class metadata
    {

    }
    public class invoiceFile
    {
        public string fileContent { get; set; }
        public string fileType { get; set; }
        public string fileExtension { get; set; }
    }
    public class meterReading
    {
        public double? previousIndex { get; set; }
        public double? currentIndex { get; set; }
        public double? factor { get; set; }
        public double? amount { get; set; }
    }
    public class summarizeInfo
    {
        public long sumOfTotalLineAmountWithoutTax { get; set; }
        public long totalAmountWithoutTax { get; set; }
        public long totalTaxAmount { get; set; }
        public long totalAmountWithTax { get; set; }
        public long? totalAmountWithTaxFrn { get; set; }
        public string totalAmountWithTaxInWords { get; set; }
        public bool? isTotalAmountPos { get; set; }
        public bool? isTotalTaxAmountPos { get; set; }
        public bool? isTotalAmtWithoutTaxPos { get; set; }
        public long? discountAmount { get; set; }
        public long? totalAmountAfterDiscount { get; set; }
        public bool? isDiscountAmtPos { get; set; }
    }
    public class taxBreakdowns
    {
        public double? taxPercentage { get; set; }
        public double? taxableAmount { get; set; }
        public double? taxAmount { get; set; }
        public double? taxableAmountPos { get; set; }
        public double? taxAmountPos { get; set; }
        public string taxExemptionReason { get; set; }
    }
}