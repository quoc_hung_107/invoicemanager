﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace INVOICESMANA.Models
{
    public class LoginRequest
    {
        [Required]
        [Display(Name = "User name")]
        public string Username { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }

    public class ResetPasswordRequest
    {
        [Required]
        [Display(Name = "User name")]
        public string Username { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Confirm Password")]
        public string ConfirmPassword { get; set; }
    }

    public class DataTableParams
    {
        public int iDisplayStart { get; set; }
        public int iDisplayLength { get; set; }
        public string sSearch { get; set; }
        public string sSortDir_0 { get; set; }
        public int iSortCol_0 { get; set; }
        public Object customData { get; set; }
        public string companyId { get; set; }
    }

    public class DataTableRespond
    {
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public Object data { get; set; }
    }

    public class UserModel
    {
        public System.Guid UserId { get; set; }
        public string UserName { get; set; }
        public string FullName { get; set; }
        public Nullable<System.Guid> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.Guid> LastModifiedBy { get; set; }
        public Nullable<System.DateTime> LastModifiedDate { get; set; }
        public string Email { get; set; }
        public System.Guid CompanyId { get; set; }
        public string CompanyName { get; set; }
        public List<Guid> RolesID { get; set; }
        public List<string> RolesName { get; set; }
        public Nullable<bool> Status { get; set; }
    }

    public class DataUser : DATA.User
    {
        public List<Guid> RolesId { get; set; }
    }
    public class UserUpdateModel
    {
        public DATA.User User { get; set; }
        public List<Guid> UserRoleId { get; set; }
        public List<INVOICESMANA.DATA.Company> Companies { get; set; }
        public List<INVOICESMANA.DATA.Role> Roles { get; set; }
    }

    public class RoleRespondModel
    {
        public Guid RoleId { get; set; }
        public int UsersInRole { get; set; }
        public string Name { get; set; }
    }

    public class RoleRightsModel
    {
        public Guid RoleId { get; set; }
        public Guid ModuleId { get; set; }
        public string ModuleName { get; set; }
        public bool IsAddAllowed { get; set; }
        public bool IsUpdateAllowed { get; set; }
        public bool IsViewAllowed { get; set; }
        public bool IsDeleteAllowed { get; set; }
        public bool IsAllAllowed { get; set; }

    }

    public class UserSimple
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public Guid UserId { get; set; }
        public List<Guid> SRolesId { get; set; }
        public string RoleId { get; set; }
    }

    public class PermisionOnPage
    {
        public bool IsViewAllowed { get; set; }
        public bool IsAddAllowed { get; set; }
        public bool IsUpdateAllowed { get; set; }
        public bool IsDeleteAllowed { get; set; }
    }

    public class SelectItem
    {
        public string Value { get; set; }
        public string Text { get; set; }
        public Object Attributes { get; set; }
    }

    public class UserParams
    {
        public System.Guid UserId { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string PasswordSalt { get; set; }
        public string FullName { get; set; }
        public Nullable<System.Guid> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.Guid> LastModifiedBy { get; set; }
        public Nullable<System.DateTime> LastModifiedDate { get; set; }
        public string Email { get; set; }
        public Nullable<bool> EmailConfirmed { get; set; }
        public System.Guid CompanyId { get; set; }
        public Nullable<bool> Status { get; set; }
        public List<Guid> Role { get; set; }
        public List<SelectItem> Roles { get; set; }
        public List<SelectItem> Companies { get; set; }
    }

    public class DroplistDependency
    {
        public List<SelectItem> VAT { get; set; }
        public List<SelectItem> PaymentType { get; set; }
    }

    public class InvoiceModel
    {
        public System.Guid InvoiceId { get; set; }
        public Nullable<System.Guid> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.Guid> LastModifiedBy { get; set; }
        public Nullable<System.DateTime> LastModifiedDate { get; set; }
        public string InvoiceNumber { get; set; }
        public string InvoiceName { get; set; }
        public string OrderNumber { get; set; }
        public string InvoiceType { get; set; }
        public Nullable<System.DateTime> ContractDate { get; set; }
        public Nullable<System.DateTime> InvoiceIssuedDate { get; set; }
        public string CustomerName { get; set; }
        public string CompanyName { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public decimal? TotalAmount { get; set; }
        public string PaymentType { get; set; }
        public int? Status { get; set; }
        public string PDF { get; set; }

        public int AdjustmentType { get; set; }
        public string AdjustmentTypeName { get; set; }
    }

    public class InvoiceDetailModel
    {
        public System.Guid InvoiceId { get; set; }
        public Nullable<System.Guid> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.Guid> LastModifiedBy { get; set; }
        public Nullable<System.DateTime> LastModifiedDate { get; set; }
        public string InvoiceType { get; set; }
        public System.Guid InvoiceCodeId { get; set; }
        public string InvoiceSeries { get; set; }
        public string InvoiceNumber { get; set; }
        public string InvoiceName { get; set; }
        public System.DateTime InvoiceIssuedDate { get; set; }
        public string CurrencyCode { get; set; }
        public decimal ExchangeRate { get; set; }
        public int AdjustmentType { get; set; }
        public Nullable<System.Guid> CustomerId { get; set; }

        public Nullable<System.Guid> CustomerCompanyId { get; set; }
        public string CustomerCode { get; set; }
        public string CustomerFullName { get; set; }
        public string CustomerTaxIdentification { get; set; }
        public string CustomerAddress { get; set; }
        public string CustomerAccountNumber { get; set; }
        public string CustomerEmail { get; set; }
        public string CustomerBank { get; set; }
        public string CustomerCompanyName { get; set; }
        public string PaymentMethodName { get; set; }
        public Nullable<System.Guid> PaymentId { get; set; }
        public string SellerBankAccount { get; set; }
        public string SellerBankName { get; set; }
        public Nullable<int> Status { get; set; }
        public string Signer { get; set; }
        public string SecretNumber { get; set; }
        public Nullable<int> InvoiceStatus { get; set; }
        public Nullable<bool> PrintStatus { get; set; }
        public Nullable<System.DateTime> SignedDate { get; set; }
        public string PrintBy { get; set; }
        public string Code_CT { get; set; }
        public DateTime? SubmittedDate { get; set; }
        public string ContractNumber { get; set; }
        public DateTime? ContractDate { get; set; }
        public string InvoiceNote { get; set; }
        public string AdditionalReferenceDes { get; set; }
        public Nullable<System.DateTime> AdditionalReferenceDate { get; set; }
        public Nullable<decimal> DiscountAmount { get; set; }
        public string OrderNumber { get; set; }
        public string LocalPDF { get; set; }
        public Nullable<System.Guid> CompanyId { get; set; }
        public string Account { get; set; }
        public string Bank { get; set; }
        public string so_benh_an { get; set; }
        public string sovb { get; set; }
        public Nullable<System.DateTime> ngayvb { get; set; }
        public string so_hd_dc { get; set; }
        public Nullable<System.Guid> inv_originalId { get; set; }
        public string dieu_tri { get; set; }
        public string ma_dvcs { get; set; }
        public string in_chuyen_doi { get; set; }
        public Nullable<System.DateTime> ngay_in_cdoi { get; set; }
        public List<Detail> Details { get; set; }
    }

    public class Detail
    {
        public System.Guid InvoiceDetailId { get; set; }
        public System.Guid InvoiceId { get; set; }
        public System.Guid CompanyId { get; set; }
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public string UnitCode { get; set; }
        public string UnitName { get; set; }
        public Nullable<decimal> UnitPrice { get; set; }
        public decimal Quantity { get; set; }
        public Nullable<decimal> TotalAmountWithoutVat { get; set; }
        public Nullable<decimal> VatPercentage { get; set; }
        public Nullable<decimal> VatAmount { get; set; }
        public Nullable<decimal> TotalAmount { get; set; }
        public bool Promotion { get; set; }
        public Nullable<decimal> DiscountPercent { get; set; }
        public Nullable<decimal> DiscountAmount { get; set; }
    }
}