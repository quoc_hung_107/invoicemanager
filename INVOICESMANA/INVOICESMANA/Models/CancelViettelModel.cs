﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace INVOICESMANA.Models
{
    public class CancelViettelModel
    {
        public string supplierTaxCode { get; set; }
        public string invoiceNo { get; set; }
        /// <summary>
        /// Ngày lập hóa đơn yyyyMMddHHmmss
        /// </summary>
        public string strIssueDate { get; set; }
        
        public string additionalReferenceDesc { get; set; }
        /// <summary>
        /// Ngày thỏa thuận yyyyMMddHHmmss
        /// </summary>
        public string additionalReferenceDate { get; set; }
    }
}