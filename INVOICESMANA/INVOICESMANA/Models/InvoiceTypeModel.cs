﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace INVOICESMANA.Models
{
    public class InvoiceTypeModel
    {
        public string InvoiceSeries { get; set; }
        public string InvoiceTypes { get; set; }
    }
}