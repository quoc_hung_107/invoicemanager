﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace INVOICESMANA.Models
{
    public class DownloadViettelModel
    {
        public string supplierTaxCode { get; set; }
        public string invoiceNo { get; set; }
        public string pattern { get; set; }
        public string transactionUuid { get; set; }
        /// <summary>
        /// Zip or PDF
        /// </summary>
        public string fileType { get; set; }
        public bool? paid { get; set; }
    }
}