﻿using INVOICESMANA.DATA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace INVOICESMANA.Models
{
    public class ResultViettelModel
    {
        public string errorCode { get; set; }
        public string description { get; set; }
        public string fileName { get; set; }
        public byte[] fileToBytes { get; set; }
    }

    public class InfomationModel
    {
        public List<CompanyModel> Companies { get; set; }
        public List<PaymentTypeModel> PaymentTypes { get; set; }
    }
}