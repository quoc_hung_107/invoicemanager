﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using Newtonsoft.Json.Linq;
using INVOICESMANA.Models;
using System.Configuration;
using INVOICESMANA.DATA;
using System.Text;
using System.Net.Security;
using System.IO;

namespace INVOICESMANA.Helper
{
    public class ApiHelper
    {
        #region mInvoices
        public static string PostAPI(string data, string url, string token)
        {
            WebClient client = new WebClient();
            client.Encoding = System.Text.Encoding.UTF8;
            client.Headers.Add(HttpRequestHeader.ContentType, "application/json");
            client.Headers.Add(HttpRequestHeader.Authorization, token);

            string rsl = client.UploadString(url, data);
            return rsl;
        }

        public static string GetAPI(string url, string token, string data = "")
        {
            WebClient client = new WebClient();
            client.Headers.Add("Content-Type", "application/json");
            client.Headers.Add("Authorization", $"{token}");

            return client.DownloadString(url);
        }

        public static string GetToken(string url, string user, string pass)
        {
            WebClient client2 = new WebClient();
            client2.Headers.Add(HttpRequestHeader.ContentType, "application/json");
            client2.Encoding = System.Text.Encoding.UTF8;
            string json = $"{{\"username\":\"{user}\",\"password\":\"{pass}\",\"ma_dvcs\":\"{ConfigurationManager.AppSettings["ma_dvcs"]}\"}}";
            string reslt = client2.UploadString(url, json);
            JObject tk = JObject.Parse(reslt);
            string token = "Bear " + tk["token"].ToString() + ";VP";

            return token;
        }

        public static void Download(string url, string token, string path)
        {
            WebClient webClient = new WebClient();
            webClient.Headers.Add("Authorization", $"{token}");
            webClient.DownloadFile(url, path);
        }

        public static string ConvertToJson(InvoiceData model, int status, Invoice oldInvoice = null)
        {
            var invoice = model.data.FirstOrDefault();
            JArray jarr = new JArray();
            JObject jsdatadetail = new JObject();
            jsdatadetail.Add("data", jarr);

            var inv_InvoiceAuth_id = Guid.NewGuid();

            if (status == 15 || status == 17)
            {
                inv_InvoiceAuth_id = oldInvoice.InvoiceId;
            }

            if (status == 5 || status == 17)
            {
                int index = 1;
                foreach (var item in oldInvoice.InvoiceDetails)
                {
                    JObject detail = new JObject();
                    detail.Add("inv_InvoiceAuthDetail_id", Guid.NewGuid());
                    detail.Add("ing_InvoiceAuth_id", inv_InvoiceAuth_id);
                    detail.Add("stt_rec0", index.ToString().PadLeft(8, '0'));
                    detail.Add("inv_itemCode", item.ItemCode);
                    detail.Add("inv_itemName", item.ItemName);
                    detail.Add("inv_unitCode", item.UnitCode);
                    detail.Add("inv_unitName", item.UnitName);
                    detail.Add("inv_unitPrice", item.UnitPrice);
                    detail.Add("inv_quantity", item.Quantity);
                    detail.Add("inv_TotalAmountWithoutVat", item.TotalAmountWithoutVat);
                    detail.Add("inv_vatPercentage", item.VatPercentage.ToString());
                    detail.Add("inv_TotalAmount", item.TotalAmount);
                    detail.Add("inv_vatAmount", item.VatAmount);
                    detail.Add("inv_promotion", false);
                    detail.Add("inv_discountPercentage", item.DiscountPercent);
                    detail.Add("inv_discountAmount", item.DiscountAmount);
                    detail.Add("ma_thue", item.VatPercentage);

                    jarr.Add(detail);
                }
            }
            else
            {
                foreach (var item in invoice.details.FirstOrDefault().data)
                {
                    JObject detail = new JObject();
                    detail.Add("inv_InvoiceAuthDetail_id", Guid.NewGuid());
                    detail.Add("ing_InvoiceAuth_id", inv_InvoiceAuth_id);
                    detail.Add("stt_rec0", item.stt_rec0);
                    detail.Add("inv_itemCode", item.inv_itemCode);
                    detail.Add("inv_itemName", item.inv_itemName);
                    detail.Add("inv_unitCode", item.inv_unitCode);
                    detail.Add("inv_unitName", item.inv_unitName);
                    detail.Add("inv_unitPrice", item.inv_unitPrice);
                    detail.Add("inv_quantity", item.inv_quantity);
                    detail.Add("inv_TotalAmountWithoutVat", item.inv_TotalAmountWithoutVat);
                    detail.Add("inv_vatPercentage", item.inv_vatPercentage.ToString());
                    detail.Add("inv_TotalAmount", item.inv_TotalAmount);
                    detail.Add("inv_vatAmount", item.inv_vatAmount);
                    detail.Add("inv_promotion", false);
                    detail.Add("inv_discountPercentage", item.inv_discountPercentage);
                    detail.Add("inv_discountAmount", item.inv_discountAmount);
                    detail.Add("ma_thue", item.inv_vatPercentage);

                    jarr.Add(detail);
                }
            }

            jsdatadetail.Add("tab_id", "TAB00188");
            jsdatadetail.Add("tab_table", "inv_InvoiceAuthDetail");

            // hết CT

            // ĐP

            JObject js = new JObject();
            js.Add("windowid", "WIN00187");
            if (status == 15 || status == 17)
            {
                js.Add("editmode", "2");
            }
            else
            {
                js.Add("editmode", "1");
            }


            JArray jarrDP = new JArray();

            JObject jsDP = new JObject();
            jsDP.Add("inv_InvoiceAuth_id", inv_InvoiceAuth_id);
            if (status == 19 || status == 21 || status == 17)
            {

                jsDP.Add("inv_invoiceType", oldInvoice.InvoiceType);
                jsDP.Add("inv_InvoiceCode_id", oldInvoice.InvoiceCodeId);
                jsDP.Add("inv_invoiceSeries", oldInvoice.InvoiceSeries);
                jsDP.Add("inv_invoiceIssuedDate", oldInvoice.InvoiceIssuedDate);
                jsDP.Add("inv_currencyCode", "VND");
                jsDP.Add("inv_exchangeRate", oldInvoice.ExchangeRate);
                jsDP.Add("inv_adjustmentType", status);
                if (oldInvoice.Customer != null)
                {
                    jsDP.Add("inv_buyerDisplayName", oldInvoice.Customer.FullName);
                    jsDP.Add("ma_dt", oldInvoice.Customer.CustomerCode);
                    jsDP.Add("inv_buyerLegalName", oldInvoice.Customer.CustomerCompanyName);
                    jsDP.Add("inv_buyerTaxCode", oldInvoice.Customer.TaxIdentification); //"0102236276"
                    jsDP.Add("inv_buyerAddressLine", oldInvoice.Customer.Address);
                    jsDP.Add("inv_buyerEmail", oldInvoice.Customer.Email);
                    jsDP.Add("inv_buyerBankAccount", oldInvoice.Customer.AccountNumber);
                    jsDP.Add("inv_buyerBankName", oldInvoice.Customer.Bank);
                }
                if (oldInvoice.PaymentType != null)
                {
                    jsDP.Add("inv_paymentMethodName", oldInvoice.PaymentType.Name);
                }
                jsDP.Add("inv_sellerBankAccount", oldInvoice.Account);
                jsDP.Add("inv_sellerBankName", oldInvoice.Bank);
                jsDP.Add("mau_hd", oldInvoice.InvoiceType);
            }
            else
            {
                jsDP.Add("inv_invoiceType", invoice.inv_invoiceType);
                jsDP.Add("inv_InvoiceCode_id", invoice.inv_InvoiceCode_id);
                jsDP.Add("inv_invoiceSeries", invoice.inv_invoiceSeries);
                jsDP.Add("inv_invoiceIssuedDate", invoice.inv_invoiceIssuedDate);
                jsDP.Add("inv_currencyCode", "VND");
                jsDP.Add("inv_exchangeRate", invoice.inv_exchangeRate);
                jsDP.Add("inv_adjustmentType", status);
                jsDP.Add("inv_buyerDisplayName", invoice.inv_buyerDisplayName);
                jsDP.Add("ma_dt", invoice.ma_dt);
                jsDP.Add("inv_buyerLegalName", invoice.inv_buyerLegalName);
                jsDP.Add("inv_buyerTaxCode", invoice.inv_buyerTaxCode); //"0102236276"
                jsDP.Add("inv_buyerAddressLine", invoice.inv_buyerAddressLine);
                jsDP.Add("inv_buyerEmail", invoice.inv_buyerEmail);
                jsDP.Add("inv_buyerBankAccount", invoice.inv_buyerBankAccount);
                jsDP.Add("inv_buyerBankName", invoice.inv_buyerBankName);
                jsDP.Add("inv_paymentMethodName", invoice.inv_paymentMethodName);
                jsDP.Add("inv_sellerBankAccount", invoice.inv_sellerBankAccount);
                jsDP.Add("inv_sellerBankName", invoice.inv_sellerBankName);
                jsDP.Add("mau_hd", invoice.inv_invoiceType);
            }

            if (status == 15 || status == 17)
            {
                jsDP.Add("inv_invoiceNumber", oldInvoice.InvoiceNumber);
            }
            else
            {
                jsDP.Add("inv_invoiceNumber", "");
            }
            jsDP.Add("inv_invoiceName", "Hóa don gia tri gia tang");

            jsDP.Add("trang_thai", "Chờ ký");
            jsDP.Add("nguoi_ky", "");
            jsDP.Add("sobaomat", "");
            jsDP.Add("trang_thai_hd", status);
            jsDP.Add("in_chuyen_doi", false);
            jsDP.Add("ngay_ky", null);
            jsDP.Add("nguoi_in_cdoi", "");
            jsDP.Add("ngay_in_cdoi", null);
            jsDP.Add("ma_ct", "HDDT");
            jsDP.Add("ma_dvcs", "VP");
            if (status == 5 || status == 21 || status == 19 || status == 3)
            {
                jsDP.Add("so_hd_dc", oldInvoice.InvoiceNumber);
                jsDP.Add("sovb", oldInvoice.InvoiceNumber);
                jsDP.Add("inv_originalId", oldInvoice.InvoiceId);
                jsDP.Add("ngayvb", invoice.inv_invoiceIssuedDate);
            }
            if (status == 15)
            {
                jsDP.Add("so_hd_dc", null);
                jsDP.Add("inv_originalId", null);
                jsDP.Add("sovb", null);
                jsDP.Add("ngayvb", null);

            }
            JArray arrdetail = new JArray();
            arrdetail.Add(jsdatadetail);
            jsDP.Add("details", arrdetail);

            jarrDP.Add(jsDP);

            js.Add("data", jarrDP);

            return js.ToString();
        }
        #endregion

        #region sInvoices
        public static string PostAPIViettel(string data, string url, string token)
        {

            WebClient client = new WebClient();
            client.Encoding = System.Text.Encoding.UTF8;
            client.Headers.Add(HttpRequestHeader.ContentType, "application/json");
            client.Headers.Add(HttpRequestHeader.Authorization, $"Basic {token}");
            string rsl = client.UploadString(url, "POST", data);
            return rsl;
        }
        public static string webRequest(string pzUrl, string pzData, string pzAuthorization, string pzMethod, string pzContentType)
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(pzUrl);
            httpWebRequest.ContentType = pzContentType;
            httpWebRequest.Method = pzMethod;
            httpWebRequest.Headers.Add("Authorization", "Basic " + pzAuthorization);
            httpWebRequest.Proxy = new WebProxy();//no proxy

            if (!string.IsNullOrEmpty(pzData))
            {
                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string json = pzData;

                    streamWriter.Write(json);
                    streamWriter.Flush();
                    streamWriter.Close();
                }
            }
            InitiateSSLTrust();//bypass SSL
            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            var result = string.Empty;
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                result = streamReader.ReadToEnd();
            }
            return result;
        }

        public static string PostAPIViettelByFormData(object data, string url, string token)
{
    WebClient client = new WebClient();
    client.Encoding = System.Text.Encoding.UTF8;
    client.Headers.Add(HttpRequestHeader.Authorization, $"Basic {token}");

    foreach (var item in data.GetType().GetProperties())
    {
        client.QueryString.Add(item.Name, item.GetValue(data, null).ToString());
    }

    InitiateSSLTrust();
    var rsl = client.UploadValues(new Uri(url), "POST", client.QueryString);
    return UnicodeEncoding.UTF8.GetString(rsl);
}

#endregion

public static void InitiateSSLTrust()
{
    try
    {
        ServicePointManager.ServerCertificateValidationCallback =
           new RemoteCertificateValidationCallback(
                delegate
                { return true; }
            );
    }
    catch (Exception ex)
    {

    }
}
    }
}