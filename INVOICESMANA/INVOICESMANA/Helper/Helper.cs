﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Newtonsoft.Json;
using INVOICESMANA.Models;
using System.Web.Routing;
using INVOICESMANA.DATA.Common;

namespace INVOICESMANA.Helper
{
    public static class Identity
    {
        public static Guid UserId
        {
            get
            {
                string data = HttpContext.Current.User.Identity.Name;
                UserSimple userData = JsonConvert.DeserializeObject<UserSimple>(data);
                return userData.UserId;
            }
        }

        public static string UserName
        {
            get
            {
                string data = HttpContext.Current.User.Identity.Name;
                UserSimple userData = JsonConvert.DeserializeObject<UserSimple>(data);
                return userData.UserName;
            }
        }

        public static Guid? RoleId
        {
            get
            {
                string data = HttpContext.Current.User.Identity.Name;
                UserSimple userData = JsonConvert.DeserializeObject<UserSimple>(data);
                Guid roleId;
                if (Guid.TryParse(userData.RoleId.ToString(), out roleId))
                {
                    return roleId;
                }
                return null;
            }
        }

        public static List<Guid> RoleIdSuperAdmin
        {
            get
            {
                string data = HttpContext.Current.User.Identity.Name;
                UserSimple userData = JsonConvert.DeserializeObject<UserSimple>(data);
                if (userData.SRolesId != null)
                {
                    return userData.SRolesId;
                }
                else
                {
                    return new List<Guid>();
                }
            }
        }

        public static bool IsSuperAdmin
        {
            get
            {
                string data = HttpContext.Current.User.Identity.Name;
                UserSimple userData = JsonConvert.DeserializeObject<UserSimple>(data);
                Guid roleId;
                if (Guid.TryParse(userData.RoleId.ToString(), out roleId))
                {
                    return (roleId != null && userData.SRolesId.Contains(roleId));
                }
                return false;
            }
        }
    }

    public class LogFilterAttribute : ActionFilterAttribute
    {
        public Object ModuleId { get; set; }
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            bool isSuperAdmin = false;
            if (Identity.RoleId != null && Identity.RoleIdSuperAdmin.Contains((Guid)Identity.RoleId))
            {
                isSuperAdmin = true;
            }
            bool allowed = new Permision(Guid.Parse(ModuleId.ToString())).IsViewAllowed;
            if (!isSuperAdmin && !allowed)
            {
                filterContext.Result = new RedirectToRouteResult(
                        new RouteValueDictionary
                        {
                            { "controller", "Home" },
                            { "action", "Index" }
                        });
            }
        }
    }

    public class Permision
    {
        private Guid ModuleId { get; set; }
        public Permision(Guid _moduleId)
        {
            ModuleId = _moduleId;
        }
        public bool IsViewAllowed
        {
            get
            {
                bool isAllowed = false;
                if (Identity.IsSuperAdmin)
                {
                    isAllowed = true;
                }
                else
                {
                    try
                    {
                        using (var uow = new UnitOfWork())
                        {
                            var roleModules = uow.ModulesRepository.GetWhere(x => x.Status == true && x.ModuleId == ModuleId).FirstOrDefault().RoleModules;
                            if (roleModules.Any())
                            {
                                List<Guid> userRoleId = uow.UserRepository.GetById(Identity.UserId).Roles.Select(x => x.RoleId).ToList();
                                List<DATA.RoleModule> permisions = roleModules.Where(x => userRoleId.Contains(x.RoleId)).ToList();
                                if (permisions.Any() && permisions.FirstOrDefault().IsViewAllowed == true)
                                {
                                    isAllowed = true;
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                   
                }
                return isAllowed;
            }
        }

        public bool IsUpdateAllowed
        {
            get
            {
                bool isAllowed = false;
                if (Identity.IsSuperAdmin)
                {
                    isAllowed = true;
                }
                else
                {
                    using (var uow = new UnitOfWork())
                    {
                        var modules = uow.ModulesRepository.GetWhere(x => x.Status == true && x.ModuleId == ModuleId);
                        if (modules.Any())
                        {
                            var roles = uow.RoleModulesRepository.GetWhere(x => x.Role.Status == true);
                            List<Guid> rolesUser = uow.INVOICESdbContext.Users.Include("Roles").FirstOrDefault(x => x.UserName == Identity.UserName).Roles.Select(x => x.RoleId).ToList();
                            var res = modules.Select(x => x.RoleModules.Where(z => rolesUser.Contains(z.RoleId) == true)).ToList();
                            if (res.Any())
                            {
                                for (var i = 0; i < res.Count(); i++)
                                {
                                    if (res[i].FirstOrDefault() != null && res[i].FirstOrDefault().IsUpdateAllowed == true)
                                    {
                                        isAllowed = true;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
                return isAllowed;
            }
        }

        public bool IsDeleteAllowed
        {
            get
            {
                bool isAllowed = false;
                if (Identity.IsSuperAdmin)
                {
                    isAllowed = true;
                }
                else
                {
                    using (var uow = new UnitOfWork())
                    {
                        var modules = uow.ModulesRepository.GetWhere(x => x.Status == true && x.ModuleId == ModuleId);
                        if (modules.Any())
                        {
                            var roles = uow.RoleModulesRepository.GetWhere(x => x.Role.Status == true);
                            List<Guid> rolesUser = uow.INVOICESdbContext.Users.Include("Roles").FirstOrDefault(x => x.UserName == Identity.UserName).Roles.Select(x => x.RoleId).ToList();
                            var res = modules.Select(x => x.RoleModules.Where(z => rolesUser.Contains(z.RoleId) == true)).ToList();
                            if (res.Any())
                            {
                                for (var i = 0; i < res.Count(); i++)
                                {
                                    if (res[i].FirstOrDefault() != null && res[i].FirstOrDefault().IsDeleteAllowed == true)
                                    {
                                        isAllowed = true;
                                        break;
                                    }
                                }
                            }

                        }
                    }
                }
                return isAllowed;
            }
        }

        public bool IsAddAllowed
        {
            get
            {
                bool isAllowed = false;
                if (Identity.IsSuperAdmin)
                {
                    isAllowed = true;
                }
                else
                {
                    using (var uow = new UnitOfWork())
                    {
                        var modules = uow.ModulesRepository.GetWhere(x => x.Status == true && x.ModuleId == ModuleId);
                        if (modules.Any())
                        {
                            var roles = uow.RoleModulesRepository.GetWhere(x => x.Role.Status == true);
                            List<Guid> rolesUser = uow.INVOICESdbContext.Users.Include("Roles").FirstOrDefault(x => x.UserName == Identity.UserName).Roles.Select(x => x.RoleId).ToList();
                            var res = modules.Select(x => x.RoleModules.Where(z => rolesUser.Contains(z.RoleId) == true)).ToList();
                            if (res.Any())
                            {
                                for (var i = 0; i < res.Count(); i++)
                                {
                                    if (res[i].FirstOrDefault() != null && res[i].FirstOrDefault().IsAddAllowed == true)
                                    {
                                        isAllowed = true;
                                        break;
                                    }
                                }
                            }

                        }
                    }
                }
                return isAllowed;
            }
        }
    }

    public enum InvoiceTypes
    {
        mInvoice = 1,
        viettel = 2
    }
}