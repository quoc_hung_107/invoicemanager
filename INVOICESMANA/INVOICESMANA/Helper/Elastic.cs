﻿using Elasticsearch.Net;
using INVOICESMANA.DATA;
using INVOICESMANA.DATA.Common;
using Nest;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;

namespace INVOICESMANA.Helper
{
    public class Elastic
    {
        private static string defaultIndex = ConfigurationSettings.AppSettings["elasticIndex"];
        public static async Task IndexLastAudit(Invoice inv)
        {
            HostingEnvironment.QueueBackgroundWorkItem(t =>
            {
                try
                {
                    ezInvoicesEntities context = new ezInvoicesEntities();
                    var node = new Uri("http://localhost:9200");
                    var settings = new ConnectionSettings(node).DefaultIndex(defaultIndex);
                    var client = new ElasticClient(settings);
                    object dataLog = new
                    {
                        Date = DateTime.UtcNow,
                        CurrentValue = context.Entry(inv).CurrentValues,
                        OriginalValue = context.Entry(inv).OriginalValues
                    };

                    var result = client.Index(dataLog, i => i.Index(defaultIndex).Type("logsInvoices").Id(Guid.NewGuid().ToString()).Refresh());
                }
                catch (Exception ex) { }

            });

            return;
        }
    }
}